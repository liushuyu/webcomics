# Transcript of Pepper&Carrot Episode 14 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 14 : La Dent de Dragon

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|5|True|Bon, bon, bon! Fini les vacances
Cumin|6|False|Commençons par un cours sur les potions antiques et leurs ingrédients primaires
Cumin|7|True|Mmm...Zut! Plus de poudre de dent de dragon
Cumin|8|False|Inutile de commencer ce cours sans cet ingrédient...
Écriture|9|False|Dent de Dragon
Écriture|4|False|33
Écriture|3|False|PROPRIÉTÉ PRIVÉE
Écriture|2|True|SORCIÈRE
Écriture|1|True|DANGER

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|1|False|Cayenne! Thym! Vous pourriez aller me chercher de la dent de dragon ?
Cayenne|2|False|Par ce froid?! Non merci!
Thym|3|False|Mouais... Pareil...
Pepper|4|False|Pas de soucis je m'en charge!
Pepper|6|False|Je reviens bientôt!
Pepper|7|True|"Le froid", "le froid"!
Pepper|8|False|Non mais quelle bande de poules mouillées !
Pepper|9|False|Une bonne tenaille, quelques protections et à nous les dents de dragons !
Cumin|5|False|Pepper attends!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ha ha!
Pepper|2|False|Zut! Ca va être un poil plus compliqué que ce que j'avais imaginé!
Pepper|3|True|Pfff!
Pepper|4|False|...et moi qui croyais que ce serait plus facile avec un dragon de l'air !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Carrot !...
Pepper|2|True|Reviens ...
Pepper|3|True|...Le dragon des marais est réputé pour être plus docile pourtant
Pepper|4|False|je l'ai pas inventé...
Pepper|5|True|Mmm... Pour le dragon-foudre...
Pepper|6|False|...Pas sure que cette pince soit adaptée

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ok.
Pepper|2|True|C'est trop difficile...
Pepper|3|False|... j'abandonne.
Pepper|5|False|Pepper n'abandonne jamais !
Pepper|4|True|Non !
Narrateur|6|False|Le lendemain
Cumin|8|False|! !|nowhitespace
Oiseau|7|False|COCORICO O oooo ! ! !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|10|False|- FIN -
Crédits|11|False|01/2016 - Dessin & Scénario : David Revoy
Écriture|4|True|Dentiste
Écriture|5|True|pour
Écriture|6|False|Dragons
Écriture|7|False|Gratuit!
Son|3|False|pop !|nowhitespace
Pepper|1|True|Alors, impressionée, non?
Pepper|2|False|J'ai au moins une bonne centaine de dents de dragon!
Cumin|8|True|...mais Pepper, la dent de dragon...
Cumin|9|False|...c'est une plante!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 671 Mécènes :
Crédits|4|False|License : Creative Commons Attribution 4.0 Sources : disponibles sur www.peppercarrot.com Logiciels : cet épisode a été dessiné à 100% avec des logiciels libres Krita 2.9.10, Inkscape 0.91 on Linux Mint 17
Crédits|2|True|Vous aussi, devenez mécène de Pepper&Carrot pour le prochain épisode sur
Crédits|3|False|https://www.patreon.com/davidrevoy

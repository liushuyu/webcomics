# Transcript of Pepper&Carrot Episode 14 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 14: The Dragon's Tooth

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|5|True|Well well well! Holidays are over!
Cumin|6|False|Let's start with a course on ancient potions and their primary ingredients.
Cumin|7|True|Hmm... Shoot! No more powdered Dragon's Tooth.
Cumin|8|False|No point starting the course without it...
Writing|9|False|Dragon's Tooth
Writing|4|False|33
Writing|3|False|PROPERTY
Writing|2|True|WITCH
Writing|1|True|WARNING

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|1|False|Cayenne! Thyme! Can one of you go find me some Dragon's Tooth?
Cayenne|2|False|In this cold?! No thanks!
Thyme|3|False|Yeah... Ditto...
Pepper|4|False|No problem, I'll take care of it!
Pepper|6|False|Back in a jiffy!
Pepper|7|True|"The cold", "the cold"!
Pepper|8|False|What a bunch of sissies!
Pepper|9|False|A good set of pliers, a little body armor, and we'll have those Dragon's Teeth!
Cumin|5|False|Pepper, wait!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ha ha!
Pepper|2|False|Whoo ! I think it'll be a teeny tiny bit more complicated than I thought!
Pepper|3|True|Pfff!
Pepper|4|False|... and I thought it'd be easier with an Air Dragon!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Carrot! ...
Pepper|2|True|Come back ...
Pepper|3|True|... Swamp dragons are famous for their docile nature!
Pepper|4|False|at least I thought so...
Pepper|5|True|Hmm... For the Lightning Dragon...
Pepper|6|False|... I'm not so sure that this is the right tool...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ok.
Pepper|2|True|It's too hard...
Pepper|3|False|... I give up
Pepper|5|False|Pepper never gives up !
Pepper|4|True|No !
Bird|7|False|COCk-a-doodle-doooooo ! ! !|nowhitespace
Narrator|6|False|The next day...
Cumin|8|False|! !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|10|False|- FIN -
Credits|11|False|01/2016 - Art & Scenario: David Revoy Translation: Alex Gryson
Writing|6|False|Dentist|nowhitespace
Writing|5|True|-|nowhitespace
Writing|4|True|Dragon
Writing|7|False|Free!
Sound|3|False|pop !|nowhitespace
Pepper|1|True|Well? Impressed, aren't you?
Pepper|2|False|I have at least a hundred Dragon's Teeth!
Cumin|8|True|... but Pepper, Dragon's Tooth...
Cumin|9|False|... It's a plant!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thank you to the 671 Patrons:
Credits|2|True|You too can become a patron of Pepper&Carrot for the next episode at
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|4|False|License: Creative Commons Attribution 4.0 Source: available at www.peppercarrot.com Software: this episode was 100% drawn with libre software Krita 2.9.10, Inkscape 0.91 on Linux Mint 17

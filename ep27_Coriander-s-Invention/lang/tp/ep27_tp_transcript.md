# Transcript of Pepper&Carrot Episode 27 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute luka tu: ilo sin pi jan Kowijana

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|1|False|ma Tomopona lon insa pi tenpo suno
jan pali len|4|False|jan lawa lili Kowijana o...
jan pali len|5|False|sina ken ala ken pini e tawa?
jan Kowijana|6|False|ken, ken...
jan pali len|7|False|tenpo li kama pini. taso tenpo pi lawa sina li kama lon tenpo ilo tu wan.
jan Kowijana|8|False|...
sitelen|2|True|ONA LI KAMA LAWA
sitelen|3|False|JAN LAWA KOWIJANA
<hidden>|0|False|Translate “Her Majesty’s Coronation” and “Queen Coriander” on the banner on the building (in the middle). The other text boxes will update automatically (they are clones).
<hidden>|0|False|NOTE FOR TRANSLATORS
<hidden>|0|False|You can strech the scroll and translate “Qualicity” to “City of Qualicity”.
<hidden>|0|False|NOTE FOR TRANSLATORS

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kowijana|1|True|jan pona o...
jan Kowijana|2|False|awen mi li lon lawa mi...
jan Kowijana|3|True|jan lawa mute li moli lon tenpo sewi ona pi kama lawa tan jan ike.
jan Kowijana|4|False|ni li kama nasin tawa sona tenpo pi kulupu mama mi.
jan Kowijana|5|True|tenpo lili la, jan mute en kulupu mute li kama tan ma ale. mi lon insa pi ale ni. ona ale li kama tawa mi.
jan Pepa|7|True|o pilin ike ala, jan Kowijana o.
jan Kowijana|6|False|mi pilin ike. mi wile moli ala a!
jan Pepa|8|True|mi ale o kepeken nasin pi mi ale.
jan Pepa|9|False|jan Shichimi en mi li pali, li awen e sijelo sina. o awen e sona ni.
jan Sisimi|10|True|ni a.
jan Sisimi|11|False|sina o lape e lawa sina. o lukin e ijo ante.
jan Kowijana|12|False|pona.
jan Kowijana|13|False|a! mi sona! mi ken pana e ilo sin tawa lukin sina!
jan pali len|14|False|...
jan Kowijana|15|True|ona li lon anpa tomo, lon tomo pali mi.
jan Kowijana|16|False|o tawa lon poka mi.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kowijana|1|False|sina kama sona e ni: mi wile pali e ona sin tan seme?
jan Kowijana|2|False|mi pali e ona tan pilin ike tawa tenpo mi pi kama lawa.
jan Kowijana|3|False|o lukin e ilo pali sin mi...
ilo pi sona pilin|5|False|ale o.
ilo pi sona pilin|6|False|mi ILO PI SONA PILIN mu, mu!
ilo pi sona pilin|7|False|o kama supa lon supa mi. mi kama sona e pilin sina.
kalama|4|False|Nena!
kalama|8|False|Luka!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kowijana|1|True|sina toki e seme?
jan Kowijana|2|False|sina wile ala wile kepeken ona?
jan Pepa|3|True|ala.
jan Pepa|4|True|ni li suli ala.
jan Pepa|5|False|pilin mi li pona taso.
kalama|6|True|W|nowhitespace
jan Pepa|11|False|JAN O, ILO SINA LI IKE TAN SEME?!
jan Kowijana|12|True|ike mi!
jan Kowijana|13|True|pakala li lon ona. mi sona ala pona e ni.
jan Kowijana|14|False|ona li pana e wawa seli tawa toki pi lon ala .
jan Pepa|15|False|toki pi lon ala?!
jan Pepa|16|False|taso mi toki pi lon ala lon tenpo ALA!
jan Pepa|22|True|NI...
jan Pepa|23|True|LI...
jan Pepa|24|False|MUSI ALA!!!
kalama|7|True|A|nowhitespace
kalama|8|True|W|nowhitespace
kalama|9|True|A|nowhitespace
kalama|10|False|!|nowhitespace
kalama|17|True|W
kalama|18|True|A|nowhitespace
kalama|19|True|W|nowhitespace
kalama|20|True|A|nowhitespace
kalama|21|False|!|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sisimi|1|False|musi anu seme? ala. ala! ike mi.
jan Kowijana|2|False|aa a! lon! ni li musi ala a!
jan Pepa|8|True|a...
jan Pepa|9|True|a...
jan Pepa|10|False|a.
jan Kowijana|11|True|IKE A!
jan Kowijana|12|False|len mi pi tenpo sewi li jaki! jan pi pali len li pana e toki ike tan ni!
jan Kowijana|13|True|ILO IKE A!
jan Kowijana|14|False|mi weka e pakala, lon tenpo ale a!
jan Pepa|15|False|O PINI!!!
kalama|3|True|W
kalama|4|True|A|nowhitespace
kalama|5|True|W|nowhitespace
kalama|6|True|A|nowhitespace
kalama|7|False|!|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|mi sona e pona pali tawa pakala ni.
kalama|2|False|Kiwen!
sitelen toki|3|False|tenpo pimeja li kama...
jan Kowijana|11|False|jan Pepa o, sina jan sona!
jan Pepa|12|True|ala, sina jan sona.
jan Pepa|13|False|ni li ilo sin sina !
ilo pi sona pilin|14|False|jan kama o, kama.
ilo pi sona pilin|15|True|toki!
ilo pi sona pilin|16|False|sina wile ala wile moli e jan lon tenpo pimeja ni?
sitelen toki|17|False|TOKI NI LI AWEN...
kalama|6|True|W
kalama|7|True|A|nowhitespace
kalama|8|True|W|nowhitespace
kalama|9|True|A|nowhitespace
kalama|10|False|!|nowhitespace
sitelen|4|True|ONA LI KAMA LAWA
sitelen|5|False|JAN LAWA KOWIJANA

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|5|True|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa. nimi sina li ken lon ni!
jan Pepa|3|True|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute.
jan Pepa|4|False|jan 1060 li pana e mani la, lipu ni li lon!
jan Pepa|7|True|sina wile sona la, o lukin e lipu www.peppercarrot.com !
jan Pepa|6|True|ilo Patreon en ilo Tipeee en ilo PayPal en ilo Liberapay en ilo ante la, mi lon!
jan Pepa|8|False|sina pona!
jan Pepa|2|True|sina sona ala sona?
mama|1|False|tenpo October 31, 2018 musi sitelen & toki: jan David Revoy. jan pi lukin pona: jan Amyspark en jan Butterfly en jan Bhoren en jan CalimeroTeknik en jan Craig Maloney en jan Gleki Arxokuna en jan Imsesaok en jan Jookia en jan Martin Disch en jan Midgard en jan Nicolas Artance en jan uncle Night en jan ValVin en jan xHire en jan Zveryok. toki pona ante toki li tan jan Ret Samys. jan Juli li pona e ona. musi ni li tan pali pi ma Elewa mama: jan David Revoy. jan awen nanpa wan: jan Craig Maloney. jan pi toki sitelen: jan Craig Maloney en jan Nartance en jan Scribblemaniac en jan Valvin. jan pi pona pali: jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson . ilo: ilo Krita 4.2-dev en ilo Inkscape 0.92.3 li kepeken ilo Kubuntu 18.04. nasin pi lawa jo: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.

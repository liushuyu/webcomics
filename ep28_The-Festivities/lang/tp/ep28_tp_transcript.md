# Transcript of Pepper&Carrot Episode 28 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute luka tu wan: tenpo musi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|1|True|tenpo pimeja ni la, mun tu wan pi ma Elewa li lon poka,
sitelen toki|2|False|li kama e tenpo sewi pona tan suno ona, lon tomo sewi Molitawaa.
sitelen toki|3|False|suno sewi ni la, jan pona mi Kowijana li kama...
sitelen toki|4|False|jan lawa pi ma Tomopona.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|1|False|tenpo sewi li pini la, tenpo musi li lon.
sitelen toki|2|False|jan suli pi mute suli en kulupu ale pi wawa nasa li kama tan ma Elewa ale tawa tenpo musi suli.
jan Pepa|3|False|!?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|a! ni li sina!
jan Pepa|3|False|mi lon insa lawa mi.
soweli Kawa|2|False|Poka Poka
jan Pepa|4|False|mi tawa tomo. poka ni li kama lete.
jan pi toki pi ijo sin|6|False|o tawa! ona li lon!
jan pi toki pi ijo sin|5|False|o lukin! ni li ona!
jan pi toki pi ijo sin|7|False|jan Sapon o! sina ken ala ken pana e toki tawa lipu Suno Tomopona?
jan Sapon|8|False|pona, mi ken!
jan Pepa|15|False|...
jan Pepa|16|False|sina sona e pilin anpa mi, soweli Kawa o, anu seme?
kalama|9|False|S UNO|nowhitespace
jan pi toki pi ijo sin|13|True|jan Sapon o! mi tan lipu Tenpo Elewa.
kalama|10|False|S UNO|nowhitespace
kalama|11|False|S UNO|nowhitespace
kalama|12|False|S UNO|nowhitespace
jan pi toki pi ijo sin|14|False|len sina li tan jan len musi seme?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|jan pona ale mi li pali pona suli. tenpo mute la, mi wile e sama lili.
jan Pepa|2|False|jan Kowijana li jan lawa.
jan Pepa|3|False|jan Sapon li mani mute, li pona tawa kulupu.
jan Pepa|4|False|jan Sisimi kin li suli lukin lon kulupu ona.
jan Pepa|5|False|taso seme li tawa mi?
jan Pepa|6|False|ni.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sisimi|1|False|jan Pepa o?
jan Sisimi|2|False|mi weka len e moku tawa mi. sina wile ala wile?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sisimi|1|True|mi wile kepeken nasin len tawa moku la, mi pilin nasa.
jan Sisimi|2|False|taso jan sona mi la, kulupu mi li sama kon sewi, li wile ala e wile jan.
jan Sapon|3|False|aa! sina lon ni!
jan Sapon|4|True|pini pona a! mi kama weka tan jan pi wile sitelen lon ni.
jan Sapon|5|False|ona li nasa e lawa mi.
jan Kowijana|6|False|jan pi wile sitelen anu seme?
jan Kowijana|7|False|sina sona ala e toki pi pilin ala lon poka pi jan lawa, kepeken ni lon lawa sina.
kalama|8|False|Luka Luka

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kowijana|1|True|sin la, jan Pepa o, mi kama sona e jan sona sina.
jan Kowijana|2|False|ona li “ante” a.
jan Pepa|3|False|!!!
jan Kowijana|4|True|mi la, pona li tawa sina.
jan Kowijana|5|False|pilin la, ona li ken e ale tawa sina.
jan Kowijana|6|False|sina wile ala e pona ala tan nasin toki pi jan lawa la, sina ken utala e ona.
jan Sapon|7|False|pilin pi jan ante li suli ala tawa sina la, sina ken musi, li ken tawa musi.
jan Sisimi|8|False|sama la, jan ante li lukin e sina la, sina ken moku e moku ale!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|4|False|TOKI NI LI AWEN...
jan Sisimi|1|False|a! jan Pepa o!? toki mi li ike e pilin sina anu seme?
jan Pepa|2|True|ala a!
jan Pepa|3|False|sina pona, jan pona mi o!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|5|True|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa. nimi sina li ken lon ni!
jan Pepa|3|True|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute.
jan Pepa|4|False|jan 960 li pana e mani la, lipu ni li lon!
jan Pepa|7|True|sina wile sona la, o lukin e lipu www.peppercarrot.com !
jan Pepa|6|True|ilo Patreon en ilo Tipeee en ilo PayPal en ilo Liberapay en ilo ante la, mi lon!
jan Pepa|8|False|sina pona!
jan Pepa|2|True|sina sona ala sona?
mama|1|False|tenpo January, 2019 musi sitelen & toki: jan David Revoy.. jan pi lukin pona: jan CalimeroTeknik en jan Craig Maloney en jan Martin Disch en jan Midgard en jan Nicolas Artance en jan Valvin. toki pona ante toki: jan Ret Samys. musi ni li tan pali pi ma Elewa mama: jan David Revoy. jan awen nanpa wan: jan Craig Maloney. jan pi toki sitelen: jan Craig Maloney en jan Nartance en jan Scribblemaniac en jan Valvin. jan pi pona pali: Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson . ilo: ilo Krita 4.1.5~appimage en ilo Inkscape 0.92.3 li kepeken ilo Kubuntu 18.04.1. nasin pi lawa jo: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.

# Transcript of Pepper&Carrot Episode 10 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa luka luka: lipu pona pi tenpo seli

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|1|False|- PINI -
mama|2|False|tenpo 08/2015 musi sitelen & toki li tan jan David Revoy ante toki li tan jan Ret Samys

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 422 li pana e mani la, lipu ni li lon:
mama|3|False|https://www.patreon.com/davidrevoy
mama|2|True|sina ken pana kin e pona tawa lipu kama pi jan Pepa&soweli Kawa:
mama|4|False|nasin lawa : Creative Commons Attribution 4.0 mama pali li lon lipu www.peppercarrot.com ilo : lipu ni li kepeken taso ilo pi nasin jo pi jan ale. ilo pi nasin Libre li ilo Krita 2.9.6, li ilo Inkscape 0.91 lon ilo Linux Mint 17

# Transcript of Pepper&Carrot Episode 12 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa luka luka tu: weka jaki pi tenpo pi kasi loje

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|9|True|Kiwen
kalama|10|True|Kiwen
kalama|11|False|Kiwen
jan Kajen|1|False|telo pi kalama uta musi
jan Kajen|2|False|telo pi linja lawa mute
jan Kajen|3|False|telo pi sike kon jaki
jan Kajen|4|False|telo pi ale pona
jan Kajen|5|False|en telo pi kon pimeja...
jan Kajen|6|False|... en telo ante mute!
jan Pepa|7|True|mi sona, mi sona:
jan Pepa|8|False|"jan-sona-pi-kulupu-Pakalaa-li-pali-ala-e-telo-ni".
jan Kajen|13|True|mi wile tawa esun pi ma Komona.
jan Kajen|14|False|mi weka la, o weka e ale ni; jan pi wile musi li kama jo e telo ni la, pakala mute li ken tan musi ona.
jan Kajen|12|False|pona. ni o lon.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|2|False|Tawaaa !|nowhitespace
jan Pepa|3|False|pakala!
jan Pepa|4|True|taso ni li kepeken tenpo mute li pakala e luka mi!
jan Pepa|5|False|Anpa
kalama|6|False|mi o weka e ona? pona! mi wile!
jan Pepa|1|False|mi o tawa anpa ma e ale anu seme?!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|"taso tenpo ale la, o kepeken ala wawa sina. pali pi nasa ala la, jan sona pi kulupu Pakalaa li kepeken ala wawa nasa."
jan Pepa|2|False|ike a !
jan Pepa|3|True|soweli Kawa o!
jan Pepa|4|False|o pana e lipu nanpa luka tu pi kulupu Pakalaa: "sona pi nasin pi kama anpa kiwen"... ...o tawa!
jan Pepa|5|False|mi pana a e pali mi pi kulupu Pakalaa a tawa ona a!
kalama|6|True|Noka
kalama|7|False|Noka

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|2|False|KO O oo oo oo oon n|nowhitespace
jan Kajen|3|False|... lupa pimeja pi kulupu Pakalaa a?!...
jan Pepa|4|False|?!
jan Kajen|5|False|... sina sona ala sona e toki mi ni: "o kepeken ala e wawa sina"?
jan Pepa|6|False|... a... sina lon ala lon esun?
jan Kajen|7|True|mi ken ala!
jan Kajen|8|False|mi sona e ni: sina pali ala e pona; mi o lukin e sina lon tenpo ale!
jan Pepa|1|False|MOKUS PIMEJUS!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|M o oo K u u MOOKU|nowhitespace
kalama|7|False|M o oo K u u MOOKU|nowhitespace
kalama|6|False|Anpa
jan Kajen|8|True|kama ni li kama: ale li kama awen lon sike awen. ante la, ona li awen lon poki awen, li tawa ala.
soweli Kawa|10|False|?!
jan Kajen|2|True|... o lukin e ni.
jan Kajen|3|True|kiwen ona li lili taso!
jan Kajen|4|True|kama ante pi nasin pi kama anpa kiwen li wawa ala!
jan Kajen|5|False|ike la, sike awen pona pi poka lupa li suli ala.
jan Kajen|9|False|sina suli lili e selo pi lupa pimeja la, ale li pona. taso sina pali ala e ni!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|! ! !|nowhitespace
jan Kajen|2|False|! ! !|nowhitespace
jan Kajen|3|False|WAWATUM PINITUS SULIMUS!
kalama|4|False|A wen!|nowhitespace
sitelen toki|5|False|- PINI -
mama|6|False|tenpo 10/2015 - musi sitelen & toki li tan jan David Revoy - ante toki li tan jan Ret Samys

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 575 li pana e mani la, lipu ni li lon:
mama|3|False|https://www.patreon.com/davidrevoy
mama|2|True|sina ken pana kin e pona tawa lipu kama pi jan Pepa/soweli Kawa:
mama|4|False|nasin lawa : Creative Commons Attribution 4.0 mama pali li lon lipu www.peppercarrot.com ilo : sitelen pi lipu ni li kepeken taso ilo pi nasin jo pi jan ale. ilo pi nasin Libre li ilo Krita 2.9.8, li ilo Inkscape 0.91, li Blender 2.71, li GIMP 2.8.14, li G'MIC 1.6.7 lon Linux Mint 17.2

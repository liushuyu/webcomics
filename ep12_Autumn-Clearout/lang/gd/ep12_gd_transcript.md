# Transcript of Pepper&Carrot Episode 12 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 12: An cartadh-foghair

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|9|True|Gliong
Fuaim|10|True|Glang
Fuaim|11|False|Gloing
Cìob|1|False|Deochan-gàire
Cìob|2|False|Deochan an fhalt-fhàis mhòir
Cìob|3|False|Deochan builgein-tùid
Cìob|4|False|Deochan “cho sona ri luchag an lofa”
Cìob|5|False|Deochan toite ...
Cìob|6|False|... gun ainmeachadh ach air cuid bheag dhiubh!
Peabar|7|True|Seadh seadh, tha fhios ’am:
Peabar|8|False|“Cha-chruthaicheadh-bana-bhuidseach-cheart-na-Dubh-choimeasgachd-deochan-dhen-leithid”.
Cìob|13|True|Feumaidh mi togail orm gu fhèill Chomona a-nis.
Cìob|14|False|Gun rachadh a h-uile càil seo à sealladh mus till mi; cha bhiodh e math nan lorgadh amadain bheaga e a chluicheadh leis.
Cìob|12|False|Sin dìreach. ’S mar bu chòir.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|2|False|Fruuuis !|nowhitespace
Peabar|3|False|Murt!
Peabar|4|True|Adhlachadh ?!
Peabar|5|False|Ach bheir seo tòrr ùine ’s na doir guth air na fuileanan a bhios oirnn mus bi sinn deiseil leis!
Fuaim|6|False|Poc
Peabar|1|False|A-mach à sealladh? Furasta! Hò-rò!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|2|False|Grrr !
Peabar|3|True|A CHURRAIN!
Peabar|4|False|Faigh lorg leabhar a seachd na Dubh-choimeasgachd: “Raointean cumhachd-talmhainn” ... gun dàil!
Peabar|5|False|Seallaidh mi dhan fheannag theagmhail a tha seo dè th’ ann am bana-bhuidseach cheart na Dubh-choimeasgachd!
Fuaim|6|True|Tom
Fuaim|7|False|Tom
Peabar|1|False|“Agus na cleachd draoidheachd air dòigh sam bith. Cha chleachd bana-bhuidseach cheart na Dubh-choimeasgachd draoidheachd airson dreuchd an latha.”

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|2|False|FRU U uu Bh uu uu uu|nowhitespace
Cìob|3|False|... Toll Dubh-choimeasgach?! ...
Peabar|4|False|?!
Cìob|5|False|... An e seo mar a thuigeas tusa “na cleachd draoidheachd”?
Peabar|6|False|... Haoigh ... Nach robh dol dhan fhèill fainear dhuibh ?
Cìob|7|True|’S cinnteach nach robh!
Cìob|8|False|’S bha mi ceart cuideachd; tha e do-dhèanta d’ fhàgail ’nad aonar ’s gun sùil a chumail ort fad na h-ùine!
Peabar|1|False|GURGES ATER!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|Ao uu aou aou aou|nowhitespace
Fuaim|7|False|Ao uu a ouu aouu|nowhitespace
Fuaim|6|False|Beum
Cìob|8|True|Agus seo dhut: Mairidh cha mhòr a h-uile càil air fleod ’s e ga tharraing dha chuairtean seasmhach no puingean Lagrange
Curran|10|False|?!
Cìob|2|True|... Coimhead air seo.
Cìob|3|True|Chan fhoghainn an tomad!
Cìob|4|True|Tha diofarail an raoin iom-tharraing lag!
Cìob|5|False|Tha fiù ’s a’ chuairt chearcallach sheasmhach as dlùithe ro bheag!
Cìob|9|False|Rinneadh oir thachartasan dìreach beagan nas motha a chùis!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|! ! !|nowhitespace
Cìob|2|False|! ! !|nowhitespace
Cìob|3|False|MIONNUS DUBHÀSARE MAXIMUS!
Fuaim|4|False|Br rag!|nowhitespace
Neach-aithris|5|False|- Deireadh na sgeòil -
Urram|6|False|10/2015 – Obair-ealain ⁊ sgeulachd: David Revoy – Eadar-theangachadh: GunChleoc

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 575 pàtran a thug taic dhan eapasod seo:
Urram|3|False|https://www.patreon.com/davidrevoy
Urram|2|True|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd airson an ath-eapasod air
Urram|4|False|Ceadachas : Creative Commons Attribution 4.0 Bun-tùs : ri fhaighinn air www.peppercarrot.com Bathar-bog : chaidh 100% dhen eapasod seo a tharraing le bathar-bog saor Krita 2.9.8, Inkscape 0.91, Blender 2.71, GIMP 2.8.14, G'MIC 1.6.7 air Linux Mint 17.2

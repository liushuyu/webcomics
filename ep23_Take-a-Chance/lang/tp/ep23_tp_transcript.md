# Transcript of Pepper&Carrot Episode 23 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute tu wan: o jo e ken pona

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|1|False|esun Komona, tenpo suno luka tu li pini
sitelen|2|False|suli
sitelen|3|False|50 000 Komo
sitelen|4|False|pona pi jan Sapon
sitelen|5|False|len
sitelen|6|False|pona pi jan Sapon
sitelen|7|False|pilin
sitelen|8|False|pona pi jan Sapon
kalama|9|False|A N PA !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|sina pona, soweli Kawa o…
jan Pepa|2|False|sina sona e ni: mi wile kama wawa tawa wawa nasa la, mi pali mute…
jan Pepa|3|True|…mi kepeken nasin. mi kepeken lawa…
jan Pepa|4|False|…jan Sapon li kama suli wawa kepeken pali nasa la, ni li pona ala tawa pali mi.
jan Pepa|5|False|pona ala a.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kon sewi pi pali pona|2|True|toki!
kon sewi pi pali pona|3|False|o kama sona e mi:
kalama|1|False|Kiwen !|nowhitespace
kon sewi pi pali pona|4|False|mi kon sewi pi pali pona. esun mi li lon ale pi tenpo suno, li lon tenpo suno ale!*

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kon sewi pi pali pona|1|False|*taso ni li ken ala lon tenpo pi esun ala. sina ken ala wan e ni kepeken esun ante. pana wan ni li pana taso.
kon sewi pi pali pona|2|False|toki esun pi pona sina li tawa jan ale!
kon sewi pi pali pona|3|False|ma ale li kama sona e sina!
kon sewi pi pali pona|4|False|jan pali li pona e len sina, e linja lawa sina!
kon sewi pi pali pona|5|False|pona li kama kepeken tenpo ala!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kon sewi pi pali pona|1|False|sina sitelen e nimi sina lon noka pi lipu ni la, pona li kama tawa sina!
jan Pepa|2|False|o awen.
jan Pepa|4|True|nasin pi kon sewi li tan seme?!
jan Pepa|3|False|mi pilin ike mute, mi pilin anpa mute. tenpo ni sama la, sina kama tawa mi, li pona sewi e ike ale mi a?!
jan Pepa|5|False|ni ale li nasa tawa mi; nasa mute a!
jan Pepa|6|True|mi tawa, soweli Kawa o.
jan Pepa|7|False|tenpo pini la, ni li ken lawa e mi. taso tenpo ni la, mi sona e nasin ona! ona li ken nasa ala e mi.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
waso|1|True|Muu!
waso|2|True|Mu!
waso|3|True|Muuu!
waso|4|False|Muu!
jan Pepa|5|False|soweli Kawa o, mi pilin e ni: nasin ni la, mi kama jan suli.
jan Pepa|6|False|o weka tan nasin pi pali ala; o pilin pona tan pali wawa…
jan Pepa|7|False|…pona li tawa jan ante la, ni li ike ala tawa sina!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen|1|False|suli
sitelen|2|False|50 000 Komo
sitelen|3|False|pona pi jan Sapon
sitelen|4|False|len
sitelen|5|False|pona pi jan Sapon
jan Pepa|8|False|pona li kama tawa mi lon tenpo wan!
sitelen|6|False|pilin
sitelen|7|False|pona pi jan Sapon
kalama|9|False|Annnnttee !
sitelen|10|False|suli
sitelen|11|False|pona pi waso meli
sitelen|12|False|len
sitelen|13|False|pona pi waso meli
sitelen|14|False|pilin
sitelen|15|False|pona pi waso meli
sitelen toki|16|False|- PINI -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|tenpo 08/2017 - www.peppercarrot.com - musi sitelen & toki li tan jan David Revoy - ante toki pi toki pona li tan jan Ret Samys
mama|2|False|jan Alex Gryson en jan Calimeroteknik en jan Nicolas Artance en jan Valvin en jan Craig Maloney li lukin pona, li pona e toki jan, li pona e toki.
mama|4|False|musi ni li tan pali pi ma Elewa. pali ni li tan jan David Revoy. jan Craig Maloney li pana e pona tawa ona. jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson li pona e musi ni.
mama|5|False|ilo: ilo Krita 3.1.4 en ilo Inkscape 0.92dev li kepeken ilo Linux Mint 18.2 Cinnamon
mama|6|False|nasin pi lawa jo: Creative Commons Attribution 4.0
mama|8|False|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa lon www.patreon.com/davidrevoy
mama|7|False|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 879 li pana e mani la, lipu ni li lon:
mama|3|False|jan Calimeroteknik en Craig Maloney li pana e pona tawa nasin sitelen toki e pona tawa pali sitelen toki.

# Transcript of Pepper&Carrot Episode 22 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 22: Das Publikumsvoting

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bürgermeister von Komona|1|False|Unser großer Zauberwettbewerb kann endlich beginnen!
Schrift|2|False|Zauberwettbewerb

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bürgermeister von Komona|1|False|Und dank unserer genialen Ingenieure können Sie ebenfalls teilnehmen!
Bürgermeister von Komona|2|False|Ja genau, werfen Sie einen Blick auf diese kleinen technischen Wunderwerke, die Ihnen unsere Helferinnen und Helfer überreichen!
Bürgermeister von Komona|3|False|Mit dem grünen Knopf geben Sie einen Punkt an die Kandidatin, mit dem roten ziehen Sie einen ab!
Bürgermeister von Komona|4|True|„Und die Jury?“, höre ich Sie fragen
Bürgermeister von Komona|5|False|Keine Angst, wir haben an alles gedacht!
Bürgermeister von Komona|6|False|Jedes Jurymitglied hat eine Spezialknopf, um 100 Punkte zu geben oder 100 Punkte abzuziehen!
Pepper|7|False|Wow!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bürgermeister von Komona|5|False|… 50,000 Ko!
Bürgermeister von Komona|1|False|Und der Clou: die Punkte erscheinen direkt über dem Kopf der Teilnehmerin!
Bürgermeister von Komona|3|False|Die drei Hexen mit der höchsten Punktzahl kommen ins Finale!
Bürgermeister von Komona|4|True|Und nicht vergessen, der Gewinnerin des Hauptpreises winkt die fantastische Summe von …
Schrift|2|False|1337
<hidden>|0|False|Edit this one, all others are linked
Publikum|6|False|Klatsch

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Carrot|2|False|Tipp
Pepper|3|False|Ist das nicht eine tolle Erfindung, Carrot?
Pepper|4|True|Innovativ …
Pepper|5|True|lustig …
Pepper|6|True|demokratisch …
Pepper|7|False|… das perfekte System!
Pepper|8|True|Wir brauchen keine Fachleute mehr, um Qualität zu bewerten!
Pepper|9|False|Was für ein Fortschritt! Wir leben echt in einer tollen Zeit!
Bürgermeister von Komona|10|False|So, alle ihr Gerät bereit?
Publikum|11|False|Super!
Publikum|12|False|Ja!
Publikum|13|False|Jaha!
Publikum|14|False|Ja!
Publikum|15|False|Jaaa!
Bürgermeister von Komona|16|True|Gut!
Bürgermeister von Komona|17|True|Möge der Wettstreit
Bürgermeister von Komona|18|False|beginnen!!
<hidden>|0|False|Edit this one, all others are linked
Publikum|1|False|Klatsch

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bürgermeister von Komona|1|False|Und los geht es mit Kamille!
Geräusch|2|False|Djiiioo…
Kamille|3|False|SYLVESTRIS!
Geräusch|4|False|Knall!
Publikum|5|True|Tipp
Publikum|6|True|Tipp
Publikum|7|True|Tipp
Publikum|8|True|Tipp
Publikum|9|True|Tipp
Publikum|10|True|Tipp
Publikum|11|False|Tipp

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bürgermeister von Komona|2|False|Eine sehr ansehnliche Punktzahl für Kamille! Als nächste – Shichimi
Schrift|1|False|5861
Shichimi|4|False|LUX…
Geräusch|5|False|Fii iiiiisss|nowhitespace
Shichimi|6|False|MAXIMA!
Publikum|7|False|Aah!!
Publikum|8|False|Aua!!
Publikum|9|False|Meine Augen!!
Pepper|12|False|Carrot … gib ihr trotzdem einen grünen Daumen, sie ist schließlich unsere Freundin.
Carrot|13|False|Tap
Publikum|10|True|Buuuh!
Publikum|11|False|Buh!
Publikum|14|True|Buuuh!
Publikum|15|False|Buuu!
Bürgermeister von Komona|17|False|Ah, offenbar hat sich das Publikum von dieser „strahlenden“ Vorführung nicht blenden lassen. Kommen wir jetzt zu Spirulina!
Schrift|16|False|-42
Publikum|18|True|Buuuh!
Publikum|19|True|Buuuh!
Publikum|20|True|Buuuh!
Publikum|21|False|Buuh!
<hidden>|0|False|Edit this one, all others are linked
Publikum|3|False|Klatsch

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Spirulina|1|False|LIBERATUS KRAKENIS!
Geräusch|2|False|Bllluuh!|nowhitespace
Geräusch|3|False|Platsch|nowhitespace
Bürgermeister von Komona|5|False|Wunderbar! Kraftvoll! Spirulina liegt vorne! Coriander, Sie sind am Zug!
Schrift|4|False|6225
Spirulina & Durian|6|False|Tatsch
Coriander|8|False|MORTUS REDITUS!
Geräusch|9|False|Gruu wuuu !|nowhitespace
Bürgermeister von Komona|11|False|Oh, es sieht so aus, als seien Gerippe nicht mehr gefragt … und jetzt unsere liebe Saffron!
Schrift|10|False|2023
<hidden>|0|False|Edit this one, all others are linked
Publikum|7|False|Klatsch

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Saffron|1|False|Grrr! Auf keinen Fall schneide ich schlechter als Spirulina ab! Ich muss alles geben!
Saffron|2|False|Zurücktreten, Trüffel!
Geräusch|3|False|Fr rrshh !|nowhitespace
Geräusch|4|False|Fr rrshh !|nowhitespace
Geräusch|5|False|Crchh!
Trüffel|6|False|Miau!
Saffron|7|False|SPIRALIS
Saffron|8|False|FLAMA aaaa aaah!|nowhitespace
Geräusch|9|False|Fw wwuuuu sch !|nowhitespace
Geräusch|10|False|Flutsch!
Geräusch|11|False|Fs ssh !|nowhitespace
Geräusch|12|False|Plumps!
Saffron|13|False|?!!
Geräusch|14|False|Fs ssh !|nowhitespace
Geräusch|15|False|Fs ssh !|nowhitespace
Saffron|16|False|Auweeh! !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Saffron|1|False|Oh nein … wie peinlich
Saffron|3|False|?!
Schrift|4|True|14
Schrift|5|False|849
Pepper|6|False|Tipp
Lord Azeirf|7|True|Tipp
Lord Azeirf|8|True|Tipp
Lord Azeirf|9|False|Tipp
Schrift|10|True|18
Schrift|11|False|231
Bürgermeister von Komona|12|True|Bitte … ein wenig Anstand bitte!
Bürgermeister von Komona|13|False|Shichimi und Coriander sind ausgeschieden!
Bürgermeister von Komona|14|False|Kamille, Spirulina unnd Saffron haben sich fürs Finale qualifiziert!
<hidden>|0|False|Edit this one, all others are linked
Publikum|2|False|Klatsch

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bürgermeister von Komona|1|False|Meine Damen, bereit für eine weitere Herausforderung??
Bürgermeister von Komona|2|False|Sie sind am Zug!
Schrift|3|True|1 5|nowhitespace
Schrift|4|False|703
Schrift|5|True|19
Schrift|6|False|863
Schrift|7|True|1 3|nowhitespace
Schrift|8|False|614
Geräusch|10|False|Plumps
Pepper|11|False|Ich nehme alles zurück, was ich über das System gesagt habe …
Lord Azeirf|12|True|Tipp
Lord Azeirf|13|True|Tipp
Lord Azeirf|14|False|Tipp
Erzähler|15|False|- ENDE -
<hidden>|0|False|Edit this one, all others are linked
Publikum|9|False|Klatsch

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|05/2017 - www.peppercarrot.com - Illustration & Handlung : David Revoy - Deutsche Übersetzung: colognella
Impressum|2|False|Dialogverbesserungen: Nicolas Artance, Valvin, Craig Maloney.
Impressum|3|False|Basierend auf der Hereva-Welt von David Revoy mit Beiträgen von Craig Maloney. Korrekturen von Willem Sonke, Moini, Hali, CGand und Alex Gryson.
Impressum|4|False|Software : Krita 3.1.3, Inkscape 0.92.1 auf Linux Mint 18.1
Impressum|5|False|Lizenz : Creative Commons Namensnennung 4.0
Impressum|6|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Episode geht der Dank an die 864 Förderer:
Impressum|7|False|Auch Du kannst Pepper&Carrot unterstützen: www.patreon.com/davidrevoy

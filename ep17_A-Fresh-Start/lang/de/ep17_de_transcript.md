# Transcript of Pepper&Carrot Episode 17 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 17: Ein Neuanfang

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kümmel|1|False|Aber Pepper... bleib hier...
Pepper|2|False|NEIN! ICH GEHE!!!
Pepper|3|False|Ihr unterrichtet keine echte Zauberei! Ich gehe - zu den Hexen von Ah!
Geräusch|4|False|Wuu u s ch !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Also gut, auf geht's in das Land der untergehenden Monde
Pepper|2|False|Shichimi kann uns sagen, wie wir zu den Hexen von Ah kommen
Pepper|3|False|Carrot, gib mir Kompass und Karte: bei dem Nebel sehe ich nicht, wo es hingeht.
Pepper|5|True|MIST!
Pepper|6|True|Ein Sturm!
Pepper|7|False|FESTHALTEN!
Pepper|9|False|Oh nein !!!
Geräusch|8|False|BrrUuuuMmm! !|nowhitespace
Schrift|4|False|N

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|Krawumm! !|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|6|False|Aah! !|nowhitespace
Monster|5|False|Wah! !|nowhitespace
Pepper|1|False|Wir müssen jetzt ganz ruhig bleiben, Carrot...
Cayenne|2|True|"Ein böser Blick ist besser als ein dutzend schwacher Zaubersprüche! ...
Pepper|7|False|Ich hätte lieber ein paar Angriffszauber gelernt... aber was soll's...
Pepper|8|True|ohh je... keinen Besen und keine Ausrüstung,
Pepper|9|False|das wird eine langer Weg...
Cayenne|3|True|...starre sie nieder.
Cayenne|4|False|Kontrolliere sie! "

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kümmel|4|False|"Zu wissen, welche Pflanzen essbar sind, ist wie ein fertiger Zaubertrank gegen Hunger!"
Pepper|6|False|Wenn sie mir nur EINEN beigebracht hätte!
Pepper|2|True|Ich bin auch total ausgehungert, Carrot...
Pepper|3|False|Seit Tagen haben wir nichts mehr gegessen.
Carrot|1|False|Knurr
Pepper|5|True|Aber echte Zaubertränke?...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thymian|1|False|"Eine echte Hexe von Chaosâh braucht weder Karte noch Kompass. Eine sternenklare Nacht genügt!"
Pepper|3|True|Komm Carrot !
Pepper|4|False|Schau, wir sind da!
Pepper|2|False|... Ich hätte einen echten Kurs in Wahrsagerei bevorzugt!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|7|False|Lizenz : Creative Commons Namensnennung 4.0, Software: Krita, G'MIC, Inkscape on Ubuntu
Impressum|6|False|Basierend auf dem Hereva Universum von David Revoy mit Unterstützung von Craig Maloney. Korrekturen von Willem Sonke, Moini, Hali, CGand und Alex Gryson.
Erzähler|4|False|- ENDE -
Impressum|5|False|06/2016 - www.peppercarrot.com - Grafik & Handlung : David Revoy - Deutsche Übersetzung : Philipp Hemmer
Pepper|2|False|Jetzt kennst du die ganze Geschichte.
Shichimi|3|False|...und du sagst mir, dass du hier bist, weil sie dir nichts beigebracht haben?
Pepper|1|True|...und hier sind wir.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Episode geht der Dank an die 719 Förderer:
Impressum|2|False|Auch Du kannst Pepper&Carrot unterstützen: www.patreon.com/davidrevoy

# Transcript of Pepper&Carrot Episode 30 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute luka luka: wile pi pona luka

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|1|False|- PINI -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|5|True|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa. nimi sina li ken lon ni!
jan Pepa|3|True|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute.
jan Pepa|4|False|jan 973 li pana e mani la, lipu ni li lon!
jan Pepa|7|True|sina wile sona la, o lukin e lipu www.peppercarrot.com !
jan Pepa|6|True|ilo Patreon en ilo Tipeee en ilo PayPal en ilo Liberapay en ilo ante la, mi lon!
jan Pepa|8|False|sina pona!
jan Pepa|2|True|sina sona ala sona?
mama|1|False|tenpo September 03, 2019 musi sitelen & toki: jan David Revoy. open toki la, jan ni li lukin pona: jan Alina the Hedgehog en jan Craig Maloney en jan Jihoon Kim en jan Parnikkapore en jan Martin Disch en jan Nicolas Artance en jan Valvin. toki pona ante toki: jan Ret Samys. musi ni li tan pali pi ma Elewa mama: jan David Revoy. jan awen nanpa wan: jan Craig Maloney. jan pi toki sitelen: Craig Maloney en jan Nicolas Artance en jan Scribblemaniac en jan Valvin. jan pi pona pali: Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson . ilo: ilo Krita/4.2~git branchen ilo Inkscape 0.92.3 li kepeken ilo Kubuntu 18.04.2. nasin pi lawa jo: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.

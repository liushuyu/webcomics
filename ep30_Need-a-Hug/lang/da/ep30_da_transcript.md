# Transcript of Pepper&Carrot Episode 30 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 30: Brug for et kram

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|1|False|- SLUT -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|3. September 2019 Tegning, farver og manuskript: David Revoy . Gænlæsning af storyboard: Alina the Hedgehog, Craig Maloney, Jihoon Kim, Parnikkapore, Martin Disch, Nicolas Artance, Valvin. Dansk Version Oversættelse: Emmiline Alapetit e Rettelser: Rikke & Alexandre Alapetite Baseret på Hereva-universet Skabt af: David Revoy. Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Væktøj: Krita/4.2~git branch, Inkscape 0.92.3 on Kubuntu 18.04.2. Licens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|3|True|Du kan også blive tilhænger og få dit navn skrevet her!
Pepper|4|False|Pepper & Carrot er fri, open-source og sponsoreret af sine læsere.
Pepper|7|True|Denne episode blev støttet af 973 tilhængere!!
Pepper|6|True|Gå på www.peppercarrot.com og få mere information!
Pepper|8|False|Vi er på Patreon, Tipeee, PayPal, Liberapay … og andre!
Pepper|2|True|Tak!
Credits|1|False|Vidste I det?
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.

# Transcript of Pepper&Carrot Episode 02 [kr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
제목|1|False|제2 화: 무지개 물약

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
소리|5|True|쪼롱
소리|6|True|쪼롱
소리|7|False|쪼롱
글자|1|True|경고
글자|2|True|마녀사유지
글자|3|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
소리|7|False|톡|nowhitespace
글자|1|False|FIRE D
글자|2|False|DEEP OCEAN
글자|3|False|VIOLE(N)T
글자|4|False|ULTRA BLUE
글자|5|False|PIN
글자|6|False|MSON
글자|8|False|NATURE
글자|9|False|YELLOW
글자|10|False|ORANGE TOP
글자|11|False|FIRE DANCE
글자|12|False|DEEP OCEAN
글자|13|False|VIOLE(N)T
글자|14|False|ULTRA BLUE
글자|15|False|META PINK
글자|16|False|MAGENTA X
글자|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
소리|1|True|쪼록
소리|2|True|쪼록
소리|3|False|쪼록

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
소리|1|True|꿀꺽
소리|2|False|꿀꺽
소리|3|True|꿀꺽
소리|4|False|꿀꺽
소리|5|True|꿀꺽
소리|6|False|꿀꺽
소리|9|False|읍
소리|7|False|풉!
소리|8|False|주르르륵

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
소리|1|True|뽀글
소리|2|False|뽀글
소리|4|True|퍽|nowhitespace
소리|3|True|철
소리|6|True|퍽|nowhitespace
소리|5|True|철
소리|8|False|퍽|nowhitespace
소리|7|True|철

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
크레딧|1|True|이 웹툰은 오픈소스이며 이번 화는 아래 주소에서 21명의 후원을 받아 제작되었습니다.
크레딧|2|False|www.patreon.com/davidrevoy
크레딧|3|False|아래 분들에게 감사드립니다.
크레딧|4|False|GNU/리눅스에서 Krita로 제작

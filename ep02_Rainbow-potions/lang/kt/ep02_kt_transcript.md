# Transcript of Pepper&Carrot Episode 02 [kt]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Zixa 2 : Korfiulibeem

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Glu
Sound|6|True|Glu
Sound|7|False|Glu
Writing|1|True|Iyela
Writing|3|False|Yasegaxo
Writing|2|True|DIWIK
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|tock|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Glu
Sound|2|True|Glu
Sound|3|False|Glu

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Glu
Sound|2|False|Glu
Sound|3|True|Glu
Sound|4|False|Glu
Sound|5|True|Glu
Sound|6|False|Glu
Sound|20|False|m|nowhitespace
Sound|19|True|m|nowhitespace
Sound|18|True|M|nowhitespace
Sound|7|True|S|nowhitespace
Sound|8|True|P|nowhitespace
Sound|9|True|l|nowhitespace
Sound|10|False|erp !|nowhitespace
Sound|11|True|S|nowhitespace
Sound|12|True|S|nowhitespace
Sound|13|True|S|nowhitespace
Sound|14|True|P|nowhitespace
Sound|15|True|l|nowhitespace
Sound|16|True|o|nowhitespace
Sound|17|False|p|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|B
Sound|2|True|lup|nowhitespace
Sound|3|True|B
Sound|4|False|lup|nowhitespace
Sound|7|True|utc|nowhitespace
Sound|6|True|l|nowhitespace
Sound|5|True|p
Sound|10|True|tc|nowhitespace
Sound|9|True|lu|nowhitespace
Sound|8|True|p
Sound|13|False|tc|nowhitespace
Sound|12|True|lu|nowhitespace
Sound|11|True|p

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Bata wimbra tir ton Open-Source ise gan 21 webesik bata zixa zo fukorlayar dene :
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Grewá va :
Credits|4|False|fait avec Krita sur GNU/Linux

# Transcript of Pepper&Carrot Episode 02 [ja]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|エピソード 2: 虹の薬

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|トポ
Sound|6|True|トポ
Sound|7|False|トポ
Writing|1|True|WARNING
Writing|3|False|PROPERTY
Writing|2|True|WITCH
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|カタン|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|ザバ
Sound|2|True|ザバ
Sound|3|False|ザバ

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|3|True|ごく
Sound|4|False|ごく
Sound|5|True|がぶ
Sound|6|False|がぶ
Sound|1|True|ぐび
Sound|2|False|ぐび
Sound|20|False|... !
Sound|19|True|ぐぐ|nowhitespace
Sound|18|True|ん
Sound|7|False|ぶーっ !
Sound|8|True|で|nowhitespace
Sound|9|True|ろ|nowhitespace
Sound|10|True|-|nowhitespace
Sound|11|False|ん|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|と
Sound|2|True|ぽ|nowhitespace
Sound|3|True|と
Sound|4|False|ぽ|nowhitespace
Sound|6|True|ちゃ|nowhitespace
Sound|5|True|ぴ
Sound|8|True|ちゃ|nowhitespace
Sound|7|True|ぴ
Sound|10|False|ちゃ|nowhitespace
Sound|9|True|ぴ

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|このウェブコミックはオープンソースです このエピソードは以下の21人のパトロンによる支援を受けました
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Many thanks to
Credits|4|False|made with Krita on GNU/Linux

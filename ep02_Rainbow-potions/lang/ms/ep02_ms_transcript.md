# Transcript of Pepper&Carrot Episode 02 [ms]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episod 2 : Posyen pelangi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Glup
Sound|6|True|Glup
Sound|7|False|Glup
Writing|1|True|AMARAN
Writing|3|False|AHLI SIHIR
Writing|2|True|RUMAH
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|tok|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Glup
Sound|2|True|Glup
Sound|3|False|Glup

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Guk
Sound|2|False|Guk
Sound|3|True|Guk
Sound|4|False|Guk
Sound|5|True|Guk
Sound|6|False|Guk
Sound|20|False|g h|nowhitespace
Sound|19|True|r|nowhitespace
Sound|18|True|U|nowhitespace
Sound|7|True|B|nowhitespace
Sound|8|True|l|nowhitespace
Sound|9|True|e|nowhitespace
Sound|10|False|r g h !|nowhitespace
Sound|11|True|U|nowhitespace
Sound|12|True|w|nowhitespace
Sound|13|True|e|nowhitespace
Sound|14|True|r|nowhitespace
Sound|15|True|g|nowhitespace
Sound|16|True|h|nowhitespace
Sound|17|False|h|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|B
Sound|2|True|lup|nowhitespace
Sound|3|True|B
Sound|4|False|lup|nowhitespace
Sound|7|True|ap|nowhitespace
Sound|6|True|pl|nowhitespace
Sound|5|True|s
Sound|10|True|ap|nowhitespace
Sound|9|True|pl|nowhitespace
Sound|8|True|s
Sound|13|False|ap|nowhitespace
Sound|12|True|pl|nowhitespace
Sound|11|True|s
Credits|14|False|- TAMAT - Julai 2014 - www.peppercarrot.com - Senilukis & Senario : David Revoy - Terjemahan Bahasa Melayu : muhdnurhidayat

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Komik web ini bersumber terbuka dan episod ini mendapat dana daripada 21 patron di
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Jutaan terima kasih kepada
Credits|4|False|dibuat dengan Krita di GNU/Linux

# Transcript of Pepper&Carrot Episode 02 [hu]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cím|1|False|2. rész: Szivárvány főzetek
<hidden>|2|False|Paprika
<hidden>|3|False|Sárgarépa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hang|5|True|Glup
Hang|6|True|Glup
Hang|7|False|Glup
Írta|1|True|VIGYÁZAT!
Írta|3|False|BIRTOK
Írta|2|True|BOSZORKÁNY
Írta|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hang|7|False|tok|nowhitespace
Írta|1|False|FIRE D
Írta|2|False|DEEP OCEAN
Írta|3|False|VIOLE(N)T
Írta|4|False|ULTRA BLUE
Írta|5|False|PIN
Írta|6|False|MSON
Írta|8|False|NATURE
Írta|9|False|YELLOW
Írta|10|False|ORANGE TOP
Írta|11|False|FIRE DANCE
Írta|12|False|DEEP OCEAN
Írta|13|False|VIOLE(N)T
Írta|14|False|ULTRA BLUE
Írta|15|False|META PINK
Írta|16|False|MAGENTA X
Írta|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hang|1|True|Glup
Hang|2|True|Glup
Hang|3|False|Glup

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hang|1|True|Gulp
Hang|2|False|Gulp
Hang|3|True|Gulp
Hang|4|False|Gulp
Hang|5|True|Gulp
Hang|6|False|Gulp
Hang|20|False|m|nowhitespace
Hang|19|True|m|nowhitespace
Hang|18|True|M|nowhitespace
Hang|7|True|S|nowhitespace
Hang|8|True|P|nowhitespace
Hang|9|True|l|nowhitespace
Hang|10|False|urp !|nowhitespace
Hang|11|True|S|nowhitespace
Hang|12|True|S|nowhitespace
Hang|13|True|S|nowhitespace
Hang|14|True|P|nowhitespace
Hang|15|True|l|nowhitespace
Hang|16|True|a|nowhitespace
Hang|17|False|t|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hang|1|True|B
Hang|2|True|lup|nowhitespace
Hang|3|True|B
Hang|4|False|lup|nowhitespace
Hang|7|True|up|nowhitespace
Hang|6|True|pl|nowhitespace
Hang|5|True|s
Hang|10|True|up|nowhitespace
Hang|9|True|pl|nowhitespace
Hang|8|True|s
Hang|13|False|up|nowhitespace
Hang|12|True|pl|nowhitespace
Hang|11|True|s

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Készítők|1|True|A képregény nyílt forráskódú, és ezt a részt 21 pártfogó támogatta
Készítők|2|False|www.patreon.com/davidrevoy
Készítők|3|False|Köszönet:
Készítők|4|False|Kritával és GNU/Linuxszal készült

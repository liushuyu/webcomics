# Transcript of Pepper&Carrot Episode 02 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa tu: telo pi kule mute

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|6|True|Telo
kalama|7|True|Telo
kalama|8|False|Telo
sitelen|1|True|O LUKIN!
sitelen|3|False|LI LON
sitelen|2|True|JAN PI
sitelen|4|False|33
sitelen|5|False|WAWA NASA

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|7|False|Pana|nowhitespace
sitelen|1|False|SELI T
sitelen|2|False|TELO ANPA
sitelen|3|False|UTALA(SO)
sitelen|4|False|LASO WAWA
sitelen|5|False|LOJ
sitelen|6|False|E PIPI
sitelen|8|False|KASI
sitelen|9|False|JELO
sitelen|10|False|JELO LOJE A
sitelen|11|False|SELI TAWA
sitelen|12|False|TELO ANPA
sitelen|13|False|UTALA(SO)
sitelen|14|False|LASO WAWA
sitelen|15|False|WALO LOJE
sitelen|16|False|LOJE WALO
sitelen|17|False|LOJE PIPI

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|True|Telo
kalama|2|True|Telo
kalama|3|False|Telo

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|True|Moku
kalama|2|False|Moku
kalama|3|True|Moku
kalama|4|False|Moku
kalama|5|True|Moku
kalama|6|False|Moku
kalama|20|False|m|nowhitespace
kalama|19|True|m|nowhitespace
kalama|18|True|M|nowhitespace
kalama|7|True|W|nowhitespace
kalama|8|True|E|nowhitespace
kalama|9|True|k|nowhitespace
kalama|10|False|aa!|nowhitespace
kalama|11|True|A|nowhitespace
kalama|12|True|N|nowhitespace
kalama|13|True|P|nowhitespace
kalama|14|True|a|nowhitespace
kalama|15|True|K|nowhitespace
kalama|16|True|o|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|True|T
kalama|2|True|elo|nowhitespace
kalama|3|True|T
kalama|4|False|elo|nowhitespace
kalama|7|True|ko|nowhitespace
kalama|6|True|ka|nowhitespace
kalama|5|True|no
kalama|10|True|ko|nowhitespace
kalama|9|True|ka|nowhitespace
kalama|8|True|no
kalama|13|False|ko|nowhitespace
kalama|12|True|ka|nowhitespace
kalama|11|True|no

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|True|sitelen ni li nasin jo pi jan ale. jan 21 li pana e mani la, lipu wan ni li lon.
mama|2|False|www.patreon.com/davidrevoy
mama|3|False|pona mute tan jan ni:
mama|4|False|pali kepeken ilo Krita lon ilo GNU/Linux

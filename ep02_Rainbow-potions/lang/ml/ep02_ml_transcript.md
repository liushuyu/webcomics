# Transcript of Pepper&Carrot Episode 02 [ml]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|എപ്പിസോഡ് 2: മഴവില്‍ തൈലം

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|ഗ്ള്ഗ്
Sound|6|True|ഗ്ള്ഗ്
Sound|7|False|ഗ്ള്ഗ്
Writing|1|True|മുന്നറിയിപ്പ്
Writing|3|False|സ്ഥലം
Writing|2|True|മന്ത്രവാദിനികളുടെ
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|ഠക്ക്|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|ഗ്ളഗ്
Sound|2|True|ഗ്ളഗ്
Sound|3|False|ഗ്ളഗ്

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|ഗ്ളഗ്
Sound|2|False|ഗ്ളഗ്
Sound|3|True|ഗ്ളഗ്
Sound|4|False|ഗ്ളഗ്
Sound|5|True|ഗ്ളഗ്
Sound|6|False|ഗ്ളഗ്
Sound|20|False|മ്|nowhitespace
Sound|19|True|മ|nowhitespace
Sound|18|True|മ|nowhitespace
Sound|7|False|ബ്വ!
Sound|8|True|സ്
Sound|9|True|പ്ല|nowhitespace
Sound|10|False|ട്|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|ബ
Sound|2|True|ർ|nowhitespace
Sound|3|True|ബ
Sound|4|False|ർ|nowhitespace
Sound|7|True|പ്പ്|nowhitespace
Sound|6|True|പ്ല|nowhitespace
Sound|5|True|സ്
Sound|10|True|പ്പ്|nowhitespace
Sound|9|True|പ്ല|nowhitespace
Sound|8|True|സ്
Sound|13|False|പ്പ്|nowhitespace
Sound|12|True|പ്ല|nowhitespace
Sound|11|True|സ്

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|ഇത് ഒരു സ്വതന്ത്ര വെബ്കോമിക്കാണ് ഈ എപ്പിസോഡിന് എന്റെ 21 രക്ഷാധികാരികളാണ് ധനസഹായം തന്നത്
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|വളരെ നന്നിയുണ്ട്
Credits|4|False|made with Krita on GNU/Linux

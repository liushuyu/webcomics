# Transcript of Pepper&Carrot Episode 02 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 2 : Les potions arc-en-ciel

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|5|True|Glou
Son|6|True|Glou
Son|7|False|Glou
Écriture|1|True|DANGER
Écriture|3|False|PROPRIÉTÉ PRIVÉE
Écriture|2|True|SORCIÈRE
Écriture|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|7|False|tock|nowhitespace
Écriture|1|False|FIRE D
Écriture|2|False|DEEP OCEAN
Écriture|3|False|VIOLE(N)T
Écriture|4|False|ULTRA BLUE
Écriture|5|False|PIN
Écriture|6|False|MSON
Écriture|8|False|NATURE
Écriture|9|False|YELLOW
Écriture|10|False|ORANGE TOP
Écriture|11|False|FIRE DANCE
Écriture|12|False|DEEP OCEAN
Écriture|13|False|VIOLE(N)T
Écriture|14|False|ULTRA BLUE
Écriture|15|False|META PINK
Écriture|16|False|MAGENTA X
Écriture|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|True|Glou
Son|2|True|Glou
Son|3|False|Glou

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|True|Glou
Son|2|False|Glou
Son|3|True|Glou
Son|4|False|Glou
Son|5|True|Glou
Son|6|False|Glou
Son|20|False|m|nowhitespace
Son|19|True|m|nowhitespace
Son|18|True|M|nowhitespace
Son|7|True|S|nowhitespace
Son|8|True|P|nowhitespace
Son|9|True|l|nowhitespace
Son|10|False|eurp !|nowhitespace
Son|11|True|S|nowhitespace
Son|12|True|S|nowhitespace
Son|13|True|S|nowhitespace
Son|14|True|P|nowhitespace
Son|15|True|l|nowhitespace
Son|16|True|o|nowhitespace
Son|17|False|p|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|True|B
Son|2|True|lup|nowhitespace
Son|3|True|B
Son|4|False|lup|nowhitespace
Son|7|True|utch|nowhitespace
Son|6|True|lo|nowhitespace
Son|5|True|p
Son|10|True|tch|nowhitespace
Son|9|True|lou|nowhitespace
Son|8|True|p
Son|13|False|tch|nowhitespace
Son|12|True|lou|nowhitespace
Son|11|True|p

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|True|Cette BD est open-source et cet épisode a été sponsorisé par 21 mécènes sur
Crédits|2|False|www.patreon.com/davidrevoy
Crédits|3|False|Merci à :
Crédits|4|False|fait avec Krita sur GNU/Linux

# Transcript of Pepper&Carrot Episode 04 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 4: Genistreg

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|1|False|Natten inden eliksirkonkurrencen, kl. 02:00 ...
Pepper|2|True|Endelig... Med denne eliksir, vil ingen, ikke en gang Safran, nå mig til sokkeholderne!
Pepper|3|False|Jeg skal bare lige afprøve den ...
Pepper|4|False|CAAAAAAAA~ ~AAAROTT!!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|14|False|KATTEMUMS
Pepper|1|False|Carrot?
Pepper|2|False|Utroligt!... Heller ikke under sengen?
Pepper|4|False|Carrot?
Pepper|5|False|Carrot?!
Pepper|6|False|Carrot?!!!
Pepper|3|False|Carrot!
Pepper|7|False|Jeg troede ikke jeg skulle komme dertil og bruge dette kneb...
Pepper|8|False|Carrot!
Lyd|9|True|Crrr
Lyd|10|True|Crrr
Lyd|11|True|Crrr
Lyd|12|True|Crrr
Lyd|13|False|Crrr
Lyd|15|True|Shhh
Lyd|16|True|Shhh
Lyd|17|True|Shhh
Lyd|18|True|Shhh
Lyd|19|False|Shhh
Lyd|21|False|Shhh Shhh Shhh Shhh Shhh
Lyd|20|False|Crrr Crrr Crrr Crrr Crrr
Lyd|23|False|Crrr Crrr Crrr Crrr Crrr
Lyd|24|False|Shhh Shhh Shhh Shhh Shhh
Carrot|25|False|Rumle rumle
Lyd|26|False|Fiiuuuu!
Carrot|22|False|O Sole Mjjaaavvv!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Århh, Carrot! Din søde lille mis! Du kom helt af dig selv
Pepper|2|False|Og lige når jeg har brug for min favoritmedhjælper
Pepper|3|True|Daaadaaaa! Her er mit mesterværk
Pepper|4|False|Geni-eliksiren!
Pepper|5|False|Tag bare en tår ...
Pepper|6|False|... og hvis det virker, skulle du gerne få en genial idé

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Pok
Pepper|3|False|Wauw... Carrot! Det er helt utroligt...
Pepper|5|False|Du... Du skriver?
Pepper|6|False|E?
Pepper|7|False|Energi?
Pepper|8|False|Evig?
Pepper|9|False|Elske?
Lyd|4|False|Shrrrrrrr
Lyd|2|False|Shrr

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Hvad?
Pepper|2|False|Emalje?
Pepper|3|False|Element?
Pepper|4|False|Kom så, Carrot, Streng dig lidt an, Det giver ingen mening
Pepper|5|False|... Det giver virkelig ingen mening...
Pepper|6|True|Arghhhh!!!
Pepper|7|False|Der er aldrig noget, der fungerer for mig!
Pepper|8|True|Nåh...
Pepper|9|False|Jeg kan stadig nå at lave en bedre
Lyd|10|False|Kli... iii... irr!
Pepper|11|False|... God nat Carrot...
Fortæller|12|False|Fortsættes...
Credits|13|False|Oversættelse: Alexandre & Rikke Alapetite
Credits|14|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo og særligt tak til Amireeti for hjælp med engelske rettelser !
Credits|1|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Denne episode kunne ikke være blevet til uden støtte fra de 156 tilhængere:
Credits|6|False|https://www.patreon.com/davidrevoy
Credits|5|False|Støt næste episode af Pepper&Carrot, hver en krone gør en forskel:
Credits|9|False|Værktøj: Denne episode er 100% designet med fri software: Krita, G'MIC, Blender, GIMP på Ubuntu Gnome (GNU/Linux)
Credits|8|False|Open source: al kildematerialet, skrifttyper, og lagdelte filer kan downloades i høj opløsning fra den officielle hjemmeside
Credits|7|False|Licens: Creative Commons Kreditering til 'David Revoy' Du kan dele og tilpasse materialet til alle formål, også kommercielle
Carrot|4|False|Tak !

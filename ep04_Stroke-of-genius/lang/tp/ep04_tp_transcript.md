# Transcript of Pepper&Carrot Episode 04 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa tu tu: sona pi kama wawa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|1|False|utala pi pali telo li kama la, ilo tenpo li pana e nanpa tu lon tenpo pimeja
jan Pepa|2|True|pini la ... telo ni la, jan ala, en jan Sapon ala kin, li ken wawa sama wawa mi !
jan Pepa|3|False|a, mi wile kepeken ona lon seme ...
jan Pepa|4|False|SOWELI KAWA OOOOOO !!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen|14|False|MOKU SOWELI
jan Pepa|1|False|soweli Kawa o ?
jan Pepa|2|False|seme! ona li lon ala anpa supa kin
jan Pepa|4|False|soweli Kawa o?
jan Pepa|5|False|soweli Kawa o?!
jan Pepa|6|False|soweli Kawa o?!!!
jan Pepa|3|False|soweli Kawa o!
jan Pepa|7|False|mi wile ala wile kepeken nasin ni ... ?
jan Pepa|8|False|soweli Kawa o!
kalama|9|True|Poki
kalama|10|True|Poki
kalama|11|True|Poki
kalama|12|True|Poki
kalama|13|False|Poki
kalama|15|True|Kkko
kalama|16|True|Kkko
kalama|17|True|Kkko
kalama|18|True|Kkko
kalama|19|False|Kkko
kalama|21|False|Kkko Kkko Kkko Kkko Kkko
kalama|20|False|Poki Poki Poki Poki Poki
kalama|23|False|Poki Poki Poki Poki Poki
kalama|24|False|Kkko Kkko Kkko Kkko Kkko
soweli Kawa|25|False|mumoku
kalama|26|False|Taawwaaa !
soweli Kawa|22|False|O Sole Muuuu !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|soweli Kawa suwi o! sina kama kepeken lawa sina taso anu seme
jan Pepa|2|False|tenpo pi kama sina li tenpo pona la, mi wile e pona sina
jan Pepa|3|True|o lukin! pali mi ni li pona mute mute
jan Pepa|4|False|telo pi sona wawa
jan Pepa|5|False|o moku lili e ona
jan Pepa|6|False|ona li pali la, sina kama jo wawa e sona wawa

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|Jo
jan Pepa|3|False|AAA ... soweli Kawa o! ni li pona a... sitelen ni li lon ...
jan Pepa|5|False|sina ken... sitelen?
jan Pepa|6|False|nimi seme ?
jan Pepa|7|False|nimi "en" ?
jan Pepa|8|False|anu nimi "e" ?
jan Pepa|9|False|anu nimi "esun" ?
kalama|4|False|Ssssssitelen
kalama|2|False|Ssitelen

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|ni ante li seme?
jan Pepa|2|False|nimi "mun" ?
jan Pepa|3|False|nimi "moku" ?
jan Pepa|4|False|soweli Kawa o! toki nimi mi li kama pini. ni li nasa mute!
jan Pepa|5|False|... ni li jo e kon ala a ...
jan Pepa|6|True|iiikeeee !!!
jan Pepa|7|False|tenpo ala la pali mi li pali!
jan Pepa|8|True|a ...
jan Pepa|9|False|pali telo la tenpo li lon
kalama|10|False|PA...KA...LA !
jan Pepa|11|False|soweli Kawa o, lape pona
sitelen toki|12|False|lipu kama la, toki ni li awen...
mama|13|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo sin la, jan Amireeti li pona e toki Inli la, lipu ni li pona !
mama|1|False|jan Pepa&soweli Kawa li nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 156 li pana e mani la, lipu ni li lon:
mama|6|False|https://www.patreon.com/davidrevoy
mama|5|False|o pana e pona tawa pali ni. mani wan li tawa wan pi sitelen ni la, mani li pona mute tawa jan Pepa&soweli Kawa !
mama|9|False|ilo : pali pi wan ni li kepeken taso ilo pi nasin jo pi jan ale. ilo pi nasin Free(libre) en ilo pi nasin Open-source li ilo Krita, li ilo G'MIC, li ilo Blender, li ilo GIMP lon ilo Ubuntu Gnome (GNU/Linux)
mama|8|False|nasin Open-source la, sina ken sitelen e sitelen suli pi lipu mama mute e nasin sitelen, li ken jo e ona, li ken esun e ona, li ken ante e ona, li ken ante toki e ona, li ken pali ante e ona
mama|7|False|nasin lawa Creative Commons Attribution la, o nimi e jan 'David Revoy'. sina ken ante, li ken musi, li ken pana, li ken esun, li ken pali ante mute e ni
soweli Kawa|4|False|sina pona !

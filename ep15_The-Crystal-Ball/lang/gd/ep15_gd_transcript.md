# Transcript of Pepper&Carrot Episode 15 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 15: Am bàla-criostail

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|1|False|- Deireadh na sgeòil -
Urram|2|False|03/2016 – Obair-ealain ⁊ sgeulachd: David Revoy

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 686 pàtran a thug taic dhan eapasod seo:
Urram|2|True|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd airson an ath-eapasod air
Urram|3|False|https://www.patreon.com/davidrevoy
Urram|4|False|Ceadachas : Creative Commons Attribution 4.0 Bun-tùs : ri fhaighinn air www.peppercarrot.com Bathar-bog : chaidh 100% dhen eapasod seo a tharraing le bathar-bog saor Krita 2.9.11, Inkscape 0.91 air Linux Mint 17

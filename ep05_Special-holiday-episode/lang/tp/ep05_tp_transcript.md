# Transcript of Pepper&Carrot Episode 05 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa luka: wan pona pi tenpo lete pona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen|2|False|lete
sitelen|1|True|tenpo
sitelen|3|False|pona

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 170 li pana e mani la, lipu ni li lon:
mama|4|False|https://www.patreon.com/davidrevoy
mama|3|False|o pana e pona tawa pali ni. mani wan li tawa lipu pi sitelen ni la, mani li pona mute tawa jan Pepa&soweli Kawa
mama|7|False|ilo : pali pi lipu ni li kepeken taso ilo pi nasin jo pi jan ale. ilo pi nasin Free(libre) en ilo pi nasin Open-source li ilo Krita, li ilo G'MIC, li ilo Blender, li ilo GIMP lon ilo Ubuntu Gnome (GNU/Linux)
mama|6|False|nasin Open-source la, sina ken sitelen e sitelen suli pi lipu mama mute e nasin sitelen, li ken jo e ona, li ken esun e ona, li ken ante e ona, li ken ante toki e ona, li ken pali ante e ona
mama|5|False|nasin lawa Creative Commons Attribution la, o nimi e jan 'David Revoy'. sina ken ante, li ken musi, li ken pana, li ken esun, li ken pali ante mute e ni
mama|2|False|Addison Lewis - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Aleš Tomeček - Alexander Bülow Tomassen - Alexander Sopicki - Alex Lusco - Alex Silver - Alex Vandiver - Alfredo - Ali Poulton (Aunty Pol) - Allan Zieser Andreas Rieger - Andrej Kwadrin - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous Antan Karmola - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans - Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brett Smith - Brian Behnke - carlos levischi - Charlotte Lacombe-bar Chris Radcliff - Chris Sakkas - Christophe Carré - Christopher Bates - Clara Dexter - Colby Driedger - Conway Scott Smith Cyrille Largillier - Cyril Paciullo - Damien - Daniel - Daniel Björkman - david - Dmitry - Donald Hayward - Eitan Goldshtrom Enrico Billich--epsilon--Eric Schulz - Frederico - Garret Patterson - GreenAngel5 - Guillaume - Gustav Strömbom Guy Davis - Happy Mimic - Helmar Suschka - Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - James Frazier - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jeffrey Schneider - Jessey Wright - John - John - Jónatan Nilsson - Jonathan Leroy Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen - Kathryn Wuerstl Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy Streisel - marcus - Martin Owens Mary Brownlee - Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates Michael Gill - Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicola Angel - Nicolae Berbece - Nicole Heersema - Noble Hays - Nora Czaykowski - Nyx Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul - Pavel Semenov Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta - Ret Samys - rictic RJ van der Weide - Roberto Zaghis - Roman - Rumiko Hoshino - Rustin Simons - Sally Bridgewater - Sami T - Samuel Mitson Scott Russ - Sean Adams Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster Simon Isenberg - Sonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo tar8156 - TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - tree - Vespertinus - Victoria - Vlad Tomash Westen Curry - Xavier Claude - Yalyn Vinkindo - Алексей - Глеб Бузало - 獨孤欣 & 獨弧悦

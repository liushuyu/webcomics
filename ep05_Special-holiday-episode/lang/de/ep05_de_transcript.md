# Transcript of Pepper&Carrot Episode 05 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 5: Spezielle Festtagsepisode

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Schrift|2|False|Fest
Schrift|1|True|Frohes

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|Pepper&Carrot ist komplett frei, Open Source und mit Hilfe von Spenden über Patreon finanziert. Diese Episode wurde durch 170 Unterstützer ermöglicht :
Impressum|4|False|https://www.patreon.com/davidrevoy
Impressum|3|False|Unterstütze das Projekt! Schon ein Dollar pro Episode kann Pepper&Carrot eine Menge helfen.
Impressum|7|False|Tools : Diese Episode wurde mit den kostenlosen Open Source Tools Krita, G'MIC und Blender auf Ubuntu Gnome (GNU/Linux) erstellt
Impressum|6|False|Open Source : Hochauflösende Quelldateien mit Ebenen und Schriften zum Herunterladen, verändern, verkaufen, übersetzen, u.s.w.
Impressum|5|False|Lizenz : Creative Commons Namensnennung als 'David Revoy' Du darfst verändern, teilen, verkaufen, abgeleitete Werke kreieren, u.s.w.
Impressum|2|False|Addison Lewis - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Aleš Tomeček - Alexander Bülow Tomassen - Alexander Sopicki - Alex Lusco - Alex Silver - Alex Vandiver - Alfredo - Ali Poulton (Aunty Pol) - Allan Zieser Andreas Rieger - Andrej Kwadrin - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous Antan Karmola - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans - Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brett Smith - Brian Behnke - carlos levischi - Charlotte Lacombe-bar Chris Radcliff - Chris Sakkas - Christophe Carré - Christopher Bates - Clara Dexter - Colby Driedger - Conway Scott Smith Cyrille Largillier - Cyril Paciullo - Damien - Daniel - Daniel Björkman - david - Dmitry - Donald Hayward - Eitan Goldshtrom Enrico Billich--epsilon--Eric Schulz - Frederico - Garret Patterson - GreenAngel5 - Guillaume - Gustav Strömbom Guy Davis - Happy Mimic - Helmar Suschka - Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - James Frazier - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jeffrey Schneider - Jessey Wright - John - John - Jónatan Nilsson - Jonathan Leroy Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen - Kathryn Wuerstl Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy Streisel - marcus - Martin Owens Mary Brownlee - Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates Michael Gill - Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicola Angel - Nicolae Berbece - Nicole Heersema - Noble Hays - Nora Czaykowski - Nyx Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul - Pavel Semenov Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta - Ret Samys - rictic RJ van der Weide - Roberto Zaghis - Roman - Rumiko Hoshino - Rustin Simons - Sally Bridgewater - Sami T - Samuel Mitson Scott Russ - Sean Adams Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster Simon Isenberg - Sonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo tar8156 - TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - tree - Vespertinus - Victoria - Vlad Tomash Westen Curry - Xavier Claude - Yalyn Vinkindo - Алексей - Глеб Бузало - 獨孤欣 & 獨弧悦

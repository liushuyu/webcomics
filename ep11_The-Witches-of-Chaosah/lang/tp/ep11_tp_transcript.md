# Transcript of Pepper&Carrot Episode 11 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa luka luka wan: jan pi wawa nasa pi kulupu Pakalaa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kajen|1|False|jan Pepa o, sina ike e nimi pi kulupu Pakalaa.
jan Tume|2|False|jan Kajen li toki pona. jan ale pi wawa nasa pi kulupu Pakalaa o lawa, o anpa e jan ante, o kama e pilin suli tawa jan ante.
jan Kumin|3|False|... taso sina pana a e telo pona e pan suwi tawa kon ike pi nasin Pakalaa*...
sitelen|4|False|* o lukin e lipu nanpa luka tu wan: jan Pepa li sike sin e suno
jan Pepa|5|True|taso...
jan Pepa|6|False|...jan sona o...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kajen|4|False|O TOKI ALA !
jan Kumin|9|False|mu-muuu!
jan Tume|1|True|jan Pepa o, pali sina li pali wawa. taso kulupu ni la, sina taso li jan sin.
jan Tume|2|False|pali wile kulupu li ni: sina kama jan pi wawa ike pi kulupu Pakalaa.
jan Pepa|3|False|taso... mi wile ala e wawa ike. ike li... ike li nasin pi pona ala--
jan Kajen|5|True|... mi ken weka e wawa ale sina!
jan Kajen|6|False|wawa weka la, sina kama jan lili pi sona ala tan ma Pini Soweli sama tenpo pini.
jan Tume|7|False|jan Kumin li tawa poka sina. tan ni la, ona li pana e pona tawa pali sina, li toki e sin pi pali sina tawa mi.
kalama|8|False|Ko!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kumin|1|False|jan lawa sin pi ma Aken li pona mute tawa jan ona. ni li ike. jan o pilin anpa tan jan lawa ona.
jan Kumin|2|False|pali sina nanpa wan li ni: o tawa jan lawa. o ante e pilin ona. tan ni la, sina ken lawa pona e ona.
jan Kumin|3|False|jan wawa pi kulupu Pakalaa li ken lawa e jan lawa!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|ni li jan lawa ?...
jan Pepa|2|False|...ona li lili a ?!
jan Pepa|3|False|ona en mi li lili sama...
jan Kumin|4|False|sina awen tan seme?! o pali! o pini e lape ona, o anpa e ona, o lawa e ona!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|Wawa
jan Pepa|2|False|sina en mi li sama lili...
jan Pepa|3|True|li lili sama...
jan Pepa|4|True|li pilin sama pi wan taso...
jan Pepa|5|False|...li wile e nasin ante lon nasin ante ala.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Tume|6|False|jan Pepa o, pali sina kama li pali mute a...
mama|8|False|tenpo 09/2015 - musi sitelen & toki li tan jan David Revoy - ante toki li tan jan Ret Samys
sitelen toki|7|False|- PINI -
kalama|1|False|Ko!
jan Pepa|2|False|?!
kalama|3|False|seli !
jan Tume|4|True|pali nanpa wan
jan Tume|5|True|LI IKE ALE!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 502 li pana e mani la, lipu ni li lon:
mama|3|False|https://www.patreon.com/davidrevoy
mama|2|True|sina ken pana kin e pona tawa lipu kama pi jan Pepa&soweli Kawa:
mama|4|False|nasin lawa : Creative Commons Attribution 4.0 mama pali li lon lipu www.peppercarrot.com ilo : sitelen pi lipu ni li kepeken taso ilo pi nasin jo pi jan ale. ilo pi nasin Libre li ilo Krita 2.9.7, li ilo G'MIC 1.6.5.2, li ilo Inkscape 0.91 lon ilo Linux Mint 17

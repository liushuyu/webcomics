# Transcript of Pepper&Carrot Episode 29 [eo]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Ĉapitro 29a: Detruanto de Mondoj

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstro|2|True|Finfine!
Monstro|3|False|interdimensia rompo!
Monstro|4|False|Eble kaŭzita de iu granda kosma evento!
Monstro|5|True|Kresku! Kresku, eta breĉo!
Monstro|6|False|Kaj malkovru tiun novan mondon por SKLAVIGI kaj MASTRI!
Monstro|7|True|Ho…
Monstro|8|False|Tio ja estas interesa.
Monstro|9|False|Ĉi tiu devas esti mia bonŝanca tago…
Rakontanto|1|False|Dume, en alia dimensio…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstro|1|False|Dimensio loĝata de inteligentaj estuloj!
Monstro|2|False|Pli rapide, eta breĉo!
Monstro|3|False|Kresku! Muhaha hahaha!
Pipro|4|True|Fiu!
Pipro|5|False|Kia bona festo, miaj amikoj!
Koriandro|6|False|He, Pipro, Safrano kaj mi ĵus konsciis, ke ni tute ne scias, kiel via Ĥaosaha magio funkcias.
Safrano|7|False|Ĝi estas la ega mistero. Ĉu vi dirus al ni pli pri ĝi?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|Haha, ĝi estas io malfacile klarigebla.
Pipro|2|True|Nu, oni povus diri, ke ĝi estas bazita sur la scio de la fundamentaj leĝoj de ĥaosaj sistemoj…
Pipro|3|False|…el la pli etaj ĝis la pli grandaj.
Koriandro|4|False|Nu, tio certe klarigas la aferon.
Pipro|7|False|Atendu, lasu al mi montri al vi simplan ekzemplon.
Pipro|8|False|Nur donu al mi sekundon kun iom da malnova Ĥaosaha saĝeco…
Pipro|9|False|Trovis ĝin!
Sono|5|True|RAT
Sono|6|False|GRAT
Pipro|10|True|Ĉu vi vidas ĉi tiun manĝostangeton, kiu estis fiksa inter tiuj pavimŝtonoj?
Pipro|11|False|Prenante ĝin mi certe evitis, ke iu piedpremu ĝin.
Pipro|12|True|Eta, bona ŝanĝo en la granda ĥaosa sistemo de ekzistado povas havi egajn konsekvencojn.
Pipro|13|False|Tiel funkcias Ĥaosaho!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ŝiŝimio|1|False|Ho aĉ!
Koriandro|2|False|Fiiii! Tiu aĵo estis en la buŝo de iu!
Safrano|3|False|Ha ha! Impresa kiel ĉiam, Pipro!
Koriandro|4|False|Bone Pipro, dankon por ĉi tiu… “klarigo”.
Koriandro|5|True|Estas tempo por iri dormi nun, ĉu ne?
Koriandro|6|False|Kaj por ke iu lavu siajn manojn.
Pipro|7|False|He! Atendu!
Sono|8|False|Bat!
Sono|9|True|Tuk!
Sono|10|False|Tuk!
Sono|11|False|PAF !|nowhitespace
Karoĉjo|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sono|1|False|Ssss!
Sono|2|False|Puŭf!
Sono|3|False|Tok!
Sono|4|False|K RAŜ !|nowhitespace
Sono|5|False|Ŝrr uŭf…|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|We have a patch for the artwork in case your text is too short or is not easily split in two words. Ask David or Midgard for more information. Look at the Korean version for an example.
<hidden>|0|False|NOTE FOR TRANSLATORS
Monstro|7|False|ATAKEN!!!
Monstro|6|True|Muhaha HA HA! Finfine!
Monstro|9|False|!?
Writing|1|True|TENEJO
Writing|2|False|DE FAJRAĴOJ
Sono|3|False|Ŝr rUuf ! !|nowhitespace
Sono|4|False|Ŝhr r ! !|nowhitespace
Sono|5|False|BUUM!
Sono|8|False|Fizzz! !|nowhitespace

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sono|3|False|B oOM !|nowhitespace
Sono|2|False|B UŬ M!|nowhitespace
Sono|1|False|K RA K !|nowhitespace
Sono|4|False|Fizzz! !|nowhitespace
Sono|5|False|PuŬ F !|nowhitespace
Pipro|6|True|Ne zorgu, Karoĉjo.
Pipro|7|False|Tio eble estas nur ĉar iuj ankoraŭ festas.
Pipro|8|False|Nu, dormu bone!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstro|5|False|...
Kumino|1|False|Ĉu vere? Ŝi povis kaŭzi tian grandan ĉen-reagon, sen eĉ konscii pri tio?
Kajeno|2|False|Sendube.
Timiano|3|False|Damoj, mi pensas, ke nia Pipro estas fine preta!
Sono|6|False|Fvip!
Sono|4|False|Fŝhhh h ! !|nowhitespace
Rakontanto|7|False|- LA TRIOPO DE LA KRONADO DE KORIANDRO, LA FINO -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|5|True|Vi ankaŭ povas iĝi mecenato de Pepper&Carrot kaj havi vian nomon ĉi tie!
Pipro|3|True|Pepper&Carrot estas tute libera, malfermitkoda kaj subtenita danke al la mecenateco de siaj legantoj.
Pipro|4|False|Pri ĉi tiu rakonto, dankon al la 960 mecenantoj!
Pipro|7|True|Vidu www.peppercarrot.com por pli da informo!
Pipro|6|True|Ni estas en Patreon, Tipeee, PayPal, Liberapay ...kaj en multaj pli!
Pipro|8|False|Dankon!
Pipro|2|True|Ĉu vi sciis?
Atribuintaro|1|False|Aprilo 25a, 2019 Arto kaj scenaro: David Revoy. Beta-legantoj: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Esperanta versio Traduko: Jorge Maldonado Ventura . Bazita sur la universo de Hereva Kreinto: David Revoy. Ĉefa fleganto: Craig Maloney. Verkistoj: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korektistoj: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Programaro: Krita 4.1.5~appimage, Inkscape 0.92.3 sur Kubuntu 18.04.1. Licenco: Krea Komunaĵo Atribuite 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.

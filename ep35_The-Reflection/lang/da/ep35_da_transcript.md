# Transcript of Pepper&Carrot Episode 35 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for how they can be generated without retyping them and also for more information
and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Episode 35: Spejlbilledet

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Åh, du er vågen!
Pepper|2|False|Går det? Er det for koldt?
Pepper|3|True|Jeg er ked af det, men jeg har snart ikke mere Rea,
Pepper|4|False|og min aura er for svag til at beskytte os begge fra kulden i denne højde.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Jeg er lige ved at miste min hyper-hastighed,
Pepper|2|True|og dragen og dens pilot har fulgt efter os i timevis...
Pepper|3|False|Det bliver ikke svært for dem at indhente os, når vi sænker farten.
Pepper|4|True|Sikke en idiot jeg har været!
Pepper|5|False|Jeg var ellers sikker på, at de ville have givet op for flere timer siden!
Pepper|6|False|Hvis bare jeg kunne slippe af med dem.
Pepper|7|True|NEJ. Jeg skal slippe af med dem!
Pepper|8|False|Før de fanger os!
Pepper|9|True|Men jeg er...
Pepper|10|True|...så...
Pepper|11|False|...træt...
Pepper|12|True|UPS!
Pepper|13|False|Tag dig sammen, Pepper!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Så du det?
Arra|2|True|Ja. Forfølgelsen er snart ovre.
Arra|3|False|Det varer ikke længe, før hun løber tør for kræfter.
Torreya|4|False|Og så angriber vi.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Bu U M !
Pepper|2|False|HVAD?!
Pepper|3|False|De brød gennem lydmuren?!
Pepper|4|False|! ! !
Pepper|5|False|WUuus H ! !!
Pepper|6|False|Grrr!
Pepper|7|False|Hold godt fast, Carrot!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|POKKERS!
Pepper|2|False|HVAD SKAL JEG GØRE?!
Pepper|3|False|Åh!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grotten ved Hornsøen!
Pepper|2|False|Carrot, det er vores chance!
Pepper|3|False|Jeg kender den grotte.
Pepper|4|True|Det er en lang snoet gang, der ender i et kollapset hulrum.
Pepper|5|True|Stalaktitterne fra sammenbruddet skaber en mur af pigge. Det er en dødsfælde!
Pepper|6|True|Der er en lille åbning, der lige netop er stor nok til, at man kan komme ud,
Pepper|7|False|men dragen vil ikke kunne standse i tide og vil ramme væggen med fuld fart! Muhahaha!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Perfekt! De følger efter os!
Pepper|2|False|Vi kommer snart af med dem!
Pepper|3|True|Jeg glæder mig til at se det!
Pepper|4|False|Muhahaha!
Pepper|5|False|! ! !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Hvad er jeg dog blevet til?
Pepper|2|True|En morder?
Pepper|3|False|En ond heks?
Pepper|4|False|! ! !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|SWooosh!
Pepper|2|True|NEJ!
Pepper|3|False|Det dér er ikke mig!
Pepper|4|True|STOOOOP!!!
Pepper|5|False|JEG OVERGIVER MIG!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Du er med på, at det ikke ender godt for dig, hvis du overgiver dig til Mester Wasabi, ikke?
Pepper|2|False|Det ved jeg, men jeg føler, at der stadig er mulighed for at gå i dialog; at få hende til at forstå.
Pepper|3|True|Når alt kommer til alt, er intet uopretteligt.
Pepper|4|False|Det har jeg lært af en god veninde.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tak fordi du lod Carrot hente skiftetøj til mig.
Torreya|2|True|Det var så lidt.
Torreya|3|False|Du gjorde arbejdet nemmere, så det var det mindste, jeg kunne gøre.
Torreya|4|True|Men du ved, du gjorde Wasabi virkelig vred.
Torreya|5|True|Hun kræver at alt er til perfektion. Det ødelagde du med dine urenheder, dit tøj, og din entré.
Torreya|6|False|Derudover angreb du hende! Hun lader dig ikke slippe let fra det.
Pepper|7|False|Det ved jeg.
Torreya|8|True|Nå, jeg har brug for en lille lur.
Torreya|9|True|Vi tager afsted, når jeg vågner.
Torreya|10|False|Du kan gøre, hvad du vil, men gå ikke for langt væk...
Lyd|11|False|Splash
Torreya|12|False|Sikke en underlig heks.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Uanset risici og farer...
Pepper|2|True|...vil jeg fra nu af forblive tro mod mig selv.
Pepper|3|False|Det lover jeg!
Fortæller|4|False|FORTSÆTTES...

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Den 18 juni 2021 Tegning og manuskript: David Revoy. Genlæsning i beta-versionen: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Dansk Version Oversættelse: Emmiline Alapetite Rettelser: Rikke & Alexandre Alapetite Baseret på Hereva-universet Skabt af: David Revoy. Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Værktøj: Krita 4.4.2, Inkscape 1.1 on Kubuntu Linux 20.04 Licens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|True|Vidste I det?
Pepper|3|True|Pepper & Carrot er fri, open-source og sponsoreret af sine læsere.
Pepper|4|False|Denne episode blev støttet af 1054 tilhængere!
Pepper|5|True|Du kan også blive tilhænger og få dit navn skrevet her!
Pepper|6|True|Vi er på Patreon, Tipeee, PayPal, Liberapay ... og andre!
Pepper|7|True|Gå på www.peppercarrot.com og få mere information!
Pepper|8|False|Tak!
<hidden>|9|True|You can also translate this page if you want.
<hidden>|10|True|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

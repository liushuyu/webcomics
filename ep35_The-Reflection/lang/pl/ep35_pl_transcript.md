# Transcript of Pepper&Carrot Episode 35 [pl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for how they can be generated without retyping them and also for more information
and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Odcinek 35: Refleksja

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Obudziłeś się!
Pepper|2|False|Wszystko dobrze? Nie jest ci zimno?
Pepper|3|True|Niedługo skończy mi się Rea,
Pepper|4|False|a sama aura to zbyt mało, by ochronić nas przed chłodem.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Moja pręd-kość niebawem zmaleje,
Pepper|2|True|a ten smoczy pilot leci za nami od kilku godzin.
Pepper|3|False|Nie będzie miał najmniejszego proble-mu z dogonieniem nas.
Pepper|4|True|Jaka ja jestem głupia!
Pepper|5|False|Byłam pewna, że do tej pory odpuszczą!
Pepper|6|False|Gdybym tylko mogła ich zgubić.
Pepper|7|True|NIE! Muszę ich zgubić,
Pepper|8|False|zanim nas dogonią!
Pepper|9|True|Ale co zrobić, gdy jestem
Pepper|10|True|taka
Pepper|11|False|zmęczona.
Pepper|12|True|UPS!
Pepper|13|False|Nie zasypiaj, Pepper!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Widziałeś to?
Arra|2|True|Tak. Pościg niebawem się skończy.
Arra|3|False|Za chwilę stracą całą moc.
Torreya|4|False|Zaatakujemy, gdy tylko zwolnią.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Bu U M !
Pepper|2|False|CO?!
Pepper|3|False|Przekroczyli barierę dźwięku?!
Pepper|4|False|! ! !
Pepper|5|False|Wuuuusz ! !!
Pepper|6|False|Grrr!
Pepper|7|False|Trzymaj się, Carrot!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|O nie!
Pepper|2|False|Co teraz?!
Pepper|3|False|Och!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Jaskinia nad Rogatym Jeziorem!
Pepper|2|False|Carrot, to nasza ostatnia szansa!
Pepper|3|False|Znam to miejsce!
Pepper|4|True|To długi, kręty korytarz, który kończy się zapadliskiem.
Pepper|5|True|Stalaktyty w tej zawalonej części ułożyły się w kolczastą ścianę. To śmiertelna pułapka!
Pepper|6|True|W sklepieniu jest mały otwór, którym uda nam się przecisnąć,
Pepper|7|False|ale smok nie zdoła tak szybko skręcić i uderzy w nią z pełną prędkością! Muhahaha!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Świetnie! Lecą za nami!
Pepper|2|False|Zaraz się ich pozbędziemy!
Pepper|3|True|Nie mogę się doczekać!
Pepper|4|False|Muhahaha!
Pepper|5|False|! ! !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Chwila. Czy stałam się
Pepper|2|True|morder-czynią?
Pepper|3|False|Złą wiedźmą?
Pepper|4|False|! ! !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|WSsiuu!
Pepper|2|True|NIE!
Pepper|3|False|Wcale taka nie jestem!
Pepper|4|True|STAAAAĆ!!!
Pepper|5|False|PODDAJĘ SIĘ!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Wiesz, że poddanie się mistrzyni Wasabi, nie skończy się dla ciebie dobrze?
Pepper|2|False|Wiem, ale nadal sądzę, że uda się jakoś do niej dotrzeć, przekonać ją.
Pepper|3|True|Nie ma rzeczy, których nie da się naprawić.
Pepper|4|False|Nauczyła mnie tego stara przyjaciółka.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Dzięki, że pozwoliłaś mu na dostarczenie mi czystych ubrań.
Torreya|2|True|Nie ma za co.
Torreya|3|False|Ułatwiłaś mi pracę, więc zwracam przysługę.
Torreya|4|True|Wiesz, że naprawdę wpieniłaś Wasabi?
Torreya|5|True|Ona oczekuje perfekcji, a ty zaburzyłaś to swoim wyglądem, brudem i zachowaniem.
Torreya|6|False|Poza tym zaatakowałaś ją! Nie odpuści ci.
Pepper|7|False|Wiem.
Torreya|8|True|Dobra, muszę się przespać.
Torreya|9|True|Odlatujemy, jak tylko wstanę.
Torreya|10|False|Możesz robić, co chcesz, bylebyś nie uciekła.
Sound|11|False|Plusk
Torreya|12|False|Dziwna z niej wiedźma.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Nieważne, na jakie zagrożenia się natknę,
Pepper|2|True|zostanę prawdziwą sobą.
Pepper|3|False|Obiecuję!
Narrator|4|False|CIĄG DALSZY NASTĄPI…

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|18 czerwca 2021 Rysunki i scenariusz: David Revoy. Poprawki skryptu: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Wersja polska Tłumaczenie: Sölve Svartskogen. Korekta i kontrola jakości: Besamir. Oparto na uniwersum Herevy Autor: David Revoy. Pomocnik: Craig Maloney. Pisarze: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korekta: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.4.2, Inkscape 1.1 na Kubuntu Linux 20.04 Licencja: Creative Commons Uznanie Autorstwa 4.0. www.peppercarrot.com
Pepper|2|True|Wiedziałeś, że
Pepper|3|True|Pepper&Carrot jest całkowicie darmowy, liberalny, open-source'owy oraz wspierany przez naszych czytelników?
Pepper|4|False|Za ten odcinek dziękujemy 1054 patronom!
Pepper|5|True|Ty również możesz zostać patronem Pepper&Carrot i znaleźć się na tej liście.
Pepper|6|True|Jesteśmy na Patreonie, Tipeee, PayPal, Liberapay i wielu innych!
Pepper|7|True|Wejdź na www.peppercarrot.com, by dowiedzieć się więcej!
Pepper|8|False|Dziękujemy!
<hidden>|9|True|You can also translate this page if you want.
<hidden>|10|True|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

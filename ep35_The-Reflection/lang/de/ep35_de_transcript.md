# Transcript of Pepper&Carrot Episode 35 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for how they can be generated without retyping them and also for more information
and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Episode 35: Das Spiegelbild

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Oh, du bist wach!
Pepper|2|False|Geht es dir gut? Nicht zu kalt?
Pepper|3|True|Tut mir leid, mein Rea ist bald aufgebraucht
Pepper|4|False|und meine Aura ist zu dünn, um uns vor der Kälte hier oben zu schützen.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ich verliere bald mein Hypertempo
Pepper|2|True|und diese Pilotin und ihr Drache haben stundenlang mit uns mitgehalten.
Pepper|3|False|Sobald wir langsamer werden, erwischen sie uns.
Pepper|4|True|Wie konnte ich nur so dumm sein!
Pepper|5|False|Ich dachte, sie würden schon längst aufgegeben haben.
Pepper|6|False|Wenn ich sie nur abschütteln könnte.
Pepper|7|True|NEIN. Ich muss sie loswerden!
Pepper|8|False|Bevor sie uns erwischen!
Pepper|9|True|Aber ich bin auch...
Pepper|10|True|...sehr...
Pepper|11|False|...müde...
Pepper|12|True|UPS!
Pepper|13|False|Reiß dich zusammen, Pepper!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Hast du das gesehen?
Arra|2|True|Ja. Diese Verfolgungsjagd ist bald zu Ende.
Arra|3|False|Ihre Kräfte werden sie bald verlassen.
Torreya|4|False|Wir greifen an, sobald sie am Ende ist.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|Bum M !
Pepper|2|False|WAS?!
Pepper|3|False|Sie haben die Schallmauer durchbrochen?!
Pepper|4|False|! ! !
Pepper|5|False|WUusc H ! !!
Pepper|6|False|Grrr!
Pepper|7|False|Festhalten, Carrot!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|MIST!
Pepper|2|False|WAS KANN ICH TUN?!
Pepper|3|False|Oh!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Die Höhle am See der Hörner!
Pepper|2|False|Carrot, das ist unsere Chance!
Pepper|3|False|Ich kenne diese Höhle.
Pepper|4|True|Es ist ein langer Tunnel mit einem eingestürzten Raum am Ende.
Pepper|5|True|Die Stalaktiten in diesem Teil bilden eine Wand aus Stacheln! Es ist eine Todesfalle!
Pepper|6|True|Es gibt eine kleine Öffnung, gerade groß genug für uns,
Pepper|7|False|aber dieser Drache wird nicht rechtzeitig bremsen können und voll gegen die Wand fliegen! Muhahaha!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Perfekt! Sie folgen uns!
Pepper|2|False|Bald haben wir uns ihnen entledigt.
Pepper|3|True|Ich kann es kaum erwarten!
Pepper|4|False|Muhahaha!
Pepper|5|False|! ! !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Moment! Was bin ich geworden?
Pepper|2|True|Eine Mörderin?
Pepper|3|False|Eine gemeine Hexe?
Pepper|4|False|! ! !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|WUusch!
Pepper|2|True|NEIN!
Pepper|3|False|Das bin nicht ich!
Pepper|4|True|STOOOPP!!!
Pepper|5|False|ICH GEBE AUF!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Du weißt schon, dass es nicht gut für dich enden wird, dich Herrin Wasabi zu ergeben?
Pepper|2|False|Ich weiß, aber ich glaube, es ist möglich, zu ihr durchzudringen—um sie verstehen zu lassen.
Pepper|3|True|Schließlich lässt sich alles wiedergutmachen.
Pepper|4|False|Das habe ich von einer alten Freundin gelernt.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Danke, dass du Carrot erlaubt hast, frische Kleider für mich zu holen.
Torreya|2|True|War mir ein Vergnügen.
Torreya|3|False|Du hast unseren Auftrag einfacher gemacht, das ist das Mindeste was ich tun kann.
Torreya|4|True|Aber weißt du, Wasabi ist wirklich sauer.
Torreya|5|True|Sie verlangt Perfektion. Du hast das gestört, mit deinem Dreck, deiner Kleidung und deinem Auftritt.
Torreya|6|False|Und du hast sie an-gegriffen! Sie wird das nicht durchgehen lassen.
Pepper|7|False|Ich weiß.
Torreya|8|True|Wie auch immer, ich muss eine Runde schlafen.
Torreya|9|True|Wir gehen, wenn ich aufwache.
Torreya|10|False|Mach inzwischen was du willst, aber geh nicht zu weit weg...
Geräusch|11|False|Pflatsch
Torreya|12|False|Was für eine sonderbare Hexe.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ab jetzt, trotz allen Risiken und Gefahren,
Pepper|2|True|werde ich mir treu bleiben.
Pepper|3|False|Versprochen!
Erzähler|4|False|FORTSETZUNG FOLGT...

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|18. Juni 2021 Illustration & Handlung: David Revoy. Beta-Leser: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Deutsche Version Übersetzung: Martin Disch, Ret Samys. Basierend auf der Herva-Welt Erstellung: David Revoy. Hauptbetreuer: Craig Maloney. Autoren: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korrektur: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.4.2, Inkscape 1.1 auf Kubuntu Linux 20.04 Lizenz: Creative Commons Namensnennung 4.0. www.peppercarrot.com
Pepper|2|True|Wusstest du schon?
Pepper|3|True|Pepper&Carrot ist vollständig frei(libre), Open Source und finanziert durch Spenden von Lesern.
Pepper|4|False|Für diese Episode danken wir 1054 Gönnern!
Pepper|5|True|Du kannst auch Gönner von Pepper&Carrot werden und deinen Namen hier lesen!
Pepper|6|True|Wir sind auf Patreon, Tipeee, PayPal, Liberapay ...und mehr!
Pepper|7|True|Geh auf www.peppercarrot.com für mehr Informationen!
Pepper|8|False|Dankeschön!
<hidden>|9|True|You can also translate this page if you want.
<hidden>|10|True|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

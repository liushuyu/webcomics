# Transcript of Pepper&Carrot Episode 35 [ru]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for how they can be generated without retyping them and also for more information
and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|False|Эпизод 35: Отражение

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|True|О, ты проснулся!
Перчинка|2|False|Ты в порядке? Не слишком холодно?
Перчинка|3|True|Извини, моя Ри скоро закончится,
Перчинка|4|False|и моя аура слишком тонкая, чтобы защитить нас от холода.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|True|Я вот-вот лишусь своей гиперскорости,
Перчинка|2|True|а дракон с его пилотом гонятся за нами много часов.
Перчинка|3|False|У них не будет никаких проблем поймать нас как только мы замедлимся.
Перчинка|4|True|Я была уверена, что они сдались бы через несколько часов!
Перчинка|5|False|Если бы только я могла сбросить их с хвоста.
Перчинка|6|False|НЕТ. Я должна избавиться от них!
Перчинка|7|True|Прежде чем они схватят нас!
Перчинка|8|False|Но я так...
Перчинка|9|True|...сильно...
Перчинка|10|True|...устала...
Перчинка|11|False|УУПС!
Перчинка|12|True|Держи себя в руках, Перчинка!
Перчинка|13|False|Какой идиоткой я была!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Ты видел это, да?
Arra|2|True|Дa. Эта погоня скоро закончится.
Arra|3|False|Совсем скоро её силы будут исчерпаны
Torreya|4|False|Мы атакуем как только они у нее кончатся.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|БуУ М !
Перчинка|2|False|ЧТО?!
Перчинка|3|False|Они сломали звуковой барьер?!
Перчинка|4|False|! ! !
Перчинка|5|False|ВУуухх ! !!
Перчинка|6|False|Гррр!
Перчинка|7|False|Держись, Морквик!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|True|БЛИН!
Перчинка|2|False|ЧТО ДЕЛАТЬ?!
Перчинка|3|False|О!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|False|Пещера Озера Рогов!
Перчинка|2|False|Морквик, это наш шанс!
Перчинка|3|False|Я знаю эту пещеру.
Перчинка|4|True|Это длинный извилистый коридор, который заканчивается обрушившимся залом.
Перчинка|5|True|Сталактиды в этой рухнувшей секции сложились в стену шипов. Это смертельная ловушка!
Перчинка|6|True|Есть небольшое отверстие достаточно широкое, что мы протиснулись,
Перчинка|7|False|но этот дракон не успеет остановиться и на всей скорости ударится об стену! Мухахаха!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|False|Отлично! Они следуют за нами!
Перчинка|2|False|Мы скоро избавимся от них.
Перчинка|3|True|Я не могу дождаться, когда увижу это!
Перчинка|4|False|Мухахаха!
Перчинка|5|False|! ! !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|True|Стоп! В кого я превратилась?
Перчинка|2|True|В убийцу?
Перчинка|3|False|В подлую ведьму?
Перчинка|4|False|! ! !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|ВЖуух!
Перчинка|2|True|НЕТ!
Перчинка|3|False|Это не я!
Перчинка|4|True|СТООООП!!!
Перчинка|5|False|Я СДАЮСЬ!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Ты знаешь, что, сдавшись Мастеру Васаби, ничем хорошим для тебя это не закончится?
Перчинка|2|False|Я знаю, но я все еще чувствую, есть способ достучаться до нее – заставить ее понять.
Перчинка|3|True|В конце концов, нет ничего непоправимого.
Перчинка|4|False|Я узнала это от старого друга.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|False|Спасибо тебе, что разрешила Морквику принести мне свежую одежду.
Torreya|2|True|Не за что
Torreya|3|False|Ты сделала нашу работу проще, так что это меньшее, что я могла сделать.
Torreya|4|True|Но знаешь, ты очень расстроила Васаби.
Torreya|5|True|Она требует совершенства.Ты нарушила это своей грязью, своей одеждой, и своим прибытием.
Torreya|6|False|И ты напала на неё! Она не будет снисходительна к тебе.
Перчинка|7|False|Я знаю
Torreya|8|True|В любом случае, мне нужно немного вздремнуть.
Torreya|9|True|Мы полетим когда я проснусь.
Torreya|10|False|Делай что хочешь, но не уходи далеко...
Sound|11|False|Хлюп
Torreya|12|False|Какая странная ведьма.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|True|Теперь, несмотря на риски и опасности,
Перчинка|2|True|Я останусь тем, кто я есть.
Перчинка|3|False|Я обещаю!
Narrator|4|False|ПРОДОЛЖЕНИЕ СЛЕДУЕТ...

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Перчинка|1|False|18 июня, 2021 Арт и сценарий: David Revoy. Бета читатели: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Русская версия Перевод: Alexander Chumovitskiy . Основано на вселенной Эревы Автор: David Revoy. Ведущий мейнтейнер: Craig Maloney. Писатели: Craig Maloney, Nartance, Scribblemaniac, Valvin. Корректура: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Програмное обеспечение: Krita 4.4.2, Inkscape 1.1 on Kubuntu Linux 20.04 Лицензия: Creative Commons Attribution 4.0. www.peppercarrot.com
Перчинка|2|True|А вы знаете?
Перчинка|3|True|Pepper&Carrot полностью бесплатный(либре), открытый и спонсируется благодаря патронажу самих читателей.
Перчинка|4|False|За этот эпизод поблагодарите 1054 патронов!
Перчинка|5|True|Вы тоже можете стать патроном Pepper&Carrot и увидеть свое имя здесь!
Перчинка|6|True|Мы есть на Patreon, Tipeee, PayPal, Liberapay ...и не только!
Перчинка|7|True|Посетите www.peppercarrot.com для дополительной информации!
Перчинка|8|False|Спасибо!
<hidden>|9|True|You can also translate this page if you want.
<hidden>|10|True|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

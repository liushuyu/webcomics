# Transcript of Pepper&Carrot Episode 35 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for how they can be generated without retyping them and also for more information
and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|lipu nanpa mute luka luka luka: sitelen telo sama

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|a, sina pini e lape!
jan Pepa|2|False|sina pona anu seme? lete ni li ike ala anu seme?
jan Pepa|3|True|mi pakala. wawa Lonaa mi li kama wawa ala
jan Pepa|4|False|wawa selo suno mi li lili taso, li ken ala awen e mi tu tan lete ni.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|tawa suli li kama pini kin.
jan Pepa|2|True|akesi sewi en jan ona li alasa awen e mi lon tenpo mute.
jan Pepa|3|False|mi lili e tawa la, ona li ken jo e mi.
jan Pepa|4|True|mi en sona mi li ike a!
jan Pepa|5|False|pilin pini mi la, ona li pini e wile alasa lon tenpo pini mute!
jan Pepa|6|False|mi wile weka tan ona, taso mi ken ala.
jan Pepa|7|True|ALA. mi o weka !
jan Pepa|8|False|ante la, ona li jo e mi!
jan Pepa|9|True|taso, kin la, mi pilin ...
jan Pepa|10|True|...lape...
jan Pepa|11|False|...mute...
jan Pepa|12|True|PAKALA!
jan Pepa|13|False|o awen wawa, jan Pepa o!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Toleja|1|False|sina lukin e ni, anu seme?
akesi Awa|2|True|lukin. alasa li kama pini lon tenpo lili.
akesi Awa|3|False|wawa ona li ken ala awen lon tenpo.
jan Toleja|4|False|wawa li weka la, mi utala.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|KaLAMA!
jan Pepa|2|False|SEME?!
jan Pepa|3|False|tawa ona li wawa sama tawa kalama anu seme?!
jan Pepa|4|False|! ! !
jan Pepa|5|False|KOoon n ! !!
jan Pepa|6|False|ike a!
jan Pepa|7|False|o awen, soweli Kawa o!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|PAKALA!
jan Pepa|2|False|MI O PALI SEME?!
jan Pepa|3|False|a!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|lupa li lon poka pi telo Palisa Lawa!
jan Pepa|2|False|soweli Kawa o, ken ni li pona!
jan Pepa|3|False|mi sona e lupa ni.
jan Pepa|4|True|ona li nasin tomo, li kama pakala lon pini ona.
jan Pepa|5|True|kiwen pakala la, palisa kiwen li weka tan sewi, li kama sinpin. moli li tan palisa a!
jan Pepa|6|True|mi tu taso li ken weka tan lupa lili lon ni.
jan Pepa|7|False|taso akesi sewi li ken ala pini e tawa, li tawa sinpin moli kepeken wawa mute! aaa a a a a a!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|pona a! ona li lon monsi mi!
jan Pepa|2|False|mi weka e ona lon tenpo lili.
jan Pepa|3|True|mi wile lukin e ni a!
jan Pepa|4|False|aaa a a a a!
jan Pepa|5|False|! ! !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|o awen! mi kama seme?
jan Pepa|2|True|jan moli anu seme?
jan Pepa|3|False|jan ike pi wawa nasa anu seme?
jan Pepa|4|False|! ! !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|SSiike!
jan Pepa|2|True|ALA!
jan Pepa|3|False|ni li mi ala!
jan Pepa|4|True|O PIIIINII!!!
jan Pepa|5|False|MI ANPA!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Toleja|1|False|sina anpa tawa jan sewi Wasapi la, ni li kama ike tawa sina. sina sona ala sona?
jan Pepa|2|False|mi sona. taso mi pilin kin e ni: mi ken toki tawa ona. mi en ona li pana e pilin la, ona li sona pona.
jan Pepa|3|True|suli la, jan li ken pona e ijo ale.
jan Pepa|4|False|mi kama sona e ni tan jan pona mi.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|soweli Kawa li kama e len sin tawa mi. sina ken e ni la, sina pona.
jan Toleja|2|True|sina pona kin.
jan Toleja|3|False|pali mi li kama pali lili tan pona sina la, mi wile pana e pona tawa sina kin.
jan Toleja|4|True|taso jan Wasapi li pilin ike tan sina. sina o sona.
jan Toleja|5|True|ona li wile e pona nanpa wan. jaki sina en len sina en kama ike sina la, sina nasa e ni.
jan Toleja|6|False|sin la, sina utala e ona! ona li wile ala pali e lili taso tawa sina.
jan Pepa|7|False|mi sona.
jan Toleja|8|True|tenpo ni la, mi wile lape lili.
jan Toleja|9|True|mi pini e lape la, mi ale li tawa.
jan Toleja|10|False|sina ken pali e ale. taso o weka ala...
kalama|11|False|Tttello
jan Toleja|12|False|jan ni pi wawa nasa li nasa.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|ike en pakala li ken kama la,
jan Pepa|2|True|mi awen e nasin mi lon tenpo ale.
jan Pepa|3|False|mi toki wawa e wile ni!
sitelen toki|4|False|TOKI NI LI AWEN...

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|tenpo June 18, 2021 musi sitelen & toki: jan David Revoy. jan pi lukin pona: jan Arlo James Barnes en jan Craig Maloney en jan Karl Ove Hufthammer en jan Martin Disch en jan Nicolas Artance en jan Parnikkapore en jan Valvin. toki pona ante toki: jan Ret Samys. jan pi lukin pona: jan Juli en jan Tamalu. musi ni li tan pali pi ma Elewa mama: jan David Revoy. jan awen nanpa wan: jan Craig Maloney. jan pi toki sitelen: jan Craig Maloney en jan Nartance en jan Scribblemaniac en jan Valvin. jan pi pona pali: jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson . ilo: ilo Krita 4.4.2 en ilo Inkscape 1.1 li kepeken ilo Kubuntu Linux 20.04 nasin pi lawa jo: Creative Commons Attribution 4.0. www.peppercarrot.com
jan Pepa|2|True|sina sona ala sona?
jan Pepa|3|True|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free (libre), li nasin Open-source, li lon tan mani tan jan pona mute.
jan Pepa|4|False|jan 1054 li pana e mani la, lipu ni li lon!
jan Pepa|5|True|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa. nimi sina li ken lon ni!
jan Pepa|6|True|ilo Patreon en ilo Tipeee en ilo PayPal en ilo Liberapay en ilo ante la, mi lon!
jan Pepa|7|True|sina wile sona la, o lukin e lipu www.peppercarrot.com !
jan Pepa|8|False|sina pona!
<hidden>|9|True|You can also translate this page if you want.
<hidden>|10|True|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

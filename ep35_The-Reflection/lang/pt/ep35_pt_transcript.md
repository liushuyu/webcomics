# Transcript of Pepper&Carrot Episode 35 [pt]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for how they can be generated without retyping them and also for more information
and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Episódio 35: O Reflexo

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ah, você acordou!
Pepper|2|False|Você está bem? Sem muito frio?
Pepper|3|True|Desculpa, minha Rea vai se esgotar em breve,
Pepper|4|False|e a minha aura é muito fina para nos proteger do frio aqui em cima.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Minha hipervelocidade está prestes a acabar,
Pepper|2|True|e esse piloto e dragão têm nos acompanhado durante horas.
Pepper|3|False|Eles nos alcançarão facilmente quando desacelerarmos.
Pepper|4|True|Mas que burra que eu fui!
Pepper|5|False|Eu tinha certeza de que eles iriam desistir há horas atrás!
Pepper|6|False|Se eu ao menos conseguisse despistá-los.
Pepper|7|True|Não. Eu tenho que me livrar deles!
Pepper|8|False|Antes que eles nos peguem!
Pepper|9|True|Mas eu também estou...
Pepper|10|True|...muito...
Pepper|11|False|...cansada...
Pepper|12|True|EPAAA!
Pepper|13|False|Aguenta firme aí, Pepper!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Você viu aquilo, né?
Arra|2|True|Sim. Esta caça termina em breve.
Arra|3|False|Os poderes dela logo vão se esgotar.
Torreya|4|False|Vamos atacar logo que ela fadigar.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|1|False|BuU M !
Pepper|2|False|O QUÊ?!
Pepper|3|False|Eles quebraram a barreira do som?!
Pepper|4|False|! ! !
Pepper|5|False|WOoos H ! !!
Pepper|6|False|Grrr!
Pepper|7|False|Segura firme, Carrot!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|DROGA!
Pepper|2|False|O QUE EU FAÇO?!
Pepper|3|False|Ah!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|A caverna do Lago dos Chifres!
Pepper|2|False|Carrot, esta é a nossa chance!
Pepper|3|False|Eu conheço essa caverna.
Pepper|4|True|É uma longa passagem sinuosa que termina numa seção que desmoronou.
Pepper|5|True|As estalactites dessa área que colapsou se amontoam numa parede de espetos. É uma armadilha mortal!
Pepper|6|True|Tem uma pequena abertura perto do final que é grande o suficiente para nós,
Pepper|7|False|mas aquele dragão não será capaz de escapar, e ele atingirá a parede em velocidade máxima! Muahaha!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ótimo! Eles estão nos seguindo!
Pepper|2|False|Em breve nos livraremos deles.
Pepper|3|True|Eu mal posso esperar para ver!
Pepper|4|False|Muahahaha!
Pepper|5|False|! ! !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Espera! No que eu me tornei?
Pepper|2|True|Uma assassina?
Pepper|3|False|Uma bruxa má ?
Pepper|4|False|! ! !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Som|1|False|SWoosh!
Pepper|2|True|NÃO!
Pepper|3|False|Essa não sou eu!
Pepper|4|True|PARAAAAA!!!
Pepper|5|False|EU ME RENDO!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Você sabe que se render à Mestre Wasabi não vai acabar bem para você, não é?
Pepper|2|False|Eu sei, mas eu ainda sinto que existe alguma forma de falar com ela – de fazer ela entender.
Pepper|3|True|Afinal, nada é irreparável.
Pepper|4|False|Eu aprendi isso com uma velha amiga.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Obrigada por deixar Carrot me trazer uma muda nova de roupas.
Torreya|2|True|Imagina.
Torreya|3|False|Você facilitou o nosso trabalho, então o mínimo que eu posso fazer é retornar o favor.
Torreya|4|True|Mas, sabe, você realmente irritou a Wasabi.
Torreya|5|True|Ela exige perfeição. Você perturbou isso com a sua sujeira, as suas roupas e a sua entrada.
Torreya|6|False|E você atacou ela! Ela não vai deixar barato.
Pepper|7|False|Eu sei.
Torreya|8|True|Pois bem, eu vou descansar um pouco.
Torreya|9|True|Nós partiremos logo que eu acordar.
Torreya|10|False|Pode fazer o que quiser, contanto que não vá muito longe...
Som|11|False|Splash
Torreya|12|False|Mas que bruxa mais curiosa.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Agora, não importa os riscos e perigos,
Pepper|2|True|Continuarei sendo quem eu sou.
Pepper|3|False|Eu prometo!
Narrador|4|False|CONTINUA...

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|18 de junho de 2021 Arte e roteiro: David Revoy. Leitores beta: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Versão em Português Tradução: Alexandre E. Almeida . Revisores: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch. Baseado no universo de Hereva Criador: David Revoy. Mantenedor principal: Craig Maloney. Escritores: Craig Maloney, Nartance, Scribblemaniac, Valvin. Corretores: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.4.2, Inkscape 1.1 no Kubuntu Linux 20.04 Licença: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|True|Você sabia?
Pepper|3|True|Pepper&Carrot é totalmente livre, com código aberto e patrocinada graças à gentil contribuição dos seus leitores.
Pepper|4|False|Para este episódio, agradecemos a 1054 patronos!
Pepper|5|True|Você também pode se tornar um(a) patrono(a) de Pepper&Carrot e ter o seu nome aqui!
Pepper|6|True|Nós estamos no Patreon, Tipeee, PayPal, Liberapay ...e mais!
Pepper|7|True|Acesse www.peppercarrot.com para mais informações!
Pepper|8|False|Obrigada!
<hidden>|9|True|You can also translate this page if you want.
<hidden>|10|True|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

# Transcript of Pepper&Carrot Episode 35 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for how they can be generated without retyping them and also for more information
and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Épisode 35 : Le Reflet

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Oh, tu es réveillé !
Pepper|2|False|Ça va ? Pas trop froid ?
Pepper|3|True|Désolée, je n'aurai bientôt plus de Réa,
Pepper|4|False|et mon aura est trop faible pour nous protéger du froid à cette altitude.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Je suis sur le point de perdre mon hypervitesse,
Pepper|2|True|et ce dragon et son pilote nous poursuivent depuis des heures.
Pepper|3|False|Ils n'auront aucun mal à nous attraper dès qu'on ralentira.
Pepper|4|True|Quelle idiote j'ai été !
Pepper|5|False|J'étais sûre qu'ils auraient abandonné il y a des heures !
Pepper|6|False|Si seulement je pouvais les semer.
Pepper|7|True|NON. Je dois me débarrasser d'eux !
Pepper|8|False|Avant qu'ils nous attrapent !
Pepper|9|True|Mais je suis...
Pepper|10|True|...si...
Pepper|11|False|...fatiguée...
Pepper|12|True|OUPS !
Pepper|13|False|Reprends-toi, Pepper !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|T'as vu ça ?
Arra|2|True|Oui. Cette poursuite sera bientôt terminée.
Arra|3|False|Il n'y en a plus pour longtemps avant que ses pouvoirs ne soient épuisés.
Torreya|4|False|Nous l'attaquerons à ce moment-là.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Ba OU M !
Pepper|2|False|QUOI ?!
Pepper|3|False|Ils ont franchi le mur du son ?!
Pepper|4|False|! ! !
Pepper|5|False|WOous H ! !!
Pepper|6|False|Grrr !
Pepper|7|False|Accroche-toi, Carrot !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|ZUT !
Pepper|2|False|QUE FAIRE ?!
Pepper|3|False|Oh !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|La grotte du Lac des Cornes !
Pepper|2|False|Carrot, c'est notre chance !
Pepper|3|False|Je connais cette grotte.
Pepper|4|True|C'est un long passage sinueux qui donne sur une cavité qui s'est effondrée.
Pepper|5|True|Les stalactites de cet effondrement se sont empilées en un mur de pics. C'est un piège mortel !
Pepper|6|True|Il y a une petite ouverture juste assez large pour qu'on puisse s'y faufiler,
Pepper|7|False|mais ce dragon ne pourra pas s'arrêter à temps et va se prendre le mur à pleine vitesse ! Mouhahaha !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Parfait ! Ils nous suivent !
Pepper|2|False|On va bientôt se débarrasser d'eux !
Pepper|3|True|J'ai hâte de voir ça !
Pepper|4|False|Mouhahaha !
Pepper|5|False|! ! !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Mais ! Qu'est-ce que je suis devenue ?
Pepper|2|True|Une meurtrière ?
Pepper|3|False|Une méchante sorcière ?
Pepper|4|False|! ! !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|SWooush !
Pepper|2|True|NON !
Pepper|3|False|Ce n'est pas moi, ça !
Pepper|4|True|STOOOOP !!!
Pepper|5|False|JE ME RENDS !

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Tu sais que te livrer à Maître Wasabi ne va pas bien se terminer pour toi ?
Pepper|2|False|Je sais, mais je sens qu'il y a encore un moyen d'ouvrir le dialogue, de lui faire comprendre.
Pepper|3|True|Après tout, rien n'est irréparable.
Pepper|4|False|J'ai appris ça d'une bonne amie.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Merci d'avoir laissé Carrot me chercher des vêtements de rechanges.
Torreya|2|True|De rien.
Torreya|3|False|Tu m'as facilité le travail, donc c'est la moindre des choses.
Torreya|4|True|Mais tu sais, tu as vraiment mis Wasabi en colère.
Torreya|5|True|Elle exige la perfection. Tu as souillé ça avec ton impureté, ta tenue, et ton arrivée.
Torreya|6|False|En plus, tu l'as attaquée ! Elle ne te fera pas de cadeau.
Pepper|7|False|Je sais.
Torreya|8|True|Bon, j'ai besoin d'une petite sieste.
Torreya|9|True|On partira dès mon réveil.
Torreya|10|False|Fais ce que tu veux tant que tu ne vas pas trop loin...
Son|11|False|Splash
Torreya|12|False|Quelle étrange sorcière.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Désormais, peu importe les risques et les dangers,
Pepper|2|True|je resterai fidèle à celle que je suis.
Pepper|3|False|Je le promets !
Narrateur|4|False|À SUIVRE...

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Le 18 Juin 2021 Art & scénario : David Revoy. Lecteurs de la version bêta : Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Traduction française : Nicolas Artance, David Revoy, Valvin, Zeograd Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Rédacteurs : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logiciels : Krita 4.4.2, Inkscape 1.1 on Kubuntu Linux 20.04 Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|True|Le saviez-vous ?
Pepper|3|True|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|4|False|Cet épisode a reçu le soutien de 1054 mécènes !
Pepper|5|True|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|6|True|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay ... et d'autres !
Pepper|7|True|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|8|False|Merci !
<hidden>|9|True|You can also translate this page if you want.
<hidden>|10|True|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

# Transcript of Pepper&Carrot Episode 35 [it]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for how they can be generated without retyping them and also for more information
and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Episodio 35: La riflessione

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ah, sei sveglio!
Pepper|2|False|Tutto OK? Hai freddo?
Pepper|3|True|Scusa, tra poco finirò la mia Rea,
Pepper|4|False|e la mia aura è troppo sottile per proteggerci dal freddo.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Sto per perdere la mia ipervelocità,
Pepper|2|True|e quel pilota ed il suo darago ci sono rimasti incollati per ore.
Pepper|3|False|Non faranno fatica a prenderci non appena rallenteremo.
Pepper|4|True|Che idiota che sono stata!
Pepper|5|False|Ero certa che avrebbero rinunciato ore fa!
Pepper|6|False|Se solo riuscissi a seminarli.
Pepper|7|True|NO. Devo liberarmi di loro!
Pepper|8|False|Prima che ci prendano!
Pepper|9|True|Ma sono anche...
Pepper|10|True|...molto...
Pepper|11|False|...stanca...
Pepper|12|True|WHOOPS!
Pepper|13|False|Tieni duro, Pepper!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|L'hai visto, vero?
Arra|2|True|Si. Questo inseguimento finirà presto.
Arra|3|False|Non ci vorrà molto prima che i suoi poteri si esauriscano.
Torreya|4|False|Attaccheremo non appena sarà esausta.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Bu U M !
Pepper|2|False|COSA?!
Pepper|3|False|Hanno infranto il muro del suono?!
Pepper|4|False|! ! !
Pepper|5|False|WOoos H ! !!
Pepper|6|False|Grrrr!
Pepper|7|False|Tieniti forte Carrot!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|DIAMINE!
Pepper|2|False|COSA FACCIO?!
Pepper|3|False|Oh!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|La grotta del Lago dei Corni!
Pepper|2|False|Carrot, questa è la nostra occasione!
Pepper|3|False|Conosco quella grotta.
Pepper|4|True|È un passaggio lungo e tortuoso che finisce in una grotta che è crollata.
Pepper|5|True|Le stalattiti in fondo alla grotta formano un muro di punte affilate. È una trappola mortale!
Pepper|6|True|C'è un piccolo cunicolo in fondo abbastanza largo da permetterci di passare
Pepper|7|False|ma il drago non farà in tempo a sterzare e colpirà il muro a tutta velocità! Uhahaha!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ottimo! Ci stanno seguendo!
Pepper|2|False|Presto ci libereremo di loro.
Pepper|3|True|Non vedo l'ora di finirla!
Pepper|4|False|Uhahaha!
Pepper|5|False|! ! !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Un momento! Cosa sono diventata?
Pepper|2|True|Una assassina?
Pepper|3|False|Una strega malavagia?
Pepper|4|False|! ! !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|SWoosh!
Pepper|2|True|NO!
Pepper|3|False|Questo non è quello che sono!
Pepper|4|True|STOOOOP!!!
Pepper|5|False|MI ARRENDO!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Sai che arrendendoti alla Maestra Wasabi non te la passerai liscia?
Pepper|2|False|Lo so, ma sento ancora che c'è un modo per arrivare a lei - per farle capire.
Pepper|3|True|Dopotutto, niente è irreparabile.
Pepper|4|False|L'ho imparato da una vecchia amica.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grazie per aver lasciato che Carrot mi portasse dei vestiti puliti.
Torreya|2|True|Il piacere è tutto mio.
Torreya|3|False|Hai reso più facile il nostro lavoro, quindi il minimo che potessi fare è restituirti il favore.
Torreya|4|True|Ma lo sai, hai fatto dav-vero arrabbiare Wasabi.
Torreya|5|True|Lei esige la perfezione. L'hai infastidita con la tua sporcizia, i tuoi vestiti, ed il tuo modo di arrivare.
Torreya|6|False|E l'hai aggredita! Non ci andrà leggera con te.
Pepper|7|False|Lo so.
Torreya|8|True|Comunque, ho bisogno di fare un pisolino.
Torreya|9|True|Ce ne andremo quando mi sveglio.
Torreya|10|False|Fai quello che vuoi purché non ti allontani...
Sound|11|False|Splaash
Torreya|12|False|Che strega curiosa.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ora, costi quel che costi,
Pepper|2|True|rimarrò fedele a me stessa.
Pepper|3|False|Lo prometto!
Narrator|4|False|CONTINUA...

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Giugno 18, 2021 Disegni e sceneggiatura: David Revoy. Co-autori del testo: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Versione in italiano Traduzione: Carlo Gandolfi. Basato sull'universo di Hereva Creato da: David Revoy. Amministratore: Craig Maloney. Autori: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correzioni: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.4.2, Inkscape 1.1 on Kubuntu Linux 20.04 Licenza: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|True|Lo sapevi?
Pepper|3|True|Pepper&Carrot è completamente libero (gratuito), open-source e sostenuto grazie alle gentili donazioni dei lettori.
Pepper|4|False|Per questo episodio, grazie ai 1054 donatori!
Pepper|5|True|Anche tu puoi diventare un sostenitore di Pepper&Carrot ed avere il tuo nome in questo elenco!
Pepper|6|True|Siamo su Patreon, Tipeee, Paypal, Liberapay …ed altri!
Pepper|7|True|Leggi www.peppercarrot.com per ulteriori informazioni!
Pepper|8|False|Grazie mille!
<hidden>|9|True|You can also translate this page if you want.
<hidden>|10|True|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

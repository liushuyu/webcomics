# Transcript of Pepper&Carrot Episode 24 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 24: Enheds-træet

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|5|False|BAM!|nowhitespace
Cayenne|1|True|Et Enheds-træ?
Cayenne|4|False|Vi er kaos-hekse; Vi gør ikke den slags ting.
Cayenne|6|False|Du burde hellere øve dine ”Ødelæggelses-trylleformularer”!
Pepper|7|True|Ødelæggelse?
Pepper|8|True|På denne tid af året?
Pepper|9|False|Hvorfor ikke skabelses-trylleformularer, til en forandring?
Cayenne|10|False|Det næste "De tre måners råd" nærmer sig, og det gør dine eksamener også. Studér! Lær! Øv dig!! STOP MED AT LADE DIG DISTRAHERE AF DUMME IDÉER!!!
Cayenne|2|False|Her?!
Cayenne|3|True|NEJ!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Bam!|nowhitespace
Pepper|5|True|En vinter uden enheds-træ?
Pepper|7|False|Hvorfor ikke bruge magi til at lave det selv?
Pepper|8|True|Hey! Jeg kan endda kombinere Hippiah-og Kaosah-magi, for første gang!
Pepper|9|True|Et stort grønt træ, fuld af stjerner og mini-solsystemer!
Pepper|10|False|Ja, det ville være perfekt!
Pepper|11|True|Og især
Pepper|12|False|kreativt !
Pepper|13|True|Men det er umuligt at bruge den slags magi her...
Pepper|14|False|...mine gudmødre ville med det samme se, at jeg ikke øver mine ”Ødelæggelses-trylleformularer”.
Carrot|3|False|bonk
Lyd|2|False|Ffff
Pepper|6|False|Aldrig!
Pepper|4|False|Grrrrr… !!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|3|False|Shrr
Lyd|17|False|ShiiING
Skrift|22|False|Den KOMONANSKE
Pepper|1|True|Medmindre…
Pepper|2|False|Hvis jeg husker rigtigt…
Pepper|4|False|Ja! I kapitlet ”Ødelæggelses-trylleformularer” anbefaler Timian at øve dem i en mikro-dimension. Og der er advarsler:
Pepper|16|False|Lad os komme i gang!
Spidskommen|19|False|Er det ikke lyden fra en mikro-dimension?
Lyd|18|False|Spluf! !|nowhitespace
Timian|20|True|Godt!
Skrift|8|False|”Af sikkerhedsårsager, bring IKKE Deres resultater med tilbage.”
Skrift|6|False|”Brug IKKE hele Deres Rea på at lave mikro-dimensionen.”
Skrift|11|True|"Håb IKKE på NOGEN hjælp udefra…
Pepper|7|False|Naturligvis.
Pepper|10|False|Ah, her!
Pepper|9|False|Pff… Indlysende!
Pepper|5|False|Hm…
Skrift|12|False|…INGEN kan se hvad De laver!”
Timian|21|False|Jeg håber Pepper forstod mine advarsler i kapitlet om ”Ødelæggelses-trylleformularer”…
Pepper|15|True|Det er perfekt!
Pepper|13|True|Ingen?
Pepper|14|True|Virkelig?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Genialt!
Pepper|2|True|Det er ligesom et isoleret rum med kun den del af biblioteket, jeg har brug for.
Pepper|3|False|Det perfekte sted til at teste mine trylleformularer!
Pepper|4|False|Og nu, mit mesterværk
Lyd|5|False|ShiiING
Lyd|6|False|PLof! !|nowhitespace
Lyd|8|False|Bluf ! !|nowhitespace
Lyd|10|False|Tching ! !|nowhitespace
Pepper|7|False|Øhm… hm, nah.
Pepper|9|False|Ad!
Pepper|11|False|Ja! Det er godt, men bare lidt for lille.
Pepper|13|False|CARROT! FINGRENE VÆK!
Carrot|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|FFlop ! !|nowhitespace
Pepper|3|True|Jeg ser intet problem i at tage dette træ med tilbage til den virkelige verden...
Pepper|2|True|Så! Den rette størrelse. Nu er det perfekt!
Pepper|5|True|Nå!
Pepper|6|True|Tid til at tage tilbage til virkeligheden, så de kan se, hvor kreativt, kaos kan være!
Lyd|8|False|Pof! !|nowhitespace
Timian|9|True|Hm, Pepper er tilbage.
Timian|10|False|Flot, Cayenne. Dine opdragelsesmetoder har båret frugt.
Cayenne|11|False|Tak, mester Timian.
Pepper|7|False|Jeg glæder mig til at se deres ansigter!
Skrift|12|False|Den KOMONANSKE
Pepper|4|False|Det er bare et enheds-træ, ikke noget fra en ”Ødelæggelses-trylleformular”.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|5|True|BRRrrr
Lyd|6|True|BRRr
Lyd|7|True|BRRrrr
Lyd|8|True|BRRrrr
Lyd|9|True|BRRrrr
Lyd|10|False|BRrr
Carrot|11|False|?
Carrot|13|False|!!
Lyd|14|True|CRRr
Lyd|15|False|FRRrrrr
Carrot|16|False|!!
Pepper|1|True|CARROT!
Pepper|2|True|Jeg henter lige Timian, Cayenne og Spidskommen.
Pepper|3|False|Rør ikke ved noget, forstået?
Pepper|4|False|Jeg kommer straks tilbage!
Pepper|17|True|Forbered jer på at blive overraskede og mundlamme! I har aldrig set noget lignende.
Pepper|18|False|Følg med mig til første sal for at beundre…
Lyd|12|False|Frrooff!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|6|False|- SLUT -
Lyd|1|False|Srooff!!
Lyd|4|False|KRAK! !!|nowhitespace
Lyd|3|False|Piuf! !!|nowhitespace
Pepper|2|False|?!!
Pepper|5|False|Det ser ud til, at jeg har opdaget en ny ”Ødelæggelses-trylleformular”!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|2|False|12/2017 - www.peppercarrot.com - Tegning og manuskript: David Revoy – Dansk oversættelse: Emmiline Alapetite
Credits|3|False|Hjælp til dialogerne: Craig Maloney, CalimeroTeknik, Jookia.
Credits|5|False|Baseret på Hereva-universet skabt af David Revoy med bidrag af Craig Maloney. Rettelser af Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Credits|6|False|Værktøj: Krita 3.2.3, Inkscape 0.92.2 på Ubuntu 17.10
Credits|7|False|Licens: Creative Commons Attribution 4.0
Credits|9|False|Støt næste episode af Pepper&Carrot på www.patreon.com/davidrevoy
Credits|8|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Til denne episode, tak til de 810 tilhængere:
Credits|4|False|Glædelig jul!

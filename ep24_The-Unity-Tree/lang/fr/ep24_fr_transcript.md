# Transcript of Pepper&Carrot Episode 24 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 24 : L'Arbre de l'Unité

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|5|False|PAF !|nowhitespace
Cayenne|1|True|Un Arbre de l'Unité ?
Cayenne|4|False|Nous sommes des Sorcières du Chaos ; Nous ne faisons pas ces choses-là.
Cayenne|6|False|Tu ferais mieux de travailler à tes « Sorts de Destruction » !
Pepper|7|True|Destruction ?
Pepper|8|True|À cette période de l'année ?
Pepper|9|False|Pourquoi pas des sorts de création, pour changer ?
Cayenne|10|False|Le prochain conseil des Trois Lunes approche, et tes examens aussi. Étudie ! Apprends ! Travaille !! ET ARRÊTE DE TE DISTRAIRE AVEC CES IDÉES STUPIDES !!!
Cayenne|2|False|Ici ?!
Cayenne|3|True|NON !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Blam !|nowhitespace
Pepper|5|True|Un hiver sans Arbre de l'Unité ?
Pepper|7|False|Et pourquoi ne pas utiliser la magie pour le fabriquer moi-même ?
Pepper|8|True|Hé ! Je pourrais même combiner les magies d'Hippiah et de Chaosah, une première !
Pepper|9|True|Un grand arbre vert, tout plein d'étoiles et de mini-systèmes solaires !
Pepper|10|False|Mais oui, ça serait parfait !
Pepper|11|True|Et surtout
Pepper|12|False|créatif !
Pepper|13|True|Mais hors de question de pratiquer ce type de magie ici…
Pepper|14|False|…mes marraines verraient immédiatement que je n'étudie pas mes « Sorts de Destruction ».
Carrot|3|False|tap
Son|2|False|Ffff
Pepper|6|False|Jamais !
Pepper|4|False|Grrrrr… !!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|3|False|Shrr
Son|17|False|ShiiING
Écriture|22|False|Le KOMONAN
Pepper|1|True|Quoique…
Pepper|2|False|Si je me souviens bien…
Pepper|4|False|Oui ! Au chapitre sur les "Sorts de Destruction", Thym recommande de les pratiquer dans une micro-dimension. Et il y a des avertissements :
Pepper|16|False|Au travail !
Cumin|19|False|Ça ne serait pas le bruit d'une micro-dimension ?
Son|18|False|Schplomp ! !|nowhitespace
Thym|20|True|Bien !
Écriture|8|False|« Pour des raisons de sécurité, NE PAS ramener les résultats avec vous. »
Écriture|6|False|« NE PAS dépenser tout son Réa en créant la micro-dimension. »
Écriture|11|True|« N'espérer AUCUNE aide extérieure…
Pepper|7|False|Naturellement.
Pepper|10|False|Ah, voilà !
Pepper|9|False|Pff… Évident !
Pepper|5|False|Hm…
Écriture|12|False|…PERSONNE ne peut voir ce que vous faites ! »
Thym|21|False|J'espère que Pepper a bien compris mes avertissements, dans le chapitre sur les « Sorts de Destruction »…
Pepper|15|True|C'est parfait !
Pepper|13|True|Personne ?
Pepper|14|True|Vraiment !?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Génial !
Pepper|2|True|C'est comme un conteneur isolé du reste du monde, avec seulement la partie de la bibliothèque dont j'ai besoin.
Pepper|3|False|L'endroit idéal pour tester mes sorts !
Pepper|4|False|Et maintenant, mon chef-d'œuvre !
Son|5|False|ShiiING
Son|6|False|PLooumm ! !|nowhitespace
Son|8|False|Blourf ! !|nowhitespace
Son|10|False|Chting ! !|nowhitespace
Pepper|7|False|Hum… euh, nan.
Pepper|9|False|Beurk !
Pepper|11|False|Oui ! Il est bien, mais un peu trop petit.
Pepper|13|False|CARROT ! PAS TOUCHE !
Carrot|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|FFloum ! !|nowhitespace
Pepper|3|True|Je ne vois aucun mal à ramener cet arbre dans le monde réel…
Pepper|2|True|Hop ! À la bonne taille. Maintenant il est parfait !
Pepper|5|True|Bon !
Pepper|6|True|Il est temps de déployer cette micro-dimension dans la réalité, qu'elles voient combien le Chaos peut être créatif !
Son|8|False|Bloum ! !|nowhitespace
Thym|9|True|Hum, Pepper est de retour.
Thym|10|False|Félicitations, Cayenne. Tes méthodes éducatives auront porté leurs fruits.
Cayenne|11|False|Merci, Maître Thym.
Pepper|7|False|J'ai hâte de voir leurs têtes !
Écriture|12|False|Le KOMONAN
Pepper|4|False|C'est juste un Arbre de l'Unité, ce n'est pas comme si c'était un « Sort de Destruction ».

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|5|True|BRRrrr
Son|6|True|BRRr
Son|7|True|BRRrrr
Son|8|True|BRRrrr
Son|9|True|BRRrrr
Son|10|False|BRrr
Carrot|11|False|?
Carrot|13|False|!!
Son|14|True|CRRr
Son|15|False|FRRrrrr
Carrot|16|False|!!
Pepper|1|True|CARROT !
Pepper|2|True|Je vais chercher Thym, Cayenne et Cumin.
Pepper|3|False|Ne touche à rien, compris ?
Pepper|4|False|Je reviens tout de suite !
Pepper|17|True|Préparez-vous à être stupéfaites et abasourdies ! Ce sera une première.
Pepper|18|False|Suivez-moi à l'étage pour admirer…
Son|12|False|Frrouff !!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|6|False|- FIN -
Son|1|False|Chrouff !!
Son|4|False|CRAC ! !!|nowhitespace
Son|3|False|Piouww ! !!|nowhitespace
Pepper|2|False|?!!
Pepper|5|False|Il semblerait que j'aie découvert un nouveau « Sort de Destruction » !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|2|False|12/2017 - www.peppercarrot.com - Dessin & Scénario : David Revoy - traduction : CalimeroTeknik, Nicolas Artance
Crédits|3|False|Aide aux dialogues : Craig Maloney, CalimeroTeknik, Jookia.
Crédits|5|False|Basé sur l'univers d'Hereva créé par David Revoy avec les contributions de Craig Maloney. Corrections de Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Crédits|6|False|Logiciels : Krita 3.2.3, Inkscape 0.92.2 sur Ubuntu 17.10
Crédits|7|False|Licence : Creative Commons Attribution 4.0
Crédits|9|False|Vous aussi, devenez mécène de Pepper&Carrot sur www.patreon.com/davidrevoy
Crédits|8|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 810 mécènes :
Crédits|4|False|Relecture pendant la version beta : Alex Gryzon, Nicolas Artance, Ozdamark, Zveryok, Valvin et xHire.
Crédits|1|False|Joyeuses Fêtes!

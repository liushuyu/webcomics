# Transcript of Pepper&Carrot Episode 24 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 24: The Unity Tree

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|False|PAF !|nowhitespace
Cayenne|1|True|You want a Unity Tree?
Cayenne|4|False|We are Witches of Chaos; we DO NOT do this.
Cayenne|6|False|You'd better focus on studying your “Spells of Destruction”!
Pepper|7|True|Destruction?
Pepper|8|True|At this time of year?
Pepper|9|False|Why not learn spells of creation instead?
Cayenne|10|False|The next council of The Three Moons is approaching and so are your exams. Learn! Learn! LEARN!!! AND STOP DISTRACTING YOURSELF WITH THESE FOOLISH IDEAS!!!
Cayenne|2|False|Here?!?
Cayenne|3|True|NO!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Sl a m !|nowhitespace
Pepper|5|True|A winter without a Unity Tree?!
Pepper|7|False|Why don't I make my own tree from scratch with magic?!
Pepper|8|True|Hey! I might even combine Hippiah and Chaosah magic together for the first time!
Pepper|9|True|A big green tree, lit by stars and mini solar systems!
Pepper|10|False|Aww! It would be perfect!
Pepper|11|True|And it would be
Pepper|12|False|creative!
Pepper|13|True|But there's no way I can do this type of magic here...
Pepper|14|False|...my godmothers would immediately notice if I'm not studying my “Spells of Destruction”.
Carrot|3|False|tap
Sound|2|False|Ffff
Pepper|6|False|Never!
Pepper|4|False|Grrrrr...!!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|3|False|Shrr
Sound|17|False|ShiiING
Writing|22|False|The KOMONAN
Pepper|1|True|Unless...
Pepper|2|False|I think I remember...
Pepper|4|False|Yes! In the chapter on “Spells of Destruction”, Thyme recommends trying them in a micro-dimension. But look at all these warnings:
Pepper|16|False|Let's get to work!
Cumin|19|False|Did Pepper just create a micro-dimension?
Sound|18|False|SCHwoomp! !|nowhitespace
Thyme|20|True|Good!
Writing|8|False|“ DO NOT bring the results back, for safety reasons.”
Writing|6|False|“ DO NOT spend all your Rea in the creation of the micro-dimension.”
Writing|11|True|“ DO NOT expect any assistance from outside of the micro-dimension...
Pepper|7|False|Naturally.
Pepper|10|False|Here it is!
Pepper|9|False|Pff... Obvious!
Pepper|5|False|Mm...
Writing|12|False|...no one can see what you are doing ! ”
Thyme|21|False|I hope she understood all of those warnings I wrote in that chapter about “Spells of Destruction”.
Pepper|15|True|That's perfect!
Pepper|13|True|No one?
Pepper|14|True|Really!?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|So cool!
Pepper|2|True|It's like a container, with only the part of the library that I need. And it's totally isolated from our own reality.
Pepper|3|False|It's the perfect sandbox for testing spells.
Pepper|4|False|Time to focus on my masterpiece!
Sound|5|False|ShiiING
Sound|6|False|PLOooom! !|nowhitespace
Sound|8|False|Dhoooff! !|nowhitespace
Sound|10|False|Chting! !|nowhitespace
Pepper|7|False|Heh. Uh, nope.
Pepper|9|False|Ewww!
Pepper|11|False|Yes! It looks good, but it's too small...
Pepper|13|False|CARROT! DON'T TOUCH IT!
Carrot|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Plooom! !|nowhitespace
Pepper|3|True|I don't see any harm in bringing this tree out of this micro-dimension.
Pepper|2|True|There! Re-sized it. Now it looks perfect!
Pepper|5|True|Alright,
Pepper|6|True|it's time to deploy this micro-dimension into reality and show them how creative The Chaos can be!
Sound|8|False|DOooong! !|nowhitespace
Thyme|9|True|Mmm... Pepper is back.
Thyme|10|False|Bravo, Cayenne. Your educational methods are finally giving us results.
Cayenne|11|False|Thank you, headmistress.
Pepper|7|False|I can't wait to see the look on their faces!
Writing|12|False|The KOMONAN
Pepper|4|False|It's just a Unity Tree; It's not like it's a “Spell of Destruction”.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|BRRrrr
Sound|6|True|BRRr
Sound|7|True|BRRrrr
Sound|8|True|BRRrrr
Sound|9|True|BRRrrr
Sound|10|False|BRrr
Carrot|11|False|?
Carrot|13|False|!!
Sound|14|True|BRRr
Sound|15|False|BRRrrrr
Carrot|16|False|!!
Pepper|1|True|CARROT!
Pepper|2|True|I'll be back in a few minutes with Thyme, Cayenne and Cumin.
Pepper|3|False|Don't do anything with that tree until I get back, OK?
Pepper|4|False|OK, I'll be back soon!
Pepper|17|True|Prepare to be amazed and astounded! You've never seen anything quite like this.
Pepper|18|False|Please follow me upstairs to see...
Sound|12|False|Shroof!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|6|False|- FIN -
Sound|1|False|Shroooof!!
Sound|4|False|CRASH! !!|nowhitespace
Sound|3|False|Fooom! !!|nowhitespace
Pepper|2|False|?!!
Pepper|5|False|It seems I studied the “Spells of Destruction” after all!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|2|False|12/2017 - www.peppercarrot.com - Art & Scenario: David Revoy
Credits|3|False|Dialog Improvements: Craig Maloney, CalimeroTeknik, Jookia.
Credits|5|False|Based on the universe of Hereva created by David Revoy with contributions by Craig Maloney. Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
Credits|6|False|Software: Krita 3.2.3, Inkscape 0.92.2 on Ubuntu 17.10
Credits|7|False|Licence: Creative Commons Attribution 4.0
Credits|9|False|You too can become a patron of Pepper&Carrot at www.patreon.com/davidrevoy
Credits|8|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thanks go to 810 Patrons:
Credits|4|False|Proofreading/Beta feedback: Alex Gryson, Nicolas Artance, Ozdamark, Zveryok, Valvin and xHire.
Credits|1|False|Happy Holidays!

# Transcript of Pepper&Carrot Episode 18 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 18: An coinneachadh

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|"Hippiah" subtle on the box
Peabar|1|False|Ceart ma-thà, faodaidh tu coimhead orm a-nis!
Peabar|2|True|TA~DÀ~~A !|nowhitespace
Peabar|3|True|Èideadh bana-bhuidseach na Clann-fhlùrachd!
Peabar|4|False|Cha robh gin sam bith eile air fhàgail sa bhùth ’s mar sin thug mi fear leam fhad ’s a tha mi a’ feitheamh air an èideadh ùr agam!
Peabar|7|False|Nise! An cuir seo dad ’nad chuimhne? ...
Sgrìobhte|6|False|Clann-fhlùrachd

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Wood panel
<hidden>|0|False|Door
Sgrìobhte|1|True|Sgoıl
Sgrìobhte|3|True|Clann-
Sgrìobhte|4|False|fhlùr achd|nowhitespace
Sgrìobhte|2|True|na
Sgrìobhte|6|False|CIDSIN
Curran|5|False|Rùdail
Calameilt|7|False|Glè mhath, a Bhiolair.
Calameilt|8|False|Glè mhath, a Mhionntainn.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Calameilt|1|False|Glè mhath, a Pheirsill.
Calameilt|2|False|Glè mhath, a Bhuidheag.
Calameilt|3|False|A Pheabar ?!
Peabar|4|False|Ach tha...
Calameilt|5|True|Seo fàilleadh eile !!!
Calameilt|6|False|Cuiream ’nad chuimhne nach e an lus-mharbhadh a tha fainear draoidheachd na Clann-fhlùrachd
Biolair|7|False|HÀ HÀHÀ HÀ !
Mionntainn|8|False|HÀ HÀ !
Peirsill|9|False|HÀ HÀ !
Buidheag|10|False|HÀ HÀHÀ HÀ !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Biolair|1|False|HÀHÀ HÀ !
Mionntainn|2|False|Hùhù hù !
Peirsill|9|False|Pfff !
Buidheag|10|False|HÀHÀ HÀ HÀ !
Peabar|3|True|SGUIRibh dheth
Peabar|4|True|Chan eil mi as coireach!!!
Peabar|5|True|Sa bhad...
Peabar|6|True|SGUIRibh
Peabar|7|True|!!!
Peabar|8|False|!!!
Mionntainn|12|False|HÀHÀ HÀ HÀ !
Biolair|11|False|Hùhù hù !
Buidheag|14|False|HÀHÀ HÀ HÀ !
Peirsill|15|False|HÀHÀ HÀ !!
Peabar|13|False|Grr... Sguiribh... Thuirt mi...
Peabar|16|True|Dh’iarr mi oirbh...
Peabar|17|False|SGUR !!!
Fuaim|18|False|B E EUM ! ! !|nowhitespace
Fuaim|19|False|BR A G ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|on the door
Cìob|1|True|Ùbh ùbh ...
Cìob|2|False|Nam b’ fheàrr leat, gabhaidh sinne cùram oirre ...
Cìob|3|False|Nach fortanach gu bheil sinn a’ sireadh oileanach na Dubh-choimeasgachd ...
Tìom|4|False|... agus tha coltas an tagraiche fhoirfe air an nighean sin.
Calameilt|5|True|A Charabhaidh?!
Calameilt|6|True|A Thìom?! A Chìob?!
Calameilt|7|False|Ach bha mi dhen bheachd gun robh sibh uile ... ... chan eil rian air
Sgrìobhte|8|False|CIDSIN

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|carachadh !|nowhitespace
Fuaim|2|False|Poc !|nowhitespace
Peabar|3|False|Hì hì hì!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|"Hippiah" subtle on the box
<hidden>|0|False|Don't translate the names of the scenarios
Peabar|1|True|Tha mi a’ faireachdainn gum bi sinn ’nar deagh charaidean!
Peabar|2|False|... agus tha beachd agam mu thràth air an ainm chuireas mi ort!
Peabar|3|False|Uill? Dè bheir seo an cuimhne dhut?!
Curran|4|False|Rùdail?
Sgrìobhte|5|False|Clann-fhlùrachd
Neach-aithris|6|False|- Deireadh na sgeòil -
Urram|7|False|08/2016 – www.peppercarrot.com – Obair-ealain ⁊ sgeulachd: David Revoy – Eadar-theangachadh: GunChleoc
Urram|8|False|Bhrosnaich dà sgeulachd a mhol Craig Maloney an sgeulachd seo: “You found me, I Choose You” ’s “Visit from Hippiah”.
Urram|9|False|Stèidhichte air saoghal Hereva a chaidh a chruthachadh le David Revoy le taic o Craig Maloney. Ceartachadh le Willem Sonke, Moini, Hali, CGand ’s Alex Gryson.
Urram|10|False|Ceadachas : Creative Commons Attribution 4.0, Bathar-bog: Krita 3.0, Inkscape 0.91 air Manjaro XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 720 pàtran a thug taic dhan eapasod seo:
Urram|2|False|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd air www.patreon.com/davidrevoy

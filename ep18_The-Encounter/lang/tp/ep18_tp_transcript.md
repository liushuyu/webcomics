# Transcript of Pepper&Carrot Episode 18 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa luka luka luka tu wan: kulupu

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|"Hippiah" subtle on the box
jan Pepa|1|False|pona a. mi pini!
jan Pepa|2|True|O LUU~KI~N !|nowhitespace
jan Pepa|3|True|len pi kulupu Piponaa a!
jan Pepa|4|False|len sin mi li kama la, len ni taso li lon esun. ni la, mi jo e ona!
jan Pepa|7|False|sina pilin seme? sina sona e ona tan seme?...
sitelen|6|False|kPIPONAA

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Wood panel
<hidden>|0|False|Door
sitelen|1|False|tomo sona Piponaa pi wawa nasa
sitelen|3|False|TOMO PALI MOKU
soweli Kawa|2|False|Mumoku
jan Pasilikun|4|False|pona, jan Olekano o.
jan Pasilikun|5|False|pona, jan Katamon o.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pasilikun|1|False|pona mute, jan Sinamon o.
jan Pasilikun|2|False|pona, jan Kamomile o.
jan Pasilikun|3|False|jan Pepa o ?!
jan Pepa|4|False|taso mi...
jan Pasilikun|5|True|ni li pakala sin!!!
jan Pasilikun|6|False|o sona awen e ni: kulupu Piponaa li pali ala e wawa nasa tawa moli kasi!...
jan Olekano|7|False|A A AA AA A !
jan Katamon|8|False|A AA A !
jan Sinamon|9|False|A AA A !
jan Kamomile|10|False|A AA AA A A !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Olekano|1|False|A A AA A !
jan Katamon|2|False|aaa a a !
jan Sinamon|9|False|aa a !
jan Kamomile|10|False|AA AA AA AA !
jan Pepa|3|True|O PINI
jan Pepa|4|True|!!!
jan Pepa|5|True|pakala li ike mi ala!!!
jan Pepa|6|True|o kute e mi...
jan Pepa|7|True|O PINI
jan Pepa|8|False|!!!
jan Katamon|12|False|A A AA A AA !
jan Olekano|11|False|a aa a !
jan Kamomile|14|False|A AA A AA A !
jan Sinamon|15|False|AA AA AA !!
jan Pepa|13|False|ike... o pini... o kute...
jan Pepa|16|True|o kute...
jan Pepa|17|False|O PINI !!!
kalama|18|False|W A AWA! ! !|nowhitespace
kalama|19|False|PA K AL A ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|on the door
jan Kajen|1|True|toki...
jan Kajen|2|False|sina wile la, mi ken awen e ona...
jan Kajen|3|False|kulupu Pakalaa la, mi alasa e jan pi kama sona...
jan Tume|4|False|... mi la, jan ni li pona tawa ni..
jan Pasilikun|5|True|jan Kajen o?!
jan Pasilikun|6|True|jan Tume o?! jan Kumin o?!
jan Pasilikun|7|False|taso... toki la, sina ale li... ... ni li ken anu seme?
sitelen|8|False|TOMO PALI MOKU

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|Tawwa !|nowhitespace
kalama|2|False|Anpa !|nowhitespace
jan Pepa|3|False|aa aa aa!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|"Hippiah" subtle on the box
<hidden>|0|False|Don't translate the names of the scenarios
jan Pepa|1|True|pilin mi la, sina en mi li pona tawa mi tu!
jan Pepa|2|False|... mi wile pana e nimi tawa sina. nimi ni o suwi!
jan Pepa|3|False|ni la, sina pilin e seme?!
soweli Kawa|4|False|Mumoku?
sitelen|5|False|kPIPONAA
sitelen toki|6|False|- PINI -
mama|7|False|tenpo 08/2016 - www.peppercarrot.com - musi sitelen & toki li tan jan David Revoy - ante toki pi toki pona li tan jan Ret Samys
mama|8|False|toki li sama lili toki tu tan jan Craig Maloney: "You found me, I Choose You" en "Visit from Hippiah".
mama|9|False|musi ni li tan pali pi ma Elewa. pali ni li tan jan David Revoy. jan Craig Maloney li pana e pona tawa ona. jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson li pona e musi ni.
mama|10|False|nasin lawa : Creative Commons Attribution 4.0, ilo: ilo Krita 3.0 en ilo Inkscape 0.91 li kepeken ilo Manjaro XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 720 li pana e mani la, lipu ni li lon:
mama|2|False|sina ken pana kin e pona tawa jan Pepa&soweli Kawa lon www.patreon.com/davidrevoy

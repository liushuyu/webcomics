# Transcript of Pepper&Carrot Episode 13 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 13 : La Soirée Pyjama

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|C'est juste trop bien !
Pepper|5|False|Merci de nous accueillir chez-toi Coriandre !
Pepper|3|False|...du monde !
Pepper|1|True|C'est les meilleures...
Pepper|2|True|...vacances...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandre|1|True|Merci !
Coriandre|2|False|J'espère que vous n'êtes pas trop impressionnées par mon personnel.
Pepper|11|False|Non, t'inquiète pas; ils sont super accueillants, on se sent tout de suite à l'aise.
Shichimi|13|False|...si raffinée et chaleureuse à la fois!
Pepper|14|False|Bien vrai, ça!
Shichimi|12|True|Moi je suis impressionnée par la déco...
Monstre|9|False|WoooOoh ! ! !|nowhitespace
Monstre|10|False|WoOoh ! ! !|nowhitespace
Monstre|8|False|WoooOoh ! ! !|nowhitespace
Son|7|False|chwwwwwwiiing !|nowhitespace
Son|6|False|Klaf !|nowhitespace
Son|5|False|Klukf !|nowhitespace
Son|3|False|Dzzziii ! ! !|nowhitespace
Son|4|False|Shkak ! ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandre|2|True|parfois je trouve tout ça si ...
Shichimi|5|True|Moi j'aimerais bien vivre dans ce confort,
Son|7|False|Shiingz ! ! !|nowhitespace
Son|4|False|bim !|nowhitespace
Shichimi|8|False|... "Une-vraie sorcière-de-Ah-ne doit-pas-vivre-de cette-façon-là" .
Pepper|9|True|Ha ha !...
Pepper|10|True|Le poids des traditions...
Pepper|11|False|On dirait une phrase de mes marraines !
Shichimi|12|False|C'est vrai ?
Coriandre|1|True|C'est sympa de me le rappeller
Coriandre|3|False|... "banal" ?...
Shichimi|6|False|mais ...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandre|2|False|Allez les filles on se reconcentre, on arrive au but!
Pepper et Shichimi|1|False|Hi hi hi!
Pepper|3|True|Oh, c'est bon...!
Pepper|5|False|...et on n'a même pas commencé à...
Son|6|False|WRoooOwwoww ! ! !|nowhitespace
Son|9|False|Shhshh|nowhitespace
Shichimi|8|False|?!!
Coriandre|7|False|PEPPER ! !!|nowhitespace
Pepper|4|True|Cette quête est quand même super fastoche...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|6|False|C R A C K ! !|nowhitespace
Shichimi|8|False|Oh NON!
Coriandre|9|False|Pas encore !
Pepper|11|False|Lâche-ça ! Tout de suite !
Coriandre|1|False|...Oh non ! C'est trop tard! elle... elle est ...
Shichimi|2|False|N oo oo on !!!|nowhitespace
Monstre|3|False|MUAH HAHA HAHA !!!|nowhitespace
Shichimi|4|False|G RR ! ! !|nowhitespace
Coriandre|5|False|TU VAS PAYER !...
Son|7|False|KLING ! !|nowhitespace
Son|13|False|P LO NK ! !|nowhitespace
Son|12|False|P AF ! !|nowhitespace
Pepper|10|True|CARROT!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|8|False|- FIN -
Crédits|9|False|11/2015 - Dessin & Scénario : David Revoy, corrections: Jo, Frémo
Son|1|False|Pam
Pepper|4|True|Je sais que tu fais ça pour me défendre...
Pepper|3|True|Allez, grogne pas!
Pepper|5|False|...mais quand on joue ça sert à rien!
Carrot|7|False|Grrrr
Écriture|6|False|Citadelles & Phoénix
Écriture|2|False|Princesse Coriandre

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 602 Mécènes :
Crédits|2|True|Vous aussi, devenez mécène de Pepper&Carrot pour le prochain épisode sur
Crédits|4|False|License : Creative Commons Attribution 4.0 Sources : disponibles sur www.peppercarrot.com Logiciels : cet épisode a été dessiné à 100% avec des logiciels libres Krita 2.9.9, Inkscape 0.91 on Linux Mint 17
Crédits|3|False|https://www.patreon.com/davidrevoy

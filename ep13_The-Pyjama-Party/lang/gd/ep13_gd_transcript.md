# Transcript of Pepper&Carrot Episode 13 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 13: An tathadh-oidhche

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|5|False|Tha an t-àite seo cho càilear!
Peabar|6|False|Mòran taing airson a’ chuiridh, a Chostag!
Peabar|4|False|... a-riamh !
Peabar|1|True|Seo na saor-làithean ...
Peabar|2|True|... as fheàrr ...
Peabar|3|True|... a bh’ agam ...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Costag|1|True|Tha sibh làn di-beathte !
Costag|2|False|Tha mi ’n dòchas nach cuir an luchd-obrach agam cus eagail oirbh.
Peabar|11|False|Na gabh dragh, tha iad glè fhàilteachail is sinne cofhurtail aig an taigh seo.
Sidimi|13|False|... cho fìnealta ach seasgair !
Peabar|14|False|Tha sin fìor !
Sidimi|12|True|Tha an sgeadachadh drùiteach ...
Uilebheist|9|False|HuuuOoo ! ! !|nowhitespace
Uilebheist|10|False|Huuuu ! ! !|nowhitespace
Uilebheist|8|False|HaooOoo ! ! !|nowhitespace
Fuaim|7|False|strrrrrrrràc !|nowhitespace
Fuaim|6|False|Sleog !|nowhitespace
Fuaim|5|False|Sgailc !|nowhitespace
Fuaim|3|False|Tsssiii ! ! !|nowhitespace
Fuaim|4|False|Solas ! ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Costag|2|True|... dhomhsa dheth, tha e cho ...
Sidimi|5|True|Bu mhiann leam fhìn fuireach ann an àite cho cofhurtail sin
Fuaim|7|False|Sgoilt ! ! !|nowhitespace
Fuaim|4|False|beum !|nowhitespace
Sidimi|8|False|... “Chan-e-seo-dòigh-bana-bhuidseach-cheart-an-Achd”
Peabar|9|True|Hà hà ! ...
Peabar|11|False|Chanadh na bana-ghoistidhean agam an dearbh rud!
Sidimi|12|False|An canadh gu dearbh ?
Costag|1|True|Tha sibh cho coibhneil seo a chur ’nam chuimhne ...
Costag|3|False|... "saoghalta" aig amannan...
Sidimi|6|False|ach ...
Peabar|10|True|Seo dleastanas an dualchais dhut ...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Costag|2|False|Siuthadaibh a chlann-nighean, an cùm sibh ur n-aire air is sinne cha mhòr aig a’ chrìoch!
Peabar ⁊ Sidimi|1|False|Tì-hi-hi!
Peabar|3|True|Ò, na gabh dragh ... !
Peabar|5|False|... ’s cha do thòisich sinn fiù ’s air ...
Fuaim|6|False|FRuuuAoibh ! ! !|nowhitespace
Fuaim|9|False|Fffssss|nowhitespace
Sidimi|8|False|?!!
Costag|7|False|A PHEABAR ! !!
Peabar|4|True|Feumaidh tu aideachadh gu bheil an turas seo uabhasach furasta ...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|6|False|B R A G ! !|nowhitespace
Sidimi|8|False|Obh OBH!
Costag|9|False|A-rithist !
Peabar|11|False|Leig às e ! Sa bhad !
Costag|1|False|...Mo chreach! Tha e ro anmoch! Tha ... tha i ...
Sidimi|2|False|O ch an Ò !!!|nowhitespace
Uilebheist|3|False|MUÀ HAHA HAHA !!!|nowhitespace
Sidimi|4|False|G RR ! ! !|nowhitespace
Costag|5|False|’S ann DAOR a bhios do cheannachd air !
Fuaim|7|False|GLIONG ! !|nowhitespace
Fuaim|13|False|B EU M ! !|nowhitespace
Fuaim|12|False|P AF ! !|nowhitespace
Peabar|10|True|A CHURRAIN!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|10|False|11/2015 – Obair-ealain ⁊ sgeulachd: David Revoy – Eadar-theangachadh: GunChleoc
Fuaim|1|False|Smug|nowhitespace
Peabar|4|True|Tha fhios a’m gun robh thu airson mo dhìon ...
Peabar|3|True|Fire faire, na cuir bèill ort!
Peabar|5|False|... ach cha robh feum air, bha sinn a’ cluich mas fhìor!
Curran|8|False|Grrrr
Sgrìobhte|6|False|Tùir ⁊ ainneamhagan
Sgrìobhte|2|False|A’ bhana-phrionnsa Costag
Neach-aithris|9|False|- Deireadh na sgeòil -
Sgrìobhte|7|False|Tùir ⁊ ainneamhagan

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 602 pàtran a thug taic dhan eapasod seo::
Urram|2|True|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd airson an ath-eapasod air
Urram|3|False|https://www.patreon.com/davidrevoy
Urram|4|False|Ceadachas : Creative Commons Attribution 4.0 Bun-tùs : ri fhaighinn air www.peppercarrot.com Bathar-bog : chaidh 100% dhen eapasod seo a tharraing le bathar-bog saor Krita 2.9.9, Inkscape 0.91 air Linux Mint 17

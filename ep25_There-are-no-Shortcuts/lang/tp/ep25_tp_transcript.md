# Transcript of Pepper&Carrot Episode 25 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute luka: sina ken ala lili e nasin pali

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen|1|False|MOKU SOWELI

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|13|False|- PINI -
jan Pepa|1|True|?
jan Pepa|2|True|?
jan Pepa|3|True|?
jan Pepa|4|False|?
jan Pepa|5|True|?
jan Pepa|6|True|?
jan Pepa|7|True|?
jan Pepa|8|False|?
jan Pepa|9|True|?
jan Pepa|10|True|?
jan Pepa|11|True|?
jan Pepa|12|False|?

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|7|False|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa lon www.patreon.com/davidrevoy
mama|6|False|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 909 li pana e mani la, lipu ni li lon:
mama|1|False|tenpo 05/2018 - www.peppercarrot.com - musi sitelen & toki li tan jan David Revoy - ante toki pi toki pona li tan jan Ret Samys
mama|3|False|musi ni li tan pali pi ma Elewa. pali ni li tan jan David Revoy. jan Craig Maloney li pana e pona tawa ona. jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson li pona e musi ni.
mama|4|False|ilo: ilo Krita 4.0.0 en ilo Inkscape 0.92.3 li kepeken Kubuntu 17.10
mama|5|False|nasin pi lawa jo: Creative Commons Attribution 4.0
mama|2|False|jan Nicolas Artance en jan Imsesaok en jan Craig Maloney en jan Midgard en jan Valvin en jan xHire en jan Zveryok li lukin pona e toki pi pini ala.

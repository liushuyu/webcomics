# Transcript of Pepper&Carrot Episode 21 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute wan: utala musi pi wawa nasa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen|1|False|jan mute li wile e ni: ma Komona li lawa e utala pi wawa nasa
sitelen|2|False|mani Komo 50 000
sitelen|3|False|mani ni li tawa pali pi pona nanpa wan! sina wile kama la, o pana e mani Komo 150 lon ma suli utala sin lon ma Komona lon tenpo Pakalsuno, lon tenpo AnpaMun nanpa 10
sitelen|4|False|KAMA SINA LI SULI
jan Pepa|5|False|mi wile! soweli Kawa o, mi tawa!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|tenpo ni la, awen la, sina wile ala lon poka mi anu seme?
jan Kajen|2|False|wile. kulupu Pakalaa li pana e wawa, e musi ala!
sitelen|3|False|lipu KOMONA
sitelen|4|False|MA SULI UTALA SIN LI OPEN LA, TENPO MUSI LI LON TENPO SUNO NI
sitelen|5|False|MANI LI TAWA LAWA LA, JAN PI MA TOMO LI PILIN PI SONA ALA
sitelen|6|False|UTALA MUSI LI SULI: JAN MUTE LI WILE KAMA, LI PANA E MANI ALE
sitelen|7|False|NASIN PI MA AA LA, JAN LI TOKI KEPEKEN UTALA
sitelen|8|False|MA LUKA LUKA LA, KULUPU AA LI LAWA E MA LUKA TU WAN
sitelen|9|False|KEN LUKIN SINA LI WAWA PONA
jan Pepa|10|True|pona sina la, sina ken e utala musi mi!
jan Pepa|11|True|o sona e ni: mi kama nanpa wan!
jan Pepa|12|False|mi tawa tomo lon pini pi tenpo suno!
jan Kajen|14|False|...
kalama|13|False|Kalama!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Edit this one, all other one are linked
kalama jan|2|False|Luka
jan lawa pi ma Komona|1|False|mi jan lawa pi ma Komona. mi toki e ni: utala musi nanpa tu pi wawa nasa li open!
jan lawa pi ma Komona|3|True|mi sona e ni: tenpo mute la, sina ale li wile e tenpo ni!
jan lawa pi ma Komona|4|False|pona la, sina kama tan ma Elewa ale. ni la, sina ken kama sona e ma mi pi utala musi! ona la, pakala ala li kama a!
jan lawa pi ma Komona|5|False|o sona e jan pi utala musi!
soweli Kawa|6|True|L
soweli Kawa|7|True|L|nowhitespace
soweli Kawa|8|False|LAPE|nowhitespace
jan Pepa|9|True|soweli Kawa o?! sina pali e seme?!
jan Pepa|10|True|o pini e lape! ni li tenpo open!
jan Pepa|11|False|jan ale li lukin e mi!
kalama|12|False|Noka!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Edit this one, all other one are linked
kalama jan|3|False|Luka
jan lawa pi ma Komona|1|True|jan ni li sona e wawa "Molitawaa". ona li...
jan lawa pi ma Komona|2|False|JAN KOWIJANA!
jan lawa pi ma Komona|4|True|kulupu Piponaa la, jan ni pi wawa nasa li sona ale e kasi e soweli! o sona e...
jan lawa pi ma Komona|5|False|JAN KAMOMILE!
jan lawa pi ma Komona|6|False|tenpo ni la, o lukin. sona ilo pi ma Komona la, sin li lon ni...
kalama|7|False|Open!
jan lawa pi ma Komona|8|True|...ni li jan pi wawa nasa tan anpa telo, li sona e nasin wawa "Telomaa"!
jan lawa pi ma Komona|9|False|JAN PILULINA!
jan Pepa|10|False|nasin Telomaa?! anu seme?! suli a!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Edit this one, all other one are linked
kalama jan|9|False|Luka
soweli Kawa|1|False|Mu u m oku|nowhitespace
jan Pepa|2|True|soweli Kawa o!
jan Pepa|3|True|tenpo mi li kama lon tenpo lili!
jan Pepa|4|True|o awen!
jan Pepa|5|True|o pona!
jan Pepa|6|False|lon tenpo ni taso!
jan lawa pi ma Komona|7|True|nasin Aa pi wawa nasa li nasin sewi, li nasin pona. jan ona pona li ...
jan lawa pi ma Komona|8|False|JAN SISIMI!
jan Pepa|10|False|soweli Kawa O PINI!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Edit this one, all other one are linked
kalama jan|7|False|a! a!
jan lawa pi ma Komona|1|False|jan wan li pana ala e nimi ona. ona li seme? ona li wile pana e wawa nasa "Selilaa" suli a...
jan lawa pi ma Komona|3|True|aaa! pilin suli a! mi kama sona! jan wan taso li ken ni, li...
jan lawa pi ma Komona|4|False|jan Sapon!
kalama|2|False|Open!
jan lawa pi ma Komona|6|False|a! lukin la, pakala li tan jan utala wan lon tenpo sin ni!
kalama|5|False|Tttelo!
jan lawa pi ma Komona|8|False|jan o!! sina o tawa poka sina!!
jan Pepa|9|False|soweli Kawa oooo!
kalama|10|False|Laawa!
jan Pepa|11|False|?!
kalama|12|False|Telllo!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|a-aa-a! sina pona, jan Pilulina o! mi pakala...
jan Pepa|2|True|sina tawa seme ?!
jan Pepa|3|False|O TAWA MI!
jan lawa pi ma Komona|4|True|a, o kute! jan nanpa pini li sona e... a... wawa ante... e wawa nasa "Pakalaa"...
jan lawa pi ma Komona|5|False|JAN PEPA!
jan lawa pi ma Komona|6|False|utala pi tenpo pini la, ona li jan nanpa wan. tenpo ni la, ona li kama pali e suli tawa utala musi ni!
jan Pepa|7|False|?!...
jan lawa pi ma Komona|8|False|ni la, ona li...
jan lawa pi ma Komona|9|False|... tawa kulupu lawa pi utala musi!
kalama|10|False|Anpa
kalama jan|11|True|Luka
kalama jan|12|True|Luka
kalama jan|13|True|Luka
kalama jan|14|True|Luka
kalama jan|15|False|Luka
sitelen|16|False|kulupu lawa pi utala musi
jan Pepa|17|False|pona a.
sitelen|18|False|lawa Ajela
sitelen|19|False|jan Pepa
sitelen|20|False|lawa Asiwepa
sitelen toki|21|False|TOKI LI AWEN...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|tenpo 02/2017 - www.peppercarrot.com - musi sitelen & toki li tan jan David Revoy - ante toki pi toki pona li tan jan Ret Samys
mama|2|False|jan Craig Maloney en jan Quiralta en jan Nicolas Artance en jan Talime en jan Valvin li toki kulupu, li pana e sona, e pilin la, toki pi lipu ni li kama.
mama|3|False|jan Craig Maloney en jan Jookia en jan Nicolas Artance en jan Valvin li pona e toki jan.
mama|4|False|pona namako li tan kulupu Inkscape team, li tan jan Mc a.
mama|5|False|musi ni li tan pali pi ma Elewa. pali ni li tan jan David Revoy. jan Craig Maloney li pana e pona tawa ona. jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson li pona e musi ni.
mama|6|False|ilo: Krita 3.2.1, Inkscape 0.91 on Linux Mint 18.1 XFCE
mama|7|False|nasin pi lawa jo: Creative Commons Attribution 4.0
mama|9|False|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa lon www.patreon.com/davidrevoy
mama|8|False|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 816 li pana e mani la, lipu ni li lon:

# Transcript of Pepper&Carrot Episode 07 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 7: Der Wunsch

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CARROT !
Pepper|2|False|CARROT !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Carrot|1|True|Zzzzz
Carrot|2|False|Zzzzz
Carrot|5|True|Zzzzz
Carrot|6|False|Zzzzz
Pepper|3|False|Hab’ dich!
Pepper|4|False|Na los, genug geschlummert, es ist Zeit, heimzugehen!
Geräusch|7|False|B Z Z Z|nowhitespace
Geräusch|8|False|POF F!|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Feen|1|True|Willkommen in der Grotte der magischen Feen, ihr Abenteurer...
Pepper|3|False|... alles ... ... was ich will
Feen|2|False|... wir gewähren euch einen einzigen Wunsch, was auch immer ihr wollt, wir werden es wahr machen!
Pepper|4|False|Ich... Ich will, also, was ich gerne hätte, ist ...
Carrot|5|True|Miau !
Carrot|6|False|Zzzzz

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|True|... aber ernsthaft?
Pepper|1|True|Ich weiß, dass es manchmal wünschenswert ist, wenn alles bleibt, wie es ist...
Pepper|3|False|„Ich will einfach nur weiterschlafen“? Was für ’ne Verschwendung!
Carrot|4|True|Zzzzz
Carrot|5|False|Zzzzz
Impressum|8|False|April 2015 - www.peppercarrot.com - Grafik und Handlung: David Revoy Übersetzung: Helmar Suschka & Alexandra Jordan
Erzähler|6|False|Episode 7 : Der Wunsch
Erzähler|7|False|ENDE

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Ich danke in dieser Episode meinen 273 Förderern :
Impressum|4|False|https://www.patreon.com/davidrevoy
Impressum|3|True|Du kannst die nächste Episode von Pepper&Carrot hier unterstützen:
Impressum|7|False|Tools : Dieser Comic wurde zu 100% mit der freien Open-Source-Software Krita auf Linux Mint erstellt
Impressum|6|False|Open Source : Alle Quelldateien mit Ebenen und Schriften findest Du auf der Website
Impressum|5|False|Lizenz : Creative Commons Namensnennung Du darfst verändern, teilen, verkaufen, abgeleitete Werke erstellen etc...
Impressum|2|False|Addison Lewis ❀ A Distinguished Robot ❀ Adrian Lord ❀ Ahmad Ali ❀ Aina Reich ❀ Alandran ❀ Alan Hardman ❀ Albert Westra ❀ Alcide Alex ❀ Alexander Bülow Tomassen ❀ Alexander Sopicki ❀ Alexandra Jordan ❀ Alexey Golubev ❀ Alex Flores ❀ Alex Lusco ❀ Alex Silver Alex Vandiver ❀ Alfredo ❀ Ali Poulton (Aunty Pol) ❀ Allan Zieser ❀ Andreas Rieger ❀ Andreas Ulmer ❀ Andrej Kwadrin ❀ Andrew Andrew Godfrey ❀ Andrey Alekseenko ❀ Angela K ❀ Anna Orlova ❀ anonymous ❀ Antan Karmola ❀ Anthony Edlin ❀ Antoine Antonio Mendoza ❀ Antonio Parisi ❀ Ardash Crowfoot ❀ Arjun Chennu ❀ Arne Brix ❀ Arturo J. Pérez ❀ Aslak Kjølås-Sæverud Axel Bordelon ❀ Axel Philipsenburg ❀ barbix ❀ BataMoth ❀ Ben Evans ❀ Bernd ❀ Betsy Luntao ❀ Birger Tuer Thorvaldsen Boonsak Watanavisit ❀ Boris Fauret ❀ Boudewijn Rempt ❀ BoxBoy ❀ Brent Houghton ❀ Brett Smith ❀ Brian Behnke ❀ Bryan Butler BS ❀ Bui Dang Hai Trieu ❀ BXS ❀ carlos levischi ❀ Carola Hänggi ❀ Cedric Wohlleber ❀ Charlotte Lacombe-bar ❀ Chris Radcliff Chris Sakkas ❀ Christian Gruenwaldner ❀ Christophe Carré ❀ Christopher Bates ❀ Clara Dexter ❀ codl ❀ Colby Driedger Conway Scott Smith ❀ Coppin Olivier ❀ Cuthbert Williams ❀ Cyol ❀ Cyrille Largillier ❀ Cyril Paciullo ❀ Damien ❀ Daniel Daniel Björkman ❀ Danny Grimm ❀ David ❀ David Tang ❀ DiCola Jamn ❀ Dmitry ❀ Donald Hayward ❀ Duke ❀ Eitan Goldshtrom Enrico Billich ❀ epsilon ❀ Eric Schulz ❀ Faolan Grady ❀ Francois Schnell ❀ freecultureftw ❀ Garret Patterson ❀ Ginny Hendricks GreenAngel5 ❀ Grigory Petrov ❀ G. S. Davis ❀ Guillaume ❀ Guillaume Ballue ❀ Gustav Strömbom ❀ Happy Mimic ❀ Helmar Suschka Henning Döscher ❀ Henry Ståhle ❀ Ilyas ❀ Irina Rempt ❀ Ivan Korotkov ❀ James Frazier ❀ Jamie Sutherland ❀ Janusz ❀ Jared Tritsch JDB ❀ Jean-Baptiste Hebbrecht ❀ Jean-Gabriel LOQUET ❀ Jeffrey Schneider ❀ Jessey Wright ❀ Jim ❀ Jim Street ❀ Jiska JoÃ£o Luiz Machado Junior ❀ Joerg Raidt ❀ Joern Konopka ❀ joe rutledge ❀ John ❀ John ❀ John Urquhart Ferguson ❀ Jónatan Nilsson Jonathan Leroy ❀ Jonathan Ringstad ❀ Jon Brake ❀ Jorge Bernal ❀ Joseph Bowman ❀ Juju Mendivil ❀ Julien Duroure ❀ Justus Kat Kai-Ting (Danil) Ko ❀ Kasper Hansen ❀ Kate ❀ Kathryn Wuerstl ❀ Ken Mingyuan Xia ❀ Kingsquee ❀ Kroet ❀ Lars Ivar Igesund Levi Kornelsen ❀ Liang ❀ Liselle ❀ Lise-Lotte Pesonen ❀ Lorentz Grip ❀ Louis Yung ❀ L S ❀ Luc Stepniewski ❀ Luke Hochrein ❀ MacCoy Magnus Kronnäs ❀ Manuel ❀ Manu Järvinen ❀ Marc & Rick ❀ marcus ❀ Martin Owens ❀ Mary Brownlee ❀ Masked Admirer Mathias Stærk ❀ mefflin ross bullis-bates ❀ Michael ❀ Michael Gill ❀ Michael Pureka ❀ Michelle Pereira Garcia ❀ Mike Mosher Miroslav ❀ mjkj ❀ Nazhif ❀ Nicholas DeLateur ❀ Nicholas Terranova ❀ Nicki Aya ❀ Nicola Angel ❀ Nicolae Berbece ❀ Nicole Heersema Nielas Sinclair ❀ NinjaKnight Comics ❀ Noble Hays ❀ Noelia Calles Marcos ❀ Nora Czaykowski ❀ No Reward ❀ Nyx ❀ Olivier Amrein Olivier Brun ❀ Olivier Gavrois ❀ Omar Willey ❀ Oscar Moreno ❀ Öykü Su Gürler ❀ Ozone S. ❀ Pablo Lopez Soriano ❀ Pat David Patrick Gamblin ❀ Paul ❀ Paul ❀ Pavel Semenov ❀ Pet0r ❀ Peter ❀ Peter Moonen ❀ Petr Vlašic ❀ Philippe Jean Edward Bateman Pierre Geier ❀ Pierre Vuillemin ❀ Pranab Shenoy ❀ Pyves & Ran ❀ Raghavendra Kamath ❀ Rajul Gupta ❀ Reorx Meng ❀ Ret Samys rictic ❀ RJ van der Weide ❀ Roberto Zaghis ❀ Robin Moussu ❀ Roman Burdun ❀ Rumiko Hoshino ❀ Rustin Simons ❀ Sally Bridgewater Sami T ❀ Samuel Mitson ❀ Scott Petrovic ❀ Sean Adams ❀ Shadefalcon ❀ ShadowMist ❀ shafak ❀ Shawn Meyer ❀ Simon Forster Simon Isenberg ❀ Sonja Reimann-Klieber ❀ Sonny W. ❀ Soriac ❀ Stanislav Vodetskyi ❀ Stephanie Cheshire ❀ Stephen Bates Stephen Smoogen ❀ Steven Bennett ❀ Stuart Dickson ❀ surt ❀ Sybille Marchetto ❀ TamaskanLEM ❀ tar8156 ❀ Terry Hancock TheFaico ❀ thibhul ❀ Thomas Citharel ❀ Thomas Courbon ❀ Thomas Schwery ❀ Thornae ❀ Tim Burbank ❀ Tim J. ❀ Tomas Hajek Tom Demian ❀ Tom Savage ❀ Tracey Reuben ❀ Travis Humble ❀ tree ❀ Tyson Tan ❀ Urm ❀ Victoria ❀ Victoria White Vladislav Kurdyukov ❀ Vlad Tomash ❀ WakoTabacco ❀ Wander ❀ Westen Curry ❀ Witt N. Vest ❀ WoodChi ❀ Xavier Claude Yalyn Vinkindo ❀ Yaroslav ❀ Zeni Pong ❀ Źmicier Kušnaroŭ ❀ Глеб Бузало ❀ 獨孤欣 & 獨弧悦

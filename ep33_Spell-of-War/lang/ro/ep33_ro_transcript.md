# Transcript of Pepper&Carrot Episode 33 [ro]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodul 33: Vrajă de Război

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Enemy|1|False|huUVOOOOOOOOOOO !
Army|2|False|Rawwwr!
Army|5|False|Grrawwr!
Army|4|False|Grrrr!
Army|3|False|Yuurrr!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|OK, tânără vrăjitoare.
King|2|False|Dacă ai o vrajă care ne poate ajuta, e acum ori niciodată!
Pepper|3|True|Am înțeles!
Pepper|4|False|Pregătește-te să fii martor la...
Sound|5|False|Dzziii ! !|nowhitespace
Pepper|6|False|...CAPODOPERA MEA!
Sound|7|False|Dzziiii ! !|nowhitespace
Pepper|8|False|Realitas Hackeris Pepperus!
Sound|9|False|Dzziooo ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Fiizz!
Sound|2|False|Dzii!
Sound|3|False|Schii!
Sound|4|False|Ffhii!
Sound|8|False|Dziing!
Sound|7|False|Fiizz!
Sound|6|False|Schii!
Sound|5|False|Ffhii!
King|9|True|Și, vraja asta ne face armele noastre mai puternice...
King|10|False|...în timp ce reduce puterea armelor inamicilor?
Sound|11|False|Dzii...
Pepper|12|True|Heh.
Pepper|13|True|O să vezi!
Pepper|14|False|Tot ce îți pot spune este că astăzi nu o să pierzi nici un soldat.
Pepper|15|False|Dar tot va trebui să lupți cu tot ce ai!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|Doar pentru asta ne-am antrenat.
King|2|False|LA COMANDA MEA!
Army|3|False|Daaa!
Army|4|False|Urraaa!
Army|5|False|Daaa!
Army|6|False|Uraaaa!!
Army|7|False|Daaa!!
Army|8|False|Daaa!!
Army|9|False|Daaa!!
Army|10|False|Daaa!
Army|11|False|Daaa!!
King|12|False|LA ATAAAAAC!!!
Army|13|False|Daaa!
Army|14|False|Uraaa!
Army|15|False|Uraaa!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|Haahhhh!!!
Enemy|2|False|YUuurr!!!
Sound|3|False|Șuiiing ! !|nowhitespace
Sound|4|False|Suouș ! !|nowhitespace
Writing|5|False|12
Enemy|6|False|?!!
Writing|7|False|8
King|8|False|?!!
Writing|9|False|64
Writing|10|False|32
Writing|11|False|72
Writing|12|False|0
Writing|13|False|64
Writing|14|False|0
Writing|15|False|56
Army|20|False|Yrrr!
Army|17|False|Yurr!
Army|19|False|Grrr!
Army|21|False|Yaaah!
Army|18|False|Yeaaah!
Army|16|False|Yaawww!
Sound|27|False|Sșing
Sound|25|False|Fffchh
Sound|22|False|wiizz
Sound|23|False|Suouș
Sound|24|False|Ciklong
Sound|26|False|Cikilng

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|?!
Writing|2|False|3
Writing|3|False|24
Writing|4|False|38
Writing|5|False|6
Writing|6|False|12
Writing|7|False|0
Writing|8|False|5
Writing|9|False|0
Writing|10|False|37
Writing|11|False|21
Writing|12|False|62
Writing|13|False|27
Writing|14|False|4
King|15|False|! !|nowhitespace
King|16|False|VRĂJITOAAREOO!!!! CE AI FĂCUT?!!!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tadaaa !
Pepper|2|True|“Asta"
Pepper|3|False|...'mi-este capodopera!
Pepper|4|False|Este o vrajă complexă ce alterează realitatea și arată punctele de viață rămase ale oponentului.
Writing|5|False|33
Pepper|6|True|Când nu mai ai puncte de viață, părăsești câmpul de luptă până la finalul bătăliei.
Pepper|7|True|Cine elimină primul toți soldații inamici, câștigă!
Pepper|8|False|Simplu.
Writing|9|False|0
Army|10|False|?
Pepper|11|True|Nu-i așa că e minunat?
Pepper|12|True|Nu moare nimeni!
Pepper|13|True|Niciun soldat nu este rănit!
Pepper|14|False|Asta va revoluționa războaiele!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Și acum că știți regulile, hai să resetez scorul, și o luați de la capăt!
Pepper|2|False|Așa e mai corect.
Writing|3|False|64
Writing|4|False|45
Writing|5|False|6
Writing|6|False|2
Writing|7|False|0
Writing|8|False|0
Writing|9|False|0
Writing|10|False|0
Writing|11|False|9
Writing|12|False|5
Writing|13|False|0
Writing|14|False|0
Writing|15|False|0
Sound|16|False|Șsing!
Sound|17|False|Zuuu!
Sound|19|False|ciac!
Sound|18|False|poc!
Pepper|20|True|Fugi mai repede, Carrot!
Pepper|21|False|Vraja nu o să mai țină prea mult!
Narrator|22|False|- SFÂRȘIT -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|1|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|2|False|You can also translate this page if you want.
<hidden>|3|False|Beta readers help with the story, proofreaders give feedback about the text.
Credits|4|False|29 Iunie, 2020 Artă & scenariu: David Revoy. Cititori Beta: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. Versiune limba română Traducere: Florin Șandru Bazat pe universul Hereva Creator: David Revoy. Administrator principal: Craig Maloney. Scriitori: Craig Maloney, Nartance, Scribblemaniac, Valvin. Corectura: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.3, Inkscape 1.0 on Kubuntu 19.10. Licență: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|5|True|Știai că..?
Pepper|6|True|Pepper&Carrot este complet gratuit, open-source și sponsorizat prin patronajul cititorilor.
Pepper|7|False|Pentru acest episod, mulțumiri celor 1190 de patroni!
Pepper|8|True|Și tu poți deveni patron al Pepper&Carrot și vei avea numele trecut aici!
Pepper|9|True|Suntem pe Patreon, Tipeee, PayPal, Liberapay ...și altele!
Pepper|10|True|Verifică pe www.peppercarrot.com mai multe detalii!
Pepper|11|False|Mulțumim!

# Transcript of Pepper&Carrot Episode 33 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 33: A’ gheas-chogaidh

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Nàmhaid|1|False|huubhÙÙÙÙÙÙÙÙÙÙ !
Feachd|2|False|Rrraoic!
Feachd|5|False|Glaodh!
Feachd|4|False|Grrrr!
Feachd|3|False|Rrràn!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rìgh|1|True|Sin sin a-nist, a bhana-bhuidsich òig.
Rìgh|2|False|Ma tha geas agad a chuidicheas leinn, seo an t-àm!
Peabar|3|True|Tha mi agaibh!
Peabar|4|False|Thoiribh fianais air…
Fuaim|5|False|Tssiii ! !|nowhitespace
Peabar|6|False|…ANNAS mo LÀIMHE!!
Fuaim|7|False|Tssiiii ! !|nowhitespace
Peabar|8|False|Fìorachdas Hackeris Peabarus!
Fuaim|9|False|Pssiuuu ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|Fiiss!
Fuaim|2|False|Tsii!
Fuaim|3|False|Siiii!
Fuaim|4|False|Ffiiii!
Fuaim|8|False|Tsiing!
Fuaim|7|False|Fiiss!
Fuaim|6|False|Siiii!
Fuaim|5|False|Ffiiii!
Rìgh|9|True|Nise, an dèan a’ gheas seo na claidheamhan againn nas cumhachdaiche…
Rìgh|10|False|…’s an lagaich i claidheamhan ar nàimhdean?
Fuaim|11|False|Tsii...
Peabar|12|True|Hè.
Peabar|13|True|Tuigidh sibh e an ceartuair!
Peabar|14|False|Cha chan mi càil ach nach caill sibh saighdear sam bith an-diugh.
Peabar|15|False|Gidheadh, feumaidh sibh sabaid fhathast is ur dìcheall a dhèanamh!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rìgh|1|False|Nì gu dearbh agus sinne dèanta ri cogadh.
Rìgh|2|False|Air m’ ÒÒÒÒÒÒÒRDUGH!
Feachd|3|False|Àidh!
Feachd|4|False|Àidh!
Feachd|5|False|Àidh!
Feachd|6|False|Àidh!
Feachd|7|False|Àidh!
Feachd|8|False|Àidh!
Feachd|9|False|Àidh!
Feachd|10|False|Àidh!
Feachd|11|False|Àidh!
Rìgh|12|False|IOOOOOONNSAIGH!!!
Feachd|13|False|Àidh!
Feachd|14|False|Àidh!
Feachd|15|False|Àidh!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rìgh|1|False|Èigh !!!
Nàmhaid|2|False|RAOic!!!
Fuaim|3|False|Strrrrràc ! !|nowhitespace
Fuaim|4|False|Fruuis ! !|nowhitespace
Sgrìobhte|5|False|12
Nàmhaid|6|False|?!!
Sgrìobhte|7|False|8
Rìgh|8|False|?!!
Sgrìobhte|9|False|64
Sgrìobhte|10|False|32
Sgrìobhte|11|False|72
Sgrìobhte|12|False|0
Sgrìobhte|13|False|64
Sgrìobhte|14|False|0
Sgrìobhte|15|False|56
Feachd|20|False|Èigh!
Feachd|17|False|Raoic!
Feachd|19|False|Grrr!
Feachd|21|False|Àidh!
Feachd|18|False|Àidh!
Feachd|16|False|Èigh!
Fuaim|27|False|Ssiing
Fuaim|25|False|Fffichh
Fuaim|22|False|Bhiiss
Fuaim|23|False|Fruuis
Fuaim|24|False|Gloing
Fuaim|26|False|Gliong

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rìgh|1|False|?!
Sgrìobhte|2|False|3
Sgrìobhte|3|False|24
Sgrìobhte|4|False|38
Sgrìobhte|5|False|6
Sgrìobhte|6|False|12
Sgrìobhte|7|False|0
Sgrìobhte|8|False|5
Sgrìobhte|9|False|0
Sgrìobhte|10|False|37
Sgrìobhte|11|False|21
Sgrìobhte|12|False|62
Sgrìobhte|13|False|27
Sgrìobhte|14|False|4
Rìgh|15|False|! !|nowhitespace
Rìgh|16|False|A BHANA-BHUIDSICH!!!! DÈ CHUIR THU OIRNN?!!!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Hò-rò !
Peabar|2|True|Siud…
Peabar|3|False|…annas mo làimhe!
Peabar|4|False|’S e geas iom-fhillte a th’ innte a dh’atharraicheas an fhìorachd ’s a sheallas na puingean-beatha a tha air fhàgail dha do nàmhaid.
Sgrìobhte|5|False|33
Peabar|6|True|Ma bhios tu air do phuingean-beatha uile a chall, fàgaidh tu am blàr gus am bi e seachad.
Peabar|7|True|Thèid an latha leis an fheachd a bheireas na puingean-beatha uile air falbh o shaighdearan an nàmhad!
Peabar|8|False|Furasta.
Sgrìobhte|9|False|0
Feachd|10|False|?
Peabar|11|True|Nach eil sin math fhèin?!
Peabar|12|True|Gun saighdear ’na shìneadh!
Peabar|13|True|Gun saighdear air leòn!
Peabar|14|False|Cha mhill an cogadh duine beò gu bràth tuilleadh!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Nise on a tha sinn uile eòlach air na riaghailtean, nach ath-shuidhich mi na sgòran dhuibh ach an tòisicheamaid às ùr?
Peabar|2|False|Bidh e nas cothromaiche mar sin.
Sgrìobhte|3|False|64
Sgrìobhte|4|False|45
Sgrìobhte|5|False|6
Sgrìobhte|6|False|2
Sgrìobhte|7|False|0
Sgrìobhte|8|False|0
Sgrìobhte|9|False|0
Sgrìobhte|10|False|0
Sgrìobhte|11|False|9
Sgrìobhte|12|False|5
Sgrìobhte|13|False|0
Sgrìobhte|14|False|0
Sgrìobhte|15|False|0
Fuaim|16|False|Ssiiing!
Fuaim|17|False|Ffruuis!
Fuaim|19|False|sgiot!
Fuaim|18|False|poc!
Peabar|20|True|Thoir do chasan leat, a Churrain!
Peabar|21|False|Cha mhair a’ gheas ach greiseag!
Neach-aithris|22|False|- Deireadh na sgeòil -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|1|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|2|False|You can also translate this page if you want.
<hidden>|3|False|Beta readers help with the story, proofreaders give feedback about the text.
Urram|4|False|29mh dhen Ògmhios 2020 Obair-ealain ⁊ sgeulachd: David Revoy. Leughadairean Beta: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. Tionndadh Gàidhlig Eadar-theangachadh: GunChleoc . Stèidhichte air saoghal Hereva Air a chruthachadh le: David Revoy. Prìomh neach-glèidhidh: Craig Maloney. Sgrìobhadairean: Craig Maloney, Nartance, Scribblemaniac, Valvin. Ceartachadh: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Bathar-bog: Krita 4.3, Inkscape 1.0 air Kubuntu 19.10. Ceadachas: Creative Commons Attribution 4.0. www.peppercarrot.com
Peabar|5|True|An robh fios agad?
Peabar|6|True|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean.
Peabar|7|False|Mòran taing dhan 1190 pàtran a thug taic dhan eapasod seo!
Peabar|8|True|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd is chì thu d’ àinm an-seo!
Peabar|9|True|Tha sinn air Patreon, Tipeee, PayPal, Liberapay ...’s a bharrachd!
Peabar|10|True|Tadhail air www.peppercarrot.com airson barrachd fiosrachaidh!
Peabar|11|False|Mòran taing!

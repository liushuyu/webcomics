# Transcript of Pepper&Carrot Episode 33 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute luka luka tu wan: utala kepeken wawa nasa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan ike|1|False|MuuUUUUUUUUUUUUUUU !
kulupu utala|2|False|aaaaaa!
kulupu utala|5|False|aaaaaaaa!
kulupu utala|4|False|aaaa!
kulupu utala|3|False|aaaaaa!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa|1|True|pona, jan lili pi wawa nasa o.
jan lawa|2|False|sina sona e wawa pona tawa utala mi la, o pali lon tenpo ni taso!
jan Pepa|3|True|mi sona!
jan Pepa|4|False|o lukin...
kalama|5|False|Sssuno ! !|nowhitespace
jan Pepa|6|False|...E PALI SULI MI!
kalama|7|False|Sssuno ! !|nowhitespace
jan Pepa|8|False|Antekonas Lonas Pepas!
kalama|9|False|Wwwawa ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|Laaso!
kalama|2|False|Laaso!
kalama|3|False|Lasoo!
kalama|4|False|Laaaso!
kalama|8|False|Jjjelo!
kalama|7|False|Jjjelo!
kalama|6|False|Jjelo!
kalama|5|False|Jjeelo!
jan lawa|9|True|sin la wawa ni li wawa e palisa utala mi...
jan lawa|10|False|...li pakala e palisa utala pi jan ike mi anu seme?
kalama|11|False|Laso...
jan Pepa|12|True|a a.
jan Pepa|13|True|sina kama lukin!
jan Pepa|14|False|mi toki e ni taso: jan sina li kama anpa ala lon tenpo ni.
jan Pepa|15|False|taso o utala kin, o pana kin e wawa sina!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa|1|False|a, mi kama tan ni.
jan lawa|2|False|O TAWA LON TOOKIIIII!
kulupu utala|3|False|aaaaaa!
kulupu utala|4|False|pona a!
kulupu utala|5|False|pona aa!
kulupu utala|6|False|pona aaa!
kulupu utala|7|False|aaaaaa!
kulupu utala|8|False|aaaa!
kulupu utala|9|False|aaaa!
kulupu utala|10|False|aaaaa!
kulupu utala|11|False|pona aa!
jan lawa|12|False|O TAAWAAAAAAAA!!!
kulupu utala|13|False|tawa aaa!
kulupu utala|14|False|aaaa!
kulupu utala|15|False|aaaaa!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa|1|False|aaaaa!!!
jan ike|2|False|aaaaa!!!
kalama|3|False|Taawaaa ! !|nowhitespace
kalama|4|False|Konnn ! !|nowhitespace
sitelen|5|False|12
jan ike|6|False|?!!
sitelen|7|False|8
jan lawa|8|False|?!!
sitelen|9|False|64
sitelen|10|False|32
sitelen|11|False|72
sitelen|12|False|0
sitelen|13|False|64
sitelen|14|False|0
sitelen|15|False|56
kulupu utala|20|False|aaaa!
kulupu utala|17|False|aaa!
kulupu utala|19|False|ike a!
kulupu utala|21|False|aaaa!
kulupu utala|18|False|aaaaa!
kulupu utala|16|False|aaaaa!
kalama|27|False|Tawaa
kalama|25|False|Konnn
kalama|22|False|Konn
kalama|23|False|Tawwaa
kalama|24|False|Kkiwen
kalama|26|False|Kkiwen

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa|1|False|?!
sitelen|2|False|3
sitelen|3|False|24
sitelen|4|False|38
sitelen|5|False|6
sitelen|6|False|12
sitelen|7|False|0
sitelen|8|False|5
sitelen|9|False|0
sitelen|10|False|37
sitelen|11|False|21
sitelen|12|False|62
sitelen|13|False|27
sitelen|14|False|4
jan lawa|15|False|! !|nowhitespace
jan lawa|16|False|JAN PI WAWA NASA O!!!! SINA PALI E SEME?!!!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|o luuukin !
jan Pepa|2|True|ni...
jan Pepa|3|False|...li pali suli mi!
jan Pepa|4|False|wawa ni li kepeken pali sona mute, li ante e lon. ona li pana e nanpa tawa awen pi jan ike sina.
sitelen|5|False|33
jan Pepa|6|True|nanpa sina li ala la, o weka tan ma utala tawa tenpo utala pini.
jan Pepa|7|True|kulupu utala wan li weka e jan ike ona ale la, ona li nanpa wan!
jan Pepa|8|False|nasin suwi a.
sitelen|9|False|0
kulupu utala|10|False|?
jan Pepa|11|True|ni li pona anu seme?
jan Pepa|12|True|moli li lon ala!
jan Pepa|13|True|jan utala li pakala ala!
jan Pepa|14|False|ni li ante e nasin utala ale a!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|sina sona e nasin lon tenpo ni la, mi ken sin e nanpa tawa utala open sin !
jan Pepa|2|False|ni li nasin pona tawa ale.
sitelen|3|False|64
sitelen|4|False|45
sitelen|5|False|6
sitelen|6|False|2
sitelen|7|False|0
sitelen|8|False|0
sitelen|9|False|0
sitelen|10|False|0
sitelen|11|False|9
sitelen|12|False|5
sitelen|13|False|0
sitelen|14|False|0
sitelen|15|False|0
kalama|16|False|Sssike!
kalama|17|False|Tawaa!
kalama|19|False|Poki!
kalama|18|False|Poki!
jan Pepa|20|True|o tawa wawa, soweli Kawa o!
jan Pepa|21|False|wawa ni li awen ala lon tenpo!
sitelen toki|22|False|- PINI -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|1|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|2|False|You can also translate this page if you want.
<hidden>|3|False|Beta readers help with the story, proofreaders give feedback about the text.
mama|4|False|tenpo June 29, 2020 musi sitelen & toki: jan David Revoy. jan pi lukin pona: Arlo James Barnes en jan Craig Maloney en jan GunChleoc en jan Hyperbolic Pain en jan Karl Ove Hufthammer en jan Martin Disch en jan Nicolas Artance en jan Quetzal2 en jan Valvin. toki pona ante toki: jan Ret Samys . musi ni li tan pali po ma Elewa mama: jan David Revoy. jan awen nanpa wan: jan Craig Maloney. jan pi toki sitelen: jan Craig Maloney en jan Nartance en jan Scribblemaniac en jan Valvin. jan pi pona pali: jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson . ilo: ilo Krita 4.3 en ilo Inkscape 1.0 li kepeken ilo Kubuntu 19.10. nasin pi lawa jo: Creative Commons Attribution 4.0. www.peppercarrot.com
jan Pepa|5|True|sina sona ala sona?
jan Pepa|6|True|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute.
jan Pepa|7|False|jan 1190 li pana e mani la, lipu ni li lon!
jan Pepa|8|True|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa. nimi sina li ken lon ni!
jan Pepa|9|True|ilo Patreon en ilo Tipeee en ilo PayPal en ilo Liberapay en ilo ante la, mi lon!
jan Pepa|10|True|sina wile sona la, o lukin e lipu www.peppercarrot.com !
jan Pepa|11|False|sina pona!

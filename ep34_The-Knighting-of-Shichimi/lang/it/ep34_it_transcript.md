# Transcript of Pepper&Carrot Episode 34 [it]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodio 34: La cerimonia di Shichimi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|...ed ora, in questa notte, ti diamo il benvenuto, Shichimi, come la più giovane tra i Cavalieri di Ah.
Hibiscus|2|False|Ancora nessuna traccia di Pepper?
Coriandolo|3|False|Quella stessa notte...
Zafferano|4|False|Se non arriva in tempo si perderà il discorso di Shichimi
Zafferano|5|False|Grazie mille.
Shichimi|6|False|Io vorrei...
Shichimi|7|False|No, non ancora.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Scusa per l'entrata teatrale ed il ritardo!|nowhitespace
Sound|2|False|Fiu uMMMM|nowhitespace
Pepper|3|True|Fiuu M M M MM
Pepper|4|True|Sto arrivando!
Pepper|5|False|Largooo!
Sound|6|False|LAAAARGOOOO!!!|nowhitespace
Sound|7|False|CR A S H!
Sound|8|False|Oooops!
Shichimi|9|False|Tutti OK? Niente di rotto?
Pepper|10|True|Pepper!
Pepper|11|True|Ciao Shichimi!
Pepper|12|False|Ho corso tutto il giorno, ma è una lunga storia.
<hidden>|13|False|Dear translator: if editing the soundFX is difficult, I made a special documentation here: https://www.peppercarrot.com/en/static14/documentation&page=055_Sound-Effect_translation

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|hiii hiii
Shichimi|2|False|Shichimi,
Pepper|3|True|Lei è una delle tue invitate?
Pepper|4|False|Si, è la mia amica Pepper. È tutto a posto.
Pepper|5|True|Carrot, tutto OK?
Pepper|6|False|Scusa perl'atter-raggio, non ho ancora il perfetto controllo dell'iper-velocità.
Shichimi|7|False|E mi scuso ancora con tutti per il disturbo,
Wasabi|8|True|e di come sono vestita...
Wasabi|9|False|questa giovane strega che è appena arrivata, è davvero una tua amica?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Si, vostra Altezza.
Shichimi|2|False|Il suo nome è Pepper, della Scuola di Chaosah.
Wasabi|3|True|La sua presenza qui inquina la natura sacra della nostra scuola.
Wasabi|4|False|Allontanala dalla mia vista. Subito!!
Shichimi|5|True|Ma...
Shichimi|6|False|Maestra Wasabi...
Wasabi|7|True|Ma... cosa?
Wasabi|8|False|Preferisci essere espulsa dalla nostra scuola?
Shichimi|9|False|! ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Scusa Pepper, ma devi andartene.
Shichimi|2|False|Adesso.
Pepper|3|True|Hoh?
Pepper|4|False|Hei hei hei, aspetta un attimo! Sono sicura che qui ci sia un equivoco.
Shichimi|5|False|Per favore Pepper non mettermi in difficoltà.
Pepper|6|False|Hei tu! Lì sul trono, se hai dei problemi con me, vieni qui e dimmelo in faccia!
Wasabi|7|False|Tsss...
Wasabi|8|True|Shichimi, hai dieci secondi...
Wasabi|9|True|nove...
Wasabi|10|True|otto...
Wasabi|11|False|sette...
Shichimi|12|False|BASTA, PEPPER! VATTENE VIA!!!
Sound|13|False|SHRRIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Shichimi, ti prego calmat...
Sound|2|False|B ADuuM!|nowhitespace
Shichimi|3|True|VAI... VIA!!!
Shichimi|4|True|VAI VIA!!!
Shichimi|5|False|VAI VIA!!!
Sound|6|False|C R E E E E E! !!|nowhitespace
Pepper|7|True|Haia!
Pepper|8|False|Hei! Questo... n-NON... Ouch... è carino!
Coriandolo|9|False|SHICHIMI! PEPPER! FERMATEVI!
Zafferano|10|False|Aspetta.
Wasabi|11|False|Hmm!
Pepper|12|True|Grrr!
Pepper|13|False|OK, ve la siete cercata!
Sound|14|False|B R Z OO!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Non obbligarmi a farti ancora più male!
Sound|2|False|Oh, il mio incantesimo ha funzionato benissimo, solo non eri tu il mio obiettivo.|nowhitespace
Pepper|3|False|CURSUS CANCELLARE MAXIMUS!!!
Sound|4|False|S H K L AK!|nowhitespace
Shichimi|5|True|Ouch!
Shichimi|6|True|P AF !!
Shichimi|7|False|Anche il tuo miglior incantesimo di cancellazione non ha alcun effetto su di me!
Pepper|8|False|Rinuncia, Pepper, e vattene via!
Shichimi|9|True|Ah?
Shichimi|10|False|Cosa vuoi dire?!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Considerati fortunata!
Pepper|2|True|?!!
Pepper|3|False|Ho appena cancellato il suo incantesimo Glamour che la fa sembrare giovane.
Pepper|4|True|Mi sono accorta dell'incantesimo non appena sono arrivata.
Pepper|5|False|Ecco, vi ho dato una piccola dimostrazione di quello che vi meritate per aver obbligato Shichimi a combattere contro di me!
Wasabi|6|True|INSOLENTE!
Wasabi|7|True|Come osi,
Wasabi|8|False|di fronte a tutta la mia scuola!
Pepper|9|True|Se avessi avuto tutta la mia Rea, non mi sarei fermata qui.
Pepper|10|False|Lei... era il mio obiettivo!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Vedo, hai raggiunto il livello di chi ti aveva preceduto molto prima di quanto avessi previsto...|nowhitespace
Wasabi|2|True|MI HANNO APPENA ATTACCATA E VOI STATE LÌ COME DEI BACCALÀ?!! PRENDETELA!!!
Wasabi|3|False|DR Z OW!!
Pepper|4|True|Questo accelera i miei piani, ma è una buona notizia.
Pepper|5|True|I tuoi piani?
Pepper|6|False|Quindi mi stavi solo mettendo alla prova e questo non aveva niente a che fare con il mio ritardo?
Wasabi|7|True|Tu sei davvero fuori!
Wasabi|8|False|sii...
Wasabi|9|True|sii.
Wasabi|10|False|PRENDETELA!!!
Wasabi|11|False|Shichimi, ne dovremo parlare più tardi!
Wasabi|12|False|Scusami Carrot, ma dobbiamo decollare ancora ad ipervelocità.
Pepper|13|False|Tieniti forte!
Pepper|14|True|Tap!
Pepper|15|False|Tap!
Sound|16|False|Fiiuu MM!!
Sound|17|False|COSA STATE LÌ IMPALATI?!!
Sound|18|False|LA VOGLIO VIVA!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|? !
Wasabi|2|False|PRENDETELA!!!
Pepper|3|False|Oh cielo!
Zafferano|4|False|Pepper, prendi la mia scopa!
Sound|5|False|Fizzz!
Pepper|6|True|Oh wow!
Pepper|7|False|Grazie mille Zafferano!
Sound|8|False|Toc!
Sound|9|False|fiuuu U M MM !|nowhitespace
Narrator|10|False|CONTINUA...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot è completamente libero (gratuito), open-source e sostenuto grazie alle gentili donazioni dei lettori.
Pepper|2|True|Per questo episodio, grazie ai 1096 donatori!
Pepper|3|True|Lo sapevi?
Pepper|4|False|Marzo 31, 2021 Disegni e sceneggiatura: David Revoy. Co-autori del testo: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Versione in italiano Traduzione: Carlo Gandolfi. Correzioni: Antonio Parisi. Un ringraziamento speciale a Nartance per aver esplorarato il personaggio di Wasabi nei suoi racconti di fan-fiction. Il modo in cui l'ha immaginata ha avuto un grande impatto su come l'ho rappresentata in questo episodio. Basato sull'universo di Hereva Creato da: David Revoy. Amministratore: Craig Maloney. Autori: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correzioni: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.4.1, Inkscape 1.0.2 su Kubuntu Linux 20.04. Licenza: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|5|True|Grazie mille!
Pepper|6|True|Anche tu puoi diventare un sostenitore di Pepper&Carrot ed avere il tuo nome in questo elenco!
Pepper|7|False|Leggi www.peppercarrot.com per ulteriori informazioni!
Pepper|8|False|Siamo su Patreon, Tipeee, Paypal, Liberapay …ed altri!
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

# Transcript of Pepper&Carrot Episode 34 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 34: Ceumnachadh Sidimi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|1|False|Air an dearbh oidhche…
Ìbisg|2|False|…agus le sin bheir sinn fàilte ort a-nochd, a Shidsimi, ’nad Bhan-ridire Achd as òige.
Costag|3|False|A bheil sgeul air Peabar fhathast?
Cròch|4|False|Chan eil fhathast.
Cròch|5|False|Mura cuir i greas oirre, caillidh i òraid Sidimi.
Sidimi|6|False|Mòran taing.
Sidimi|7|False|Bu toigh l’…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|Fru uuuuuu|nowhitespace
Fuaim|2|False|Fruuu ui S SS|nowhitespace
Peabar|3|True|A’ tighinn!
Peabar|4|True|Thoiribh an aire!
Peabar|5|False|AN AIRE!!!
Fuaim|6|False|SA I D E !|nowhitespace
Fuaim|7|False|H-opag!
Fuaim|8|False|’Eil sibh ceart gu leòr? Gun chnàmhan briste?
Sidimi|9|False|A Pheabar!
Peabar|10|True|Sin thu fhèin, a Shidimi!
Peabar|11|True|Gabh mo leisgeul gun dàinig mi a-steach le fruis is anmoch!
Peabar|12|False|Tha mi air mo dheann-ruith fad an latha, ach sin sgeulachd eile.
<hidden>|13|False|Dear translator: if editing the soundFX is difficult, I made a special documentation here: https://www.peppercarrot.com/en/static14/documentation&page=055_Sound-Effect_translation

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ìbisg|1|False|An e tè de na h-aoighean agad-sa a th’ innte?
Sidimi|2|False|’S e. Seo mo charaid Peabar. Na gabhaibh dragh.
Peabar|3|True|A Churrain, a bheil thu gu math?
Peabar|4|False|Tha mi duilich mun landadh, feumaidh mi ionnsachadh fhathast air far-luaths.
Peabar|5|True|A-rithist, gabhaibh mo leisgeul airson na trioblaide,
Peabar|6|False|agus a thaobh m’ èididh…
Sidimi|7|False|tì hì
Raidis|8|True|A Shidimi,
Raidis|9|False|a’ bhana-bhuidseach òg a tha air a tighinn, an e caraid dhut a th’ innte ann an da-rìribh ?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sidimi|1|True|’S e, a Mhòrachd.
Sidimi|2|False|’S e Peabar an t-ainm a th’ oirre is buinidh i do Sgoil na Dubh-choimeasgachd.
Raidis|3|True|Tha a làthaireachd a’ truailleadh naomhachd ar sgoile.
Raidis|4|False|Gun cuireadh tu a-mach à mo shealladh i gun dàil.
Sidimi|5|True|Ach…
Sidimi|6|False|A’ bhana-mhaighstir Raidis…
Raidis|7|True|B’ àill leat?
Raidis|8|False|Am b’ fheàrr leat gun rachadh d’ fhògradh on sgoil againn?
Sidimi|9|False|! ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sidimi|1|True|Tha mi duilich, a Pheabar, ach feumaidh tu falbh.
Sidimi|2|False|Làrach nam bonn.
Peabar|3|True|Gu dè?
Peabar|4|False|Dad thusa! Air do shocair ort! Feumaidh gur e mì-thuigse a th’ ann.
Sidimi|5|False|A Pheabar a charaid, na dèan cùis dhuilich dheth.
Peabar|6|False|Haoigh! Sibhse, air a’ chathair ud. Ma tha duilgheadas agaibh leam, a-nuas leibh is innsibh fhèin dhomh e!
Raidis|7|False|Tsss…
Raidis|8|True|A Shidimi, tha deich diogan agad…
Raidis|9|True|a naoi…
Raidis|10|True|a h-ochd…
Raidis|11|False|a seachd…
Sidimi|12|False|FÒGHNAIDH NA DH’FHÒGHNAS, A PHEABAR! THALLA!!!
Fuaim|13|False|SRRIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|A Shidimi, gabh air do shoc…
Fuaim|2|False|B ADuuM!|nowhitespace
Sidimi|3|True|THALLA!!!
Sidimi|4|True|THALLA!!!
Sidimi|5|False|THALLA!!!
Fuaim|6|False|C R E E E E E! !!|nowhitespace
Peabar|7|True|Oit!
Peabar|8|False|Nise! Chan… eil SEO… Aobh… Càilear!
Costag|9|False|A SHIDSIMI! A PHEABAR! SGUIRIBH DHETH!
Cròch|10|False|Fuirich ort.
Raidis|11|False|An-dà…
Peabar|12|True|Grrr!
Peabar|13|False|Mas e sin na dh’iarras tu!
Fuaim|14|False|B R Si UU!!|nowhitespace

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|MIONNUS DUBHÀSARE MAXIMUS!!!
Fuaim|2|False|B R R AAG!|nowhitespace
Peabar|3|False|Oit!
Fuaim|4|False|PAIS!!|nowhitespace
Sidimi|5|True|Cha doir an dubhadh às as fheàrr a th’ agad buaidh orm-sa!
Sidimi|6|True|Gèill, a Pheabar, is thalla!
Sidimi|7|False|Gun sguirinn do ghoirteachadh!
Peabar|8|False|’S ann gun deach leis an dubhadh às ceart gu leòr ach cha b’ e tusa an t-amas.
Sidimi|9|True|Gu dè?
Sidimi|10|False|Cò air a tha thu a-mach?!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sidimi|1|False|?!!
Peabar|2|True|B’ i se an t-amas!
Peabar|3|False|Dhubh mi às a geas glàmair a chumas coltas òg oirre.
Peabar|4|True|Mhothaich mi dhan gheas sin cho luath ’s a ràinig mi.
Peabar|5|False|Nise, cha do chuir mi ach blasad beag oirbh de na tha sibh airidh air on a thug sibh air Sidimi sabaid ’nam aghaidh!
Raidis|6|True|ABAIR UABHAR!
Raidis|7|True|Ciamar a dhùraig thu
Raidis|8|False|’s air beulaibh na sgoil gu lèir agam!
Peabar|9|True|Cha chreid mi nach eil sibh fortanach!
Peabar|10|False|Nan robh mo Rèatha slàn annam, chan fhòghnadh sin dhomh.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|DR S UBH!!|nowhitespace
Raidis|2|True|Tha mi a’ tuigsinn. Ràinig thu ìre do shinnsearan mòran nas luaithe na bha mi ’n dùil…
Raidis|3|False|Cuiridh sin greasad air a’ phlana agam ach ’s fhiach.
Peabar|4|True|Plana, an e?
Peabar|5|True|’S ann nach robh sibh ach airson deuchainn a chur orm is nach robh mo mhaille ’na h-adhbhar idir?
Peabar|6|False|Abair car claon!
Raidis|7|True|tì…
Raidis|8|False|hì.
Raidis|9|True|DÈ THA SIBH UILE A’ COIMHEAD AIR?!
Raidis|10|False|THA MI AIR IONNSAIGH FHULANG IS CHA DÈAN SIBHSE DAD ACH SEASAMH AIR UR N-AIS!! GLACAIBH I!!!
Raidis|11|False|THA MI ’GA H-IARRAIDH BEÒ!
Raidis|12|False|GLACAIBH I!!!
Peabar|13|False|A Shidsimi, bruidhnidh sinn mu dhèidhinn uaireigin eile!
Peabar|14|True|Tha mi duilich, a Churrain, ach tha am far-luaths ’ga èigneachadh oirnn a-rithist.
Peabar|15|False|Glac mi teann!
Fuaim|16|False|Gnog!
Fuaim|17|False|Gnog!
Fuaim|18|False|Frruu iS!!|nowhitespace

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|? !|nowhitespace
Raidis|2|False|GLACAIBH I!!
Peabar|3|False|Obh obh.
Cròch|4|False|A Pheabar, gabh mo sguab-sa!
Fuaim|5|False|Fisss!
Peabar|6|True|A shaoghail!
Peabar|7|False|Mòran taing, a Chròch!
Fuaim|8|False|Poc!
Fuaim|9|False|Fruu U U U iS!|nowhitespace
Neach-aithris|10|False|RI LEANTAINN…

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|31ad dhen Mhàrt, 2021 Obair-ealain ⁊ sgeulachd: David Revoy. Leughadairean Beta: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Tionndadh Gàidhlig Eadar-theangachadh: GunChleoc. Taing shònraichte: do Nartance a chuir sùil air pearsa Raidis sna nobhailean ficsean luchd-leantainn aige. Thug an dòigh a bheachdaich e oirre buaidh mhòr air mar a chuir mi an cèill i san eapasod seo. Stèidhichte air saoghal Hereva Air a chruthachadh le: David Revoy. Prìomh neach-glèidhidh: Craig Maloney. Sgrìobhadairean: Craig Maloney, Nartance, Scribblemaniac, Valvin. Ceartachadh: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Bathar-bog: Krita 4.4.1, Inkscape 1.0.2 air Kubuntu Linux 20.04. Ceadachas: Creative Commons Attribution 4.0. www.peppercarrot.com
Peabar|2|True|An robh fios agad?
Peabar|3|True|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean.
Peabar|4|False|Mòran taing dhan 1096 pàtran a thug taic dhan eapasod seo!
Peabar|5|True|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd is chì thu d’ àinm an-seo!
Peabar|6|True|Tha sinn air Patreon, Tipeee, PayPal, Liberapay ...’s a bharrachd!
Peabar|7|False|Tadhail air www.peppercarrot.com airson barrachd fiosrachaidh!
Peabar|8|False|Mòran taing!
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

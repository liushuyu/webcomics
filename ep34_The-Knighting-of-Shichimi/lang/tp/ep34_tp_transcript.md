# Transcript of Pepper&Carrot Episode 34 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute luka luka tu tu: tenpo sewi pi jan Sisimi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|1|False|tenpo pimeja sama...
jan Ipiku|2|False|...ni la, sina kama e jan utala sewi pona tawa kulupu Aa, jan Sisimi o.
jan Kowijana|3|False|sina sona ala sona e kama pi jan Pepa?
jan Sapon|4|False|sona ala.
jan Sapon|5|False|ona o kepeken tenpo lili. ante la, ona li lon ala tenpo toki pi jan Sisimi.
jan Sisimi|6|False|pona a.
jan Sisimi|7|False|mi wile...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|Taa waaaaa|nowhitespace
kalama|2|False|Taaa W a a aa|nowhitespace
jan Pepa|3|True|mi kama!
jan Pepa|4|True|o lukin!
jan Pepa|5|False|O LUKIN!!!
kalama|6|False|PA K A LA!|nowhitespace
kalama|7|False|ike a!
kalama|8|False|jan ale li pona ala pona? sina pakala ala anu seme?
jan Sisimi|9|False|jan Pepa o!
jan Pepa|10|True|toki, jan Sisimi o!
jan Pepa|11|True|kama mi li kalama, li lon tenpo ike tan ike mi!
jan Pepa|12|False|mi tawa lon ale pi tenpo suno ni. taso mi wile toki e ni lon tenpo ante.
<hidden>|13|False|Dear translator: if editing the soundFX is difficult, I made a special documentation here: https://www.peppercarrot.com/en/static14/documentation&page=055_Sound-Effect_translation

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Ipiku|1|False|ona li kama tan wile sina anu seme?
jan Sisimi|2|False|kama. ona li jan pona mi Pepa. ale li pona.
jan Pepa|3|True|soweli Kawa o, sina pona ala pona?
jan Pepa|4|False|kama mi li kiwen tan ike mi. mi sona mute ala e ni kepeken tawa suli.
jan Pepa|5|True|mi toki sin e ni: pakala li tan ike mi.
jan Pepa|6|False|sama la, len mi...
jan Sisimi|7|False|aa aa aa
jan Wasapi|8|True|jan Sisimi o,
jan Wasapi|9|False|jan lili ni pi wawa nasa li jan pona sina la, sina sona ala sona wawa e ni?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sisimi|1|True|sona, jan sewi o.
jan Sisimi|2|False|ona li jan Pepa, li tan kulupu Pakalaa.
jan Wasapi|3|True|ona li lon ni la, ona li jaki e kon sewi pi kulupu nasin mi.
jan Wasapi|4|False|o weka e ona tan lukin mi, lon tenpo ni.
jan Sisimi|5|True|taso...
jan Sisimi|6|False|jan sewi Wasapi o...
jan Wasapi|7|True|taso seme ?
jan Wasapi|8|False|ante la, sina wile ala wile kama weka tan kulupu mi?
jan Sisimi|9|False|! ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sisimi|1|True|o kute, jan Pepa o, sina o tawa.
jan Sisimi|2|False|lon tenpo ni.
jan Pepa|3|True|seme?
jan Pepa|4|False|o awen, o awen! ken la, ni li pakala lili taso .
jan Sisimi|5|False|jan Pepa o, ike pali ala e ni.
jan Pepa|6|False|sina o! sina lon supa sewi o. mi ike tawa sina la, o kama tawa mi. sina taso o toki e ni tawa mi!
jan Wasapi|7|False|jaki a...
jan Wasapi|8|True|jan Sisimi o, mi nanpa e tenpo luka luka...
jan Wasapi|9|True|luka tu tu...
jan Wasapi|10|True|luka tu wan...
jan Wasapi|11|False|luka tu...
jan Sisimi|12|False|O PINI, JAN PEPA O! O WEKA!!!
kalama|13|False|SSSSUnno!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|jan Sisimi o, awen lon te...
kalama|2|False|K ALaMa!|nowhitespace
jan Sisimi|3|True|O WEKA!!!
jan Sisimi|4|True|O WEKA!!!
jan Sisimi|5|False|O WEKA!!!
kalama|6|False|S S S U P AA ! !!|nowhitespace
jan Pepa|7|True|aaa!
jan Pepa|8|False|o kute! ni li... p-pona... aaaa... ALA!
jan Kowijana|9|False|JAN SISIMI EN JAN PEPA O! O PINI!
jan Sapon|10|False|o awen.
jan Wasapi|11|False|Hmm!
jan Pepa|12|True|ike a!
jan Pepa|13|False|sina la, mi pali e ni!
kalama|14|False|L L O JE!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|PINITUS WAWANASA SULIMUS!!!
kalama|2|False|W A W A!|nowhitespace
jan Pepa|3|False|ike a!
kalama|4|False|P OKA !!|nowhitespace
jan Sisimi|5|True|wawa suli sina kin pi pini pali li ken ala pini e mi!
jan Sisimi|6|True|o pini, o weka, jan Pepa o!
jan Sisimi|7|False|o pini kama e pakala sina tan mi!
jan Pepa|8|False|a, wawa mi pi pini pali li ken pini e pali. taso mi wile ala pini e pali sina.
jan Sisimi|9|True|seme?
jan Sisimi|10|False|sina toki e seme?!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sisimi|1|False|?!!
jan Pepa|2|True|mi wile pini e pali ona!
jan Pepa|3|False|mi pini e wawa pi pona lukin la, tenpo ona li weka ala.
jan Pepa|4|True|mi kama lon ni la, mi kama sona e ni lon tenpo sama.
jan Pepa|5|False|sina kama e utala pi jan Sisimi tawa mi la, mi pana e wawa lili tawa ike sina!
jan Wasapi|6|True|IKE JAKI A!
jan Wasapi|7|True|sina ken ala
jan Wasapi|8|False|kulupu mi ale li lukin a!
jan Pepa|9|True|pona a li kama tawa sina!
jan Pepa|10|False|wawa Lonaa mi li lili ala lon tenpo ni la, mi awen e pali mi.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|SSEELLO!!|nowhitespace
jan Wasapi|2|True|sona a. sina kama wawa sama jan sina pi tenpo pini kepeken tenpo lili taso...
jan Wasapi|3|False|ni li ante e tenpo pi nasin pali mi. taso ni li sin pona.
jan Pepa|4|True|nasin pali sina?
jan Pepa|5|True|ni la, sina wile sona taso e pali mi anu seme? ni li tan ala kama ike mi anu seme ?
jan Pepa|6|False|nasin sina li nasa ike!
jan Wasapi|7|True|aa...
jan Wasapi|8|False|aa aa.
jan Wasapi|9|True|SINA ALE LI LUKIN E SEME?!
jan Wasapi|10|False|JAN LI UTALA E MI LA, SINA PALI E ALA ANU SEME?! O TAWA ONA!!!
jan Wasapi|11|False|MI WILE E ONA PI MOLI ALA!
jan Wasapi|12|False|O TAWA ONA!!!
jan Pepa|13|False|jan Sisimi o, mi tu o toki e ni lon tenpo ante!
jan Pepa|14|True|soweli Kawa o, ike mi la, mi wile kepeken tawa suli kin.
jan Pepa|15|False|o awen lon mi!
kalama|16|False|Luka!
kalama|17|False|Luka!
kalama|18|False|WaawW aa!!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|? !
jan Wasapi|2|False|O TAWA ONA!!!
jan Pepa|3|False|pakala.
jan Sapon|4|False|jan Pepa o, jo e ilo mi!
kalama|5|False|Tawa!
jan Pepa|6|True|wawa a!
jan Pepa|7|False|sina pona, jan Sapon o!
kalama|8|False|Jo!
kalama|9|False|Tawwa a a a a!|nowhitespace
sitelen toki|10|False|TOKI NI LI AWEN...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|tenpo March 31, 2021 musi sitelen & toki: jan David Revoy. jan pi lukin pona: jan Arlo James Barnes en jan Carotte en jan Craig Maloney en jan Efrat b en jan GunChleoc en jan Karl Ove Hufthammer en jan Martin Disch en jan Nicolas Artance en jan Parnikkapore, Valvin. toki pona ante toki li tan jan Ret Samys. jan Juli li pona e ona. pona mute o tawa jan Nartance. ona li toki mute e jan Wasapi lon lipu ona sama lipu mi. toki ona li ante mute e sama pi jan Wasapi tawa lipu ni. musi ni li tan pali pi ma Elewa mama: jan David Revoy. jan awen nanpa wan: jan Craig Maloney. jan pi toki sitelen: jan Craig Maloney en jan Nartance en jan Scribblemaniac en jan Valvin. jan pi pona pali: jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson . ilo: ilo Krita 4.4.1 en ilo Inkscape 1.0.2 li kepeken ilo Kubuntu Linux 20.04. nasin pi lawa jo: Creative Commons Attribution 4.0. www.peppercarrot.com
jan Pepa|2|True|sina sona ala sona?
jan Pepa|3|True|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute.
jan Pepa|4|False|jan 1096 li pana e mani la, lipu ni li lon!
jan Pepa|5|True|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa. nimi sina li ken lon ni!
jan Pepa|6|True|ilo Patreon en ilo Tipeee en ilo PayPal en ilo Liberapay en ilo ante la, mi lon!
jan Pepa|7|False|sina wile sona la, o lukin e lipu www.peppercarrot.com !
jan Pepa|8|False|sina pona!
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

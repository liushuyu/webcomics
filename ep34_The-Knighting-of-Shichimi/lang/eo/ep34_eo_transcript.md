# Transcript of Pepper&Carrot Episode 34 [eo]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Ĉapitro 34a : La Kavalirigo de Ŝiĉimio

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rakontanto|1|False|La saman nokton…
Hibisko|2|False|… kaj tiel, ni ĉi-nokte bonvenigas vin, Ŝiĉimion, kiel nian plej junan kavaliron de Aho.
Koriandro|3|False|Ĉu Pipro ankoraŭ mankas ?
Safrano|4|False|Jes.
Safrano|5|False|Ŝi rapidu, aŭ ŝi forestos la paroladon de Ŝiĉimio.
Ŝiĉimio|6|False|Dankon.
Ŝiĉimio|7|False|Mi volus…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sono|1|False|ZjuUU UUUUU|nowhitespace
Sono|2|False|Zjuu U U U U U U|nowhitespace
Pipro|3|True|Alvenas !
Pipro|4|True|Atenton !
Pipro|5|False|ATENTON !!!
Sono|6|False|FR AK AS !|nowhitespace
Sono|7|False|Mis !
Pipro|8|False|Ĉu ĉiuj sanas ? Ĉu nenio rompiĝis ?
Ŝiĉimio|9|False|Pipro !
Pipro|10|True|Saluton Ŝiĉimio !
Pipro|11|True|Pardonu la draman kaj malfruan alvenon !
Pipro|12|False|Mi kuras je la tuta tago, sed tio estas longa historio.
<hidden>|13|False|Dear translator: if editing the soundFX is difficult, I made a special documentation here: https://www.peppercarrot.com/en/static14/documentation&page=055_Sound-Effect_translation

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibisko|1|False|Ĉu ŝi estas unu el viaj gastoj ?
Ŝiĉimio|2|False|Jes. Ŝi estas mia amiko Pipro. Ĉio estas en ordo.
Pipro|3|True|Karoĉjo, ĉu vi sanas ?
Pipro|4|False|Pardonu la surteriĝon, mi ankoraŭ ne majstris tion, uzante superrapidon.
Pipro|5|True|Kaj mi pardonpetas al ĉiuj pro la ĝeno ;
Pipro|6|False|kaj teme pri mia vesto…
Ŝiĉimio|7|False|hi hi hi
Vasabio|8|True|Ŝiĉimio,
Vasabio|9|False|ĉu tiu ĉi juna sorĉistino ĵus veninta vere estas via amiko ?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ŝiĉimio|1|True|Jes, via moŝto.
Ŝiĉimio|2|False|Ŝia nomo estas Pipro, de la skolo de Ĥaosaho.
Vasabio|3|True|Ŝia ĉeesto malpurigas la sanktan naturon de nia lernejo.
Vasabio|4|False|Forigu ŝin tuj de mia vido.
Ŝiĉimio|5|True|Sed…
Ŝiĉimio|6|False|Majstrino Vasabio…
Vasabio|7|True|Sed kio ?
Vasabio|8|False|Ĉu vi preferus forpeliĝi de nia lernejo ?
Ŝiĉimio|9|False|! ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ŝiĉimio|1|True|Pardonu, Pipro, sed vi devas foriri.
Ŝiĉimio|2|False|Nun.
Pipro|3|True|Be ?
Pipro|4|False|He, he, atendu minuton ! Certe okazas ia miskompreno.
Ŝiĉimio|5|False|Bonvole Pipro, ne malfaciligu ĉi tion.
Pipro|6|False|He ! Vi, sur la trono. Se vi havas problemon kun mi, venu kaj diru al mi vi mem !
Vasabio|7|False|Tsss...
Vasabio|8|True|Ŝiĉimio, vi havas dek sekundojn...
Vasabio|9|True|naŭ...
Vasabio|10|True|ok...
Vasabio|11|False|sep...
Ŝiĉimio|12|False|SUFIĈIS, PIPRO ! FORIRU !!!
Sono|13|False|ŜR R I ii i !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|Ŝiĉimio, bonvolu trankvil…
Sono|2|False|F u u M M M !
Ŝiĉimio|3|True|FORIRU !!!
Ŝiĉimio|4|True|FORIRU !!!
Ŝiĉimio|5|False|FORIRU !!!
Sono|6|False|K R U U U U U ! !!
Pipro|7|True|Oj !
Pipro|8|False|He ! Tio... n-NE... Oooj... Ne afablas !
Koriandro|9|False|ŜIĈIMIO ! PIPRO ! BONVOLE ĈESU !
Safrano|10|False|Atendu.
Vasabio|11|False|Hmm !
Pipro|12|True|Grrr !
Pipro|13|False|Bone, vi petis ĝin !
Sono|14|False|B R Z U M !!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|CURSUS CANCELLARE MAXIMUS !!!
Sono|2|False|Ŝ K L A K !
Pipro|3|False|Oj !
Sono|4|False|P AF !!
Ŝiĉimio|5|True|Eĉ via plej bona nuliga sorĉo ne efikos sur min !
Ŝiĉimio|6|True|Cedu, Pipro, kaj foriru !
Ŝiĉimio|7|False|Ĉesu min devigi vundi vin !
Pipro|8|False|Ho, mia nuliga sorĉo funkciis tute bone, sed ĝi ne celis vin.
Ŝiĉimio|9|True|Be ?
Ŝiĉimio|10|False|Pri kio vi parolas ?!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ŝiĉimio|1|False|?!!
Pipro|2|True|Ŝin ĝi celis !
Pipro|3|False|Mi simple nuligis ŝian Sorĉon de beleco, kiu ŝajnigas ŝin juna.
Pipro|4|True|Mi rimarkis tiun sorĉon tuj kiam mi venis.
Pipro|5|False|Mi donis al vi do etan parton de tio, kion vi meritas, ĉar vi bataligis Ŝiĉimion kontraŭ mi !
Vasabio|6|True|SENRESPEKTE !
Vasabio|7|True|Kiel vi aŭdacas ,
Vasabio|8|False|kaj eĉ antaŭ tuta mia lernejo !
Pipro|9|True|Pensu vin bonŝanca !
Pipro|10|False|Se mi havus ĉiom de mia Reo, mi ne haltus je tio.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sono|1|False|SO R Ĉ !!
Vasabio|2|True|Komprenite. Vi atingis la nivelon de viaj antaŭuloj pli frue ol mi atendis...
Vasabio|3|False|Tio akcelas miajn planojn, sed fakte tio bonas.
Pipro|4|True|Viajn planojn ?
Pipro|5|True|Ĉu vi do nur testis min, kaj ĉio ĉi neniel rilatis al mia malfrueco ?
Pipro|6|False|Via pensado tute malsanas !
Vasabio|7|True|hii...
Vasabio|8|False|hii.
Vasabio|9|True|KION VI ĈIUJ RIGARDAS ?!
Vasabio|10|False|ŜI ĴUS MIN ATAKIS, KAJ VI NUR STARAS TIE ?! KAPTU ŜIN !!!
Vasabio|11|False|MI VOLAS ŜIN VIVAN !
Vasabio|12|False|KAPTU ŜIN !!!
Pipro|13|False|Ŝiĉimio, ni devos paroli pri tio ĉi poste !
Pipro|14|True|Pardonu, Karoĉjo, sed ni devos ree flugi per superrapido.
Pipro|15|False|Tenu bone !
Sono|16|False|Ten !
Sono|17|False|Ten !
Sono|18|False|Zjjuuuu !!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|? !
Vasabio|2|False|KAPTU ŜIN !!!
Pipro|3|False|Ha oj.
Safrano|4|False|Pipro, prenu mian !
Sono|5|False|Ĵet !
Pipro|6|True|Ho !
Pipro|7|False|Dankegon, Safrano !
Sono|8|False|Ten !
Sono|9|False|zjuu U U U U !
Rakontanto|10|False|DAŬRIGOTE...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Atribuintaro|1|False|31a de Marto, 2021 Arto kaj scenaro : David Revoy. Beta-legado : Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Esperanta versio Traduko : Tirifto. Korekto : tuxayo. Speciala danko : al Nartance ĉar li esploris la personaĵon de Vasabio per siaj fervorulaj noveloj. La maniero, kiel li ŝin imagis, multe influis la manieron, kiel mi ŝin bildigis ĉi-epizode. Bazita sur la universo Hereva Estigo : David Revoy. Ĉefa daŭriganto : Craig Maloney. Redakto : Craig Maloney, Nartance, Scribblemaniac, Valvin. Korekto : Willem Sonke, Moini, Hali, CGand, Alex Gryson . Programaro : Krita 4.4.1, Inkscape 1.0.2 sur Kubuntu Linux 20.04. Permesilo : Krea Komunaĵo Atribuite 4.0. www.peppercarrot.com
Pipro|2|True|Ĉu vi sciis ?
Pipro|3|True|Pepper&Carrot estas tute libera, senpaga, malfermitkoda, kaj subtenata de siaj mecenataj legantoj.
Pipro|4|False|Ĉi tiu ĉapitro ricevis subtenon de 1096 mecenatoj !
Pipro|5|True|Vi ankaŭ povas iĝi mecenato de Pepper&Carrot kaj havi vian nomon skribita tie !
Pipro|6|True|Ni estas en Patreon, Tipeee, PayPal, Liberapay ... kaj aliaj !
Pipro|7|False|Venu al www.peppercarrot.com por pli da informoj !
Pipro|8|False|Dankon !
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

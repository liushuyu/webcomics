# Transcript of Pepper&Carrot Episode 34 [id]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Judul|1|False|Episode 34: Pemberian Gelar Ksatria Shichimi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narator|1|False|Pada malam yang sama...
Hibiscus|2|False|...maka, malam ini, kami menerima engkau, Shichimi, sebagai Ksatria Ah termuda.
Coriander|3|False|Pepper masih belum kelihatan?
Saffron|4|False|Belum.
Saffron|5|False|Semoga dia cepat datang, kalau tidak dia akan melewatkan pidato Shichimi.
Shichimi|6|False|Terima kasih.
Shichimi|7|False|Sekarang saya mau...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|1|False|Zio oOOOOO|nowhitespace
Suara|2|False|Zioo O O O OO|nowhitespace
Pepper|3|True|Hadir!
Pepper|4|True|Awas!
Pepper|5|False|AWAS!!!
Suara|6|False|BRUKK!|nowhitespace
Suara|7|False|Ups!
Suara|8|False|Semua baik-baik saja? Tidak ada yang rusak?
Shichimi|9|False|Pepper!
Pepper|10|True|Halo Shichimi!
Pepper|11|True|Maaf untuk kedatangan yang dramatis dan keterlambatanku!
Pepper|12|False|Banyak kejadian hari ini, panjang ceritanya.
<hidden>|13|False|Dear translator: if editing the soundFX is difficult, I made a special documentation here: https://www.peppercarrot.com/en/static14/documentation&page=055_Sound-Effect_translation

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|Dia salah satu tamumu?
Shichimi|2|False|Ya. Dia temanku Pepper. Semua baik-baik saja.
Pepper|3|True|Carrot, kamu tidak apa-apa?
Pepper|4|False|Maaf tentang pendaratannya, aku biasa dengan pendaratan dalam kecepatan super.
Pepper|5|True|Dan sekali lagi maaf semuanya untuk ketidaknyamanannya,
Pepper|6|False|dan tentang pakaianku...
Shichimi|7|False|hihihi
Wasabi|8|True|Shichimi,
Wasabi|9|False|penyihir muda yang baru sampai ini, apa dia benar temanmu?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Ya, yang mulia.
Shichimi|2|False|Namanya Pepper, dari Ajaran Chaosah.
Wasabi|3|True|Kehadirannya di sini menodai kesucian ajaran kita.
Wasabi|4|False|Enyahkan dia dari pandanganku, secepatnya.
Shichimi|5|True|Tapi...
Shichimi|6|False|Master Wasabi...
Wasabi|7|True|Tapi apa ?
Wasabi|8|False|Apa kamu lebih memilih ditendang dari ajaran kita?
Shichimi|9|False|! ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Maaf Pepper, tapi kamu harus pergi.
Shichimi|2|False|Sekarang.
Pepper|3|True|Hah?
Pepper|4|False|Hei hei hei, tunggu sebentar! Ini pasti ada kesalahpahaman.
Shichimi|5|False|Tolong Pepper, jangan buat ini jadi sulit.
Pepper|6|False|Hei! Kau yang di takhta. Kalau kamu ada masalah denganku, turunlah dan bilanglah sendiri!
Wasabi|7|False|Tsss...
Wasabi|8|True|Shichimi, kamu punya sepuluh detik...
Wasabi|9|True|sembilan...
Wasabi|10|True|delapan...
Wasabi|11|False|tujuh...
Shichimi|12|False|CUKUP, PEPPER! PERGI!!!
Suara|13|False|SHRRIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Shichimi, kumohon tenangl...
Suara|2|False|B ADuuM!|nowhitespace
Shichimi|3|True|PERGIII!!!
Shichimi|4|True|PERGII!!
Shichimi|5|False|PERGI!!!
Suara|6|False|KRI I I I I ! !!|nowhitespace
Pepper|7|True|Ow!!
Pepper|8|False|Hei! Ini... t-TIDAK... Owww... Baik!
Coriander|9|False|SHICHIMI! PEPPER! BERHENTILAH!
Saffron|10|False|Tunggu.
Wasabi|11|False|Hmm!
Pepper|12|True|Grrr!
Pepper|13|False|Oke, ini kamu yang minta ya!
Suara|14|False|B R ZUU !!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CURSUS CANCELLARE MAXIMUS!!!
Suara|2|False|S H K L AK!|nowhitespace
Pepper|3|False|Aduh!
Suara|4|False|P AF !!|nowhitespace
Shichimi|5|True|Bahkan mantra terbaikmu tidak ada efeknya buatku!
Shichimi|6|True|Menyerahlah, Pepper, dan pergi!
Shichimi|7|False|Berhenti membuat aku menyakitimu!
Pepper|8|False|Oh, mantraku bekerja dengan baik, hanya bukan kamu targetnya.
Shichimi|9|True|Hah?
Shichimi|10|False|Apa maksudmu?!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|?!!
Pepper|2|True|Dialah targetnya!
Pepper|3|False|Aku membatalkan Mantra Kecantikan yang menjaga dia terlihat muda.
Pepper|4|True|Aku menyadarinya segera setelah aku sampai di sini.
Pepper|5|False|Jadi, aku memberikan sebagian kecil dari yang sepantasnya kau terima karena telah membuat Shichimi menyerangku!
Wasabi|6|True|TIDAK SOPAN!
Wasabi|7|True|Berani-beraninya kamu,
Wasabi|8|False|dan di depan seluruh murid-muridku!
Pepper|9|True|Itu belum apa-apa!
Pepper|10|False|Kalau aku masih punya semua Rea milikku, aku tidak akan berhenti di situ.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|1|False|DR Z OW!!|nowhitespace
Wasabi|2|True|Hmm, kamu telah mencapai tingkat pendahulumu jauh lebih cepat dari yang kubayangkan...
Wasabi|3|False|Ini mempercepat rencanaku, tapi ini kabar baik.
Pepper|4|True|Rencanamu?
Pepper|5|True|Jadi kamu hanya mengujiku, dan ini semua tidak ada hubungannya dengan keterlambatanku?
Pepper|6|False|Kamu benar-benar gila!
Wasabi|7|True|he...
Wasabi|8|False|he.
Wasabi|9|True|APA YANG KALIAN TONTONI?!
Wasabi|10|False|AKU BARU SAJA DISERANG, DAN KALIAN HANYA DIAM SAJA DI SITU?! TANGKAP DIA!!!
Wasabi|11|False|AKU MAU DIA HIDUP-HIDUP!
Wasabi|12|False|TANGKAP DIA!!!
Pepper|13|False|Maafkan aku, Carrot, tapi kita harus pakai kecepatan super lagi.
Pepper|14|True|Pegangan!
Pepper|15|False|Pok!
Suara|16|False|Pok!
Suara|17|False|Ziioo OO!!
Suara|18|False|Shichimi, kita perlu bicarakan ini nanti!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|? !
Wasabi|2|False|TANGKAP DIA!!!
Pepper|3|False|Ya ampun.
Saffron|4|False|Pepper, pakai sapuku!
Suara|5|False|Fizzz!
Pepper|6|True|Oh wow!
Pepper|7|False|Terima kasih, Saffron!
Suara|8|False|Tok!
Suara|9|False|zioo O O O O!|nowhitespace
Narator|10|False|BERSAMBUNG...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kredit|1|False|29 Maret 2021 Pelukis & Skenario: David Revoy. Penguji Bacaan Cerita: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Versi Bahasa Indonesia Penerjemah: Aldrian Obaja Muis. Terima kasih kepada: Nartance atas eksplorasinya tentang karakter Wasabi dalam novel fan-fictionnya. Caranya mengimajinasikan dia sangat mempengaruhi caraku menampilkannya dalam episode ini. Berdasarkan alam semesta Hereva Pembuat: David Revoy. Penyunting utama: Craig Maloney. Penulis: Craig Maloney, Nartance, Scribblemaniac, Valvin. Pemeriksa: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Peranti Lunak: Krita 4.4.1, Inkscape 1.0.2 on Kubuntu Linux 20.04. Izin (Lisensi): Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|True|Anda juga bisa menjadi sukarelawan Pepper&Carrot dan dapatkan namamu di sini!
Pepper|3|True|Kami ada di Patreon, Tipeee, PayPal, Liberapay ...dan banyak lagi!
Pepper|4|False|Cek www.peppercarrot.com untuk informasi selanjutnya!
Pepper|5|True|Terima kasih!
Pepper|6|True|Tahukah kamu?
Pepper|7|False|Pepper&Carrot sepenuhnya gratis (bebas), sumber terbuka dan disponsori oleh para pembacanya.
Pepper|8|False|Untuk episode ini, terima kasih kepada 1096 relawan!
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

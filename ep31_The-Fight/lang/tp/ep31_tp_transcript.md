# Transcript of Pepper&Carrot Episode 31 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute luka luka wan: utala

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|1|False|nena Pimejama pi sewi Pakalaa.
kalama|2|False|SSeellii !|nowhitespace
jan Pepa|3|False|ala a!
kalama|4|False|SSeel liii|nowhitespace
kalama|5|False|LLojjee!
jan Pepa|6|False|mi pana e ni!
kalama|8|False|Tawa!
jan Kajen|9|False|jan sin a!
jan Pepa|10|False|! !|nowhitespace
kalama|11|False|WAWA !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|Seeeeeliii!
kalama|2|False|PAK ALA! !!|nowhitespace
kalama|3|False|PAA K A LA ! !!|nowhitespace
jan Pepa|4|False|sina ken ala!
jan Pepa|5|False|AWENSELOLA NASINKAMAANPA!
kalama|6|False|Woooosh! !|nowhitespace
kalama|7|False|Wekkkka! !|nowhitespace
kalama|8|False|Wekkkkka! !|nowhitespace
kalama|9|False|Anpa!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|soweli Kawa o!
jan Pepa|2|False|nasin ante nanpa luka tu a!
jan Pepa|4|False|SITELENA SULIPONA!
kalama|5|False|Ww awa !|nowhitespace
kalama|3|False|Ta w aa|nowhitespace
jan Kajen|6|False|?!!
jan Kajen|10|False|aaa!!
kalama|7|True|L
kalama|8|True|A|nowhitespace
kalama|9|False|S|nowhitespace
jan Pepa|11|False|SULIPONA LILIPANA!
jan Kajen|12|False|! !|nowhitespace
jan Kajen|13|False|ike...
sitelen|14|False|2019-12-20-E31P03_V15-pini.jpg
sitelen|15|False|sitelen li kama ala
kalama|16|False|PAKALA ! !!|nowhitespace
kalama|17|False|O

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kajen|1|False|PAKALASELI SULIPI KALAMANI!
kalama|2|False|Wwwwawa!
kalama|3|True|K
kalama|4|True|A|nowhitespace
kalama|5|True|L|nowhitespace
kalama|6|True|A|nowhitespace
kalama|7|True|M|nowhitespace
kalama|8|True|A|nowhitespace
kalama|9|True|!|nowhitespace
kalama|10|False|!|nowhitespace
kalama|11|True|KOOO
kalama|12|False|KOOO
kalama|13|False|K o nn n ...|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kajen|1|False|lili a...
jan Kajen|2|True|sina weka tawa poki ma sama jan lili anu seme?
jan Kajen|3|False|ni li anpa tan wawa lili ...
jan Pepa|4|True|ala!
jan Pepa|5|False|ona li nasin kin!
kalama|6|False|Loje! !|nowhitespace
jan Pepa|7|False|lupa la, sina pona, soweli Kawa o!
jan Pepa|8|False|o anpa, jan sona Kajen o!
jan Pepa|9|False|MOKUS...
jan Pepa|10|False|...PIMEJUS!
kalama|12|False|Liiiiiinnnnnnjjjjjjjaa
kalama|11|False|L L U UPA ! !!|nowhitespace
jan Tume|13|False|O PINI!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Tume|1|True|o kute e mi. O PINI!
jan Tume|2|False|ni ale li pini!
kalama|3|False|Pini !|nowhitespace
kalama|4|False|KO OOO !|nowhitespace
kalama|5|False|WEKA!
jan Tume|6|True|sina tu o!
jan Tume|7|True|O KAMA!
jan Tume|8|False|lon tenpo ni!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|Noka!
kalama|2|False|Noka!
jan Tume|3|True|tenpo sike tu wan la, sina wile e "utala sona nanpa pini taso"...
jan Tume|4|False|...mi kepeken tenpo pimeja ni ale ala!
jan Tume|5|False|o toki...
jan Pepa|6|False|...
jan Kajen|7|True|ken a...
jan Kajen|8|True|...ona li ken jo e lipu ona...
jan Kajen|9|False|...taso mi pana e nanpa "lili" taso tawa ona..
sitelen|10|True|lipu
sitelen|11|True|pi kulupu
sitelen|12|False|Pakalaa
sitelen|14|False|jKumin
sitelen|13|False|jKajen
sitelen|15|False|j T u me|nowhitespace
sitelen|16|False|~ jan Pepa li ~
sitelen|17|False|jan sona pi wawa nasa
sitelen toki|18|False|- PINI -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|5|True|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa. nimi sina li ken lon ni!
jan Pepa|3|True|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute.
jan Pepa|4|False|jan 971 li pana e mani la, lipu ni li lon!
jan Pepa|7|True|sina wile sona la, o lukin e lipu www.peppercarrot.com !
jan Pepa|6|True|ilo Patreon en ilo Tipeee en ilo PayPal en ilo Liberapay en ilo ante la, mi lon!
jan Pepa|8|False|sina pona!
jan Pepa|2|True|sina sona ala sona?
mama|1|False|tenpo December 20, 2019 musi sitelen & toki: jan David Revoy. jan pi lukin pona: jan Craig Maloney en jan Martin Disch en jan Arlo James Barnes en jan Nicolas Artance en jan Valvin. toki pona ante toki: jan Ret Samys . musi ni li tan pali pi ma Elewa mama: jan David Revoy. jan awen nanpa wan: jan Craig Maloney. Writers: Craig Maloney en jan Nartance en jan Scribblemaniac en jan Valvin. Correctors: Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson . Software: ilo Krita 4.2.6appimage en ilo Inkscape 0.92.3 li kepeken ilo Kubuntu 18.04-LTS. License: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.

# Transcript of Pepper&Carrot Episode 16 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa luka luka luka wan: jan sona pi nena kiwen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sapon|9|False|ike, jan Pepa o. mi wile ala!
sitelen|2|True|jan Sapon
sitelen|3|False|wawa pi
sitelen|1|False|★★★
sitelen|5|False|SOWELI MOKU
sitelen|6|False|15
sitelen|4|False|13
sitelen|7|False|nasin Mun
sitelen|8|False|LINJA LAWA PONA
kalama|10|True|Telo
kalama|11|False|Telo

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sapon|2|False|wile mi la, mi pini ala e utala toki. jan pona mi la, mi pini ala kin e ona. ante la, ni li kama ike lon tenpo ale.
jan Sapon|1|True|sina taso o pali e ni
jan Pepa|3|False|j-jan Sapon o, mi wile e pona sina...
jan Pepa|4|True|jan sona mi li pana ala e sona. ona li pali e ni taso: ona li lon tomo mi, li toki ike e kiwen sijelo ona. pali ale mi la, ona li toki kalama tawa mi...
jan Sapon|6|True|o kute! sina jan pi wawa nasa , o pali sama ni!
jan Sapon|8|False|sama jan pi lili ala!
jan Sapon|7|True|o toki wile tawa ona! o toki wawa tawa ona!
jan Pepa|5|False|sina wile ala e ni anu seme: o kama tawa tomo mi, o toki tawa ona ... ?
jan Sapon|9|False|a, ken, ken! sina la, mi ken kama lon poka sina, li toki tawa ona.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Tume|1|False|ni li seme? sina toki e ni tan seme: sina kama sona e ala?!
jan Tume|12|False|suli la, sina toki e ni kepeken jan ante. sina wawa ala pilin anu seme?!
jan Tume|14|False|monsi mi li pakala ala la, mi pana e sona mi, e sona pi wawa nasa ala a. sina lukin e sona ni!
jan Kajen|15|True|jan Tume o, utala ala
jan Kajen|16|False|toki ona li pona...
waso|2|True|mu
waso|3|False|mu
waso|4|True|mu
waso|5|False|mu
waso|6|True|mu
waso|7|False|mu
waso|8|True|mu
waso|9|False|mu
waso|10|True|mu
waso|11|False|mu
kalama|13|False|ANPA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kajen|1|True|tenpo la, mi tu li jan lili ala. mi ken pana e sona lon tenpo ni la, ni li pona.
jan Kajen|2|True|sina wile e ni la, o tawa
jan Tume|5|True|a a!
jan Tume|6|True|jan sona anu seme?
jan Tume|7|False|sona ona la, jan tu ni li lili ala lili taso?
jan Pepa|8|True|lili taso tawa seme!?
jan Pepa|9|False|mi tu li tawa!
jan Sapon|10|False|seme? "mi tu"?
jan Tume|11|True|pona a. mi lon ona.
jan Tume|12|True|ni li pona tawa mi: sina o toki tawa jan sona.
jan Tume|13|False|ona li lon sewi pi nena ni
jan Tume|14|True|sina wile ala wile
jan Tume|15|False|kama sona suli?
jan Kajen|3|True|jan sona pi nena kiwen
jan Kajen|4|False|toki sona ona li pona mute...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|wile!
jan Sapon|2|False|wile kin!
jan Tume|3|False|pona! wile sina li suli!
jan Pepa|9|False|O UTALA !!!
jan Sapon|6|False|A... AKESI SULI IKE !
jan Pepa|8|True|IKE LI TAN TOKI PI JAN SONA MI!
jan Pepa|7|True|MI SONA E NI:
jan Pepa|5|False|ike a !
jan Tume|4|False|o tawa!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|ANPA KIWENA PI SIKEMUS MUTE !
jan Sapon|2|False|SELIMUS SULIMUS !
kalama|3|False|Ttawa!
jan Tume|4|True|aaa, sina jan lili la, sina wile utala e ale lon tenpo ale!
jan Pepa|7|False|pana sona ni li seme!
mama|11|False|nasin lawa : Creative Commons Attribution 4.0, ilo: ilo Krita en ilo Blender en ilo G'MIC en ilo Inkscape li kepeken ilo Ubuntu
mama|10|False|musi ni li tan pali pi ma Elewa. pali ni li tan jan David Revoy. jan Craig Maloney li pana e pona tawa ona. jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson li pona e musi ni.
sitelen toki|8|False|- PINI -
mama|9|False|tenpo 04/2016 - www.peppercarrot.com - musi sitelen & toki li tan jan David Revoy - toki pona li tan jan Ret Samys
jan Tume|6|False|ala li kama pona la, telo seli li suli, li pona mute a! ona li pona e luka sina, e noka sina, e pilin sina!
jan Tume|5|True|sona pi jan sona li ni: o sama ona!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 671 li pana e mani la, lipu ni li lon:
mama|2|False|sina ken pana kin e pona tawa jan Pepa&soweli Kawa lon www.patreon.com/davidrevoy

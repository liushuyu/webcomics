# Transcript of Pepper&Carrot Episode 06 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 6: Der Zaubertrankwettbewerb

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Oh verflixt, ich bin schon wieder bei offenem Fenster eingeschlafen...
Pepper|2|True|... es ist so windig ...
Pepper|3|False|... und warum kann ich durchs Fenster Komona sehen?
Pepper|4|False|KOMONA!
Pepper|5|False|Der Zaubertrank-wettbewerb!
Pepper|6|True|Ich muss aus Versehen eingeschlafen sein!
Pepper|9|True|... aber?
Pepper|10|False|Wo bin ich ?!?
Vogel|12|False|cK?|nowhitespace
Vogel|11|True|qua|nowhitespace
Pepper|7|False|*|nowhitespace
Fußnote|8|False|* siehe Episode 4 : Das Genie

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Es ist so süß, dass du daran gedacht hast, mich zum Wettbewerb zu bringen!
Pepper|3|False|Fan-tas-tisch !
Pepper|4|True|Du hast sogar an einen Zaubertrank, meine Kleidung und meinen Hut gedacht...
Pepper|5|False|... mal sehen, welcher Zaubertrank das ist ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|WAS ?!!
Bürgermeister von Komona|3|False|Als Bürgermeister von Komona erkläre ich den Wettbewerb für... eröffnet!
Bürgermeister von Komona|4|False|Unsere Stadt ist erfreut, für diesen ersten Wettbewerb nicht weniger als vier Hexen begrüßen zu dürfen.
Bürgermeister von Komona|5|True|Bitte jetzt einen
Bürgermeister von Komona|6|True|herzlichen
Schrift|2|False|Komona Zaubertrankwettbewerb
Bürgermeister von Komona|7|False|Applaus für :

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|29|False|Klatsch
Bürgermeister von Komona|1|True|Von sehr weit her aus der Technologen-Union angereist, begrüße ich sehr erfreut - die hinreißende und geniale
Bürgermeister von Komona|3|True|... nicht zu vergessen, unsere einheimische Hexe aus Komona,
Bürgermeister von Komona|5|True|... Unsere dritte Teilnehmerin kommt aus dem Land der untergehenden Monde,
Bürgermeister von Komona|7|True|... und unsere letzte Teilnehmerin, aus dem Wald Eichhorns Ruh’,
Bürgermeister von Komona|2|False|Coriander !
Bürgermeister von Komona|4|False|Saffron !
Bürgermeister von Komona|6|False|Shichimi !
Bürgermeister von Komona|8|False|Pepper !
Bürgermeister von Komona|9|True|Lasst die Spiele beginnen!
Bürgermeister von Komona|10|False|Abgestimmt wird durch den Applaus!
Bürgermeister von Komona|11|False|Als Erstes: die Präsentation von Coriander
Coriander|13|False|...vergessen Sie die Angst vor dem Tod dank meines...
Coriander|14|True|.. Tranks der
Coriander|15|False|ZOMBIFIZIERUNG !
Publikum|16|True|Klatsch
Publikum|17|True|Klatsch
Publikum|18|True|Klatsch
Publikum|19|True|Klatsch
Publikum|20|True|Klatsch
Publikum|21|True|Klatsch
Publikum|22|True|Klatsch
Publikum|23|True|Klatsch
Publikum|24|True|Klatsch
Publikum|25|True|Klatsch
Publikum|26|True|Klatsch
Publikum|27|True|Klatsch
Publikum|28|True|Klatsch
Coriander|12|False|Meine Damen und Herren ...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bürgermeister von Komona|1|False|FANTASTISCH !
Publikum|3|True|Klatsch
Publikum|4|True|Klatsch
Publikum|5|True|Klatsch
Publikum|6|True|Klatsch
Publikum|7|True|Klatsch
Publikum|8|True|Klatsch
Publikum|9|True|Klatsch
Publikum|10|True|Klatsch
Publikum|11|True|Klatsch
Publikum|12|True|Klatsch
Publikum|13|True|Klatsch
Publikum|14|True|Klatsch
Publikum|15|True|Klatsch
Publikum|16|False|Klatsch
Saffron|18|True|denn hier ist
Saffron|17|True|... aber bitte, spart euch den großen Applaus noch auf, liebe Komonaer !
Saffron|22|False|... sie eifer-süchtig machen!
Saffron|19|True|MEIN
Saffron|25|False|ELEGANZ !
Saffron|24|True|... Trank der
Saffron|23|False|... und das alles nur durch die Anwendung eines Tropfens von meinem ...
Publikum|26|True|Klatsch
Publikum|27|True|Klatsch
Publikum|28|True|Klatsch
Publikum|29|True|Klatsch
Publikum|30|True|Klatsch
Publikum|31|True|Klatsch
Publikum|32|True|Klatsch
Publikum|33|True|Klatsch
Publikum|34|True|Klatsch
Publikum|35|True|Klatsch
Publikum|36|True|Klatsch
Publikum|37|False|Klatsch
Bürgermeister von Komona|39|False|dieser Zaubertrank könnte ganz Komona reich machen!
Bürgermeister von Komona|38|True|Fantastisch! Unglaublich !
Publikum|41|True|Klatsch
Publikum|42|True|Klatsch
Publikum|43|True|Klatsch
Publikum|44|True|Klatsch
Publikum|45|True|Klatsch
Publikum|46|True|Klatsch
Publikum|47|True|Klatsch
Publikum|48|True|Klatsch
Publikum|49|True|Klatsch
Publikum|50|True|Klatsch
Publikum|51|True|Klatsch
Publikum|52|True|Klatsch
Publikum|53|True|Klatsch
Publikum|54|True|Klatsch
Publikum|55|True|Klatsch
Publikum|56|False|Klatsch
Bürgermeister von Komona|2|False|Coriander schlägt mit ihrem wun-der-ba-ren Zaubertrank dem Tod ein Schnippchen!
Saffron|21|True|Der Zaubertrank, auf den ihr alle gewartet habt: er wird eure Nachbarn in Erstaunen versetzen ...
Bürgermeister von Komona|40|False|Euer Applaus könnte nicht deutlicher sein. Coriander ist schon jetzt aus dem Rennen!
Saffron|20|False|Trank

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bürgermeister von Komona|1|False|Shichimi wird Probleme haben, diese Darbietung zu schlagen!
Shichimi|4|True|NEIN!
Shichimi|5|True|Ich kann nicht, es ist zu gefährlich
Shichimi|6|False|TUT MIR LEID !
Bürgermeister von Komona|3|False|... komm schon, Shichimi, alle warten auf dich
Bürgermeister von Komona|7|False|Meine Damen und Herren, es sieht so aus, als ob Shichimi aussteigen...
Saffron|8|False|Gib das her!
Saffron|9|False|... und tu nicht so, als wärst du schüchtern, du verdirbst nur die Show.
Saffron|10|False|Es ist ohnehin schon allen klar, dass ich gewonnen habe, egal was dein Trank bewirkt.
Shichimi|11|False|!!!
Geräusch|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|RIESIGE MONSTER !
Shichimi|2|False|Ich... Ich wusste nicht, dass wir den Trank vorführen müssen
Shichimi|13|True|VORSICHT!!!
Shichimi|14|True|Das ist ein Trank für

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vogel|1|False|KRAH-KRAH-Kr aa a a ahh|nowhitespace
Geräusch|2|False|WUMM!
Pepper|3|True|... hm, cool !
Pepper|5|False|... mein Trank wird zumindest für ein paar Lacher gut sein, weil ....
Pepper|4|False|also bin ich jetzt dran?
Bürgermeister von Komona|6|True|Lauf, du Idiot!
Bürgermeister von Komona|7|False|Der Wettbewerb ist vorbei! Bring dich in Sicherheit!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|... wie immer hauen alle genau dann ab, wenn wir gerade dran sind ...
Pepper|1|True|Da hast du’s ...
Pepper|4|True|Immerhin habe ich eine Idee, was wir mit deinem „Trank“ anfangen können, Carrot -
Pepper|5|False|...alles wieder in Ordnung bringen und dann ab nach Hause!
Pepper|7|True|Du
Pepper|8|False|übergroßer, eingebildeter Zombie-Kanarienvogel!
Pepper|10|False|Möchtest du noch einen letzten Trank probieren?
Pepper|11|False|... nicht wirklich, hm?
Pepper|6|False|HEY !
Geräusch|9|False|K R AC H !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Jaaa, lies dir ganz genau durch, was hier draufsteht ...
Pepper|2|False|... Ich werde nicht zögern, das alles über dir auszukippen, wenn du nicht sofort aus Komona verschwindest!
Bürgermeister von Komona|3|True|Weil sie unsere Stadt aus großer Gefahr gerettet hat,
Bürgermeister von Komona|4|False|vergeben wir den ersten Platz an Pepper für ihren Trank des ... ??!!
Pepper|7|False|... hm ... eigentlich, ist das kein echter Trank ; das ist die Urinprobe meiner Katze vom letzten Tierarztbesuch!
Pepper|6|True|... Haha! yep ...
Pepper|8|False|... lieber keine Vorführung, oder ?...
Erzähler|9|False|Episode 6 : Der Zaubertrankwettbewerb
Erzähler|10|False|ENDE
Schrift|5|False|50,000 Ko
Impressum|11|False|März 2015 - Grafik und Handlung: David Revoy - Übersetzung: Alexandra Jordan / Helmar Suschka

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Ich danke in dieser Episode meinen 245 Förderern :
Impressum|4|False|https://www.patreon.com/davidrevoy
Impressum|3|False|Du kannst die nächste Episode von Pepper&Carrot hier unterstützen:
Impressum|7|False|Tools : Dieser Comic wurde zu 100% mit der freien Open-Source-Software Krita auf Linux Mint erstellt
Impressum|6|False|Open Source : Alle Quelldateien mit Ebenen und Schriften findest Du auf der Website
Impressum|5|False|Lizenz : Creative Commons Namensnennung Du darfst verändern, teilen, verkaufen, abgeleitete Werke erstellen etc...
Impressum|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ

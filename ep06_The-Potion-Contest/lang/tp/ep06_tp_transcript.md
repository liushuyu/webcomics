# Transcript of Pepper&Carrot Episode 06 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa luka wan: utala pi pali telo

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|ike a! mi kama lape la, lupa li open kin...
jan Pepa|2|True|... kon tawa li mute ...
jan Pepa|3|False|... tan seme la mi ken lukin e ma Komona lon lupa!?
jan Pepa|4|False|MA KOMONA!
jan Pepa|5|False|utala pi pali telo a!
jan Pepa|6|False|ken la... ken la, mi wile ala lape, li kama lape!
jan Pepa|7|True|... taso?
jan Pepa|8|False|mi lon seme ?!?
waso|10|False|u ?|nowhitespace
waso|9|True|m|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|!!!
jan Pepa|2|False|soweli Kawa o! sina wile tawa e mi tawa utala pi pali telo la, sina suwi a!
jan Pepa|3|False|po-na !
jan Pepa|4|True|sina pana kin e telo wan, e len mi, e len lawa mi...
jan Pepa|5|False|... mi kama lukin e telo ni...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|SEME ?!!
jan lawa pi ma Komona|3|False|mi jan lawa pi ma Komona la, mi toki e ni: utala pi pali telo li... open! potion contest... Open!
jan lawa pi ma Komona|4|False|ma mi li pilin pona tan ni: jan pi wawa nasa tu tu li kama a tawa utala sin ni!
jan lawa pi ma Komona|5|True|o kalama
jan lawa pi ma Komona|6|True|mute
sitelen|2|False|utala pi pali telo pi ma Komona
jan lawa pi ma Komona|7|False|tan jan ni :

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama jan|29|False|tan kulupu Sona Ilo la, kama pi jan sona pona ni li pona suli:
jan lawa pi ma Komona|1|True|... jan pi wawa nasa pi ma Komona a li
jan lawa pi ma Komona|3|True|... jan nanpa wan tu li kama tan ma pi mun mute anpa.
jan lawa pi ma Komona|5|True|... pini la, jan ni li tan ma Pini Soweli pi kasi mute:
jan lawa pi ma Komona|7|True|jan Kowijana !
jan lawa pi ma Komona|2|False|jan Sapon !
jan lawa pi ma Komona|4|False|jan Sisimi !
jan lawa pi ma Komona|6|False|jan Pepa !
jan lawa pi ma Komona|8|False|o kute! musi li open!
jan lawa pi ma Komona|9|True|telo nanpa wan li tan kalama luka mute
jan lawa pi ma Komona|10|False|nanpa wan la, jan Kowijana li pana e pali
jan lawa pi ma Komona|11|False|... moli li ike ala tan ni mi:
jan Kowijana|13|False|... telo pi
jan Kowijana|14|True|MOLI TAWA !
jan Kowijana|15|False|Luka
kalama jan|16|True|Luka
kalama jan|17|True|Luka
kalama jan|18|True|Luka
kalama jan|19|True|Luka
kalama jan|20|True|Luka
kalama jan|21|True|Luka
kalama jan|22|True|Luka
kalama jan|23|True|Luka
kalama jan|24|True|Luka
kalama jan|25|True|Luka
kalama jan|26|True|jan o ...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa pi ma Komona|1|False|PONA A !
kalama jan|3|True|Luka
kalama jan|4|True|Luka
kalama jan|5|True|Luka
kalama jan|6|True|Luka
kalama jan|7|True|Luka
kalama jan|8|True|Luka
kalama jan|9|True|Luka
kalama jan|10|True|Luka
kalama jan|11|True|Luka
kalama jan|12|True|Luka
kalama jan|13|True|Luka
kalama jan|14|True|Luka
kalama jan|15|True|Luka
kalama jan|16|False|Luka
jan Sapon|18|True|telo
jan Sapon|17|True|... o pini! tan ona la, jan pi ma Komona o kalama ala.
jan Sapon|22|False|... li wile sama sina !
jan Sapon|19|True|MI
jan Sapon|25|False|PONA LUKIN SULI !
jan Sapon|24|True|... telo pi
jan Sapon|23|False|... ona ale li ken kepeken telo ni mi pi lili mute taso:
kalama jan|26|True|Luka
kalama jan|27|True|Luka
kalama jan|28|True|Luka
kalama jan|29|True|Luka
kalama jan|30|True|Luka
kalama jan|31|True|Luka
kalama jan|32|True|Luka
kalama jan|33|True|Luka
kalama jan|34|True|Luka
kalama jan|35|True|Luka
kalama jan|36|True|telo ni la, mani mute li tawa ma Komona!
kalama jan|37|True|pona a! suli a!
kalama jan|38|True|Luka
kalama jan|39|True|Luka
kalama jan|40|False|Luka
jan lawa pi ma Komona|42|False|Luka
jan lawa pi ma Komona|41|True|Luka
kalama jan|44|True|Luka
kalama jan|45|True|Luka
kalama jan|46|True|Luka
kalama jan|47|True|Luka
kalama jan|48|True|Luka
kalama jan|49|True|Luka
kalama jan|50|True|Luka
kalama jan|51|True|Luka
kalama jan|52|True|Luka
kalama jan|53|True|jan Kowijana li anpa e moli a kepeken telo ni pi wawa nasa!
kalama jan|54|True|sina wile lukin e telo ni. jan poka sina li wile sona e ona ...
kalama jan|55|True|kalama sina li pana e sona ni: telo pi jan Kowijana li ken ala telo nanpa wan
kalama jan|56|False|li lon!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa pi ma Komona|1|False|mi sona ala e ni: jan Sisimi li ken anpa e ona ...
jan Sisimi|4|True|ALA!
jan Sisimi|5|True|mi ken ala. ona li wawa pi ike mute
jan Sisimi|6|False|MI PAKALA !
jan lawa pi ma Komona|3|False|... jan Sisimi o pali! jan ale li wile lukin
jan lawa pi ma Komona|7|False|jan ale o, ken la, jan Sisimi li wile ala utala...
jan Sapon|8|False|o pana !
jan Sapon|9|False|... o pini toki lili e pali sina. sina ken ike e tenpo musi
jan Sapon|10|False|jan ale li sona e ni: telo mi li nanpa wan. telo sina li ken ala anpa e telo mi ...
jan Sisimi|11|False|!!!
kalama|12|False|S S S S SU LI|nowhitespace
jan Sisimi|15|False|SULI IKE !
jan Sisimi|2|False|mi... mi sona ala e ni: mi o pana e pali tan telo mi
jan Sisimi|13|True|O LUKIN!!!
jan Sisimi|14|True|ni li telo pi

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
waso|1|False|MU-MU-M uuuu uuuu u u uu|nowhitespace
kalama|2|False|ANPA!
jan Pepa|3|True|... aa, pona !
jan Pepa|5|False|... telo mi la, sina kalama uta musi. ni li tan...
jan Pepa|4|False|mi ken ala ken pana e telo mi?
jan lawa pi ma Komona|6|True|o tawa wawa!
jan lawa pi ma Komona|7|False|utala li pini ! ... o awen e sina!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|2|False|... tenpo pi pali mi la, jan ale li weka, sama tenpo ale
jan Pepa|1|True|o lukin e ni ...
jan Pepa|4|True|taso tenpo ni la, mi sona e ken pi telo sina, soweli Kawa o
jan Pepa|5|False|...mi ken pona e ma ni la, mi ken tawa tomo mi!
jan Pepa|7|True|waso pi moli tawa pi pona lukin pi suli ike o!
jan Pepa|8|False|sina wile ala wile e telo pini ni? ...
jan Pepa|10|False|... sina wile ala a!
jan Pepa|11|False|O KUTE!
jan Pepa|6|False|P A KA L A !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|a, o lukin e lipu sitelen. o lukin wawa ...
jan Pepa|2|False|... sina weka ala tan ma Komona lon tenpo ni la, mi pana e telo ni lon sijelo sina !
jan lawa pi ma Komona|3|True|ma mi li kama pakala la, jan Pepa li awen e ma ni la,
jan lawa pi ma Komona|4|False|mi pana e nanpa wan tawa jan ni tan... telo seme ... ??!!
jan Pepa|7|False|... a... o sona e ni: ni li telo pi wawa nasa ala. soweli Kawa li tawa jan pi sona soweli la, ni li telo jaki jelo tan soweli!
jan Pepa|6|True|... a a a! lon ...
jan Pepa|8|False|... mi pana ala e telo anu seme?...
sitelen toki|9|False|lipu nanpa luka wan: utala pi pali telo
sitelen toki|10|False|PINI
sitelen|5|False|50,000 Ko
mama|11|False|tenpo March 2015 - musi sitelen en toki li tan jan David Revoy - ante toki li tan jan Ret Samys

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 245 li pana e mani la, lipu ni li lon:
mama|4|False|https://www.patreon.com/davidrevoy
mama|3|False|sina ken pana kin e pona tawa jan Pepa&soweli Kawa tawa lipu kama:
mama|7|False|ilo : lipu ni li kepeken taso ilo pi nasin jo pi jan ale. ilo pi nasin Free/Libre li ilo Krita lon ilo Linux Mint
mama|6|False|nasin Open-source : sitelen mama suli ale li lon lipu mama, li jo e musi sitelen mute e sitelen linja.
mama|5|False|nasin lawa : Creative Commons Attribution sina ken ante, li ken pana, li ken esun, li ken pali ante...
mama|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ

# Transcript of Pepper&Carrot Episode 06 [eo]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Ĉapitro 6a : La sorĉotrinkaĵa Konkurso
<hidden>|2|False|Pipro
<hidden>|3|False|kaj
<hidden>|4|False|Karoĉjo

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|Ho fuŝ, mi enlitiĝis, refoje ne ferminte la fenestron...
Pipro|2|True|... nu, kia vento ja !
Pipro|3|False|... kaj kial mi vidas Komonon tra la fenestro ?
Pipro|4|False|KOMONO !
Pipro|5|False|La Trinkaĵ-konkurso !
Pipro|6|True|Mi... Mi verŝajne endormiĝis nevole !
Pipro|9|True|Neŝerce,
Pipro|10|False|kie mi estas ??!?
Birdo|12|False|K ?|nowhitespace
Birdo|11|True|kva|nowhitespace
Pipro|7|False|*
Note|8|False|* Vidu ĉapitron 4an : La genia Ekfulmo

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|!!!
Pipro|2|False|Karoĉjo ! Kiel bonkore, ke vi pensis transporti min al la konkurso !
Pipro|3|False|Grandioz… ega !
Pipro|4|True|Vi eĉ pensis elpreni iun sorĉotrinkaĵon, mian veston kaj mian ĉapelon ...
Pipro|5|False|... Nu, kiun trinkaĵon vi ja kunprenis ? ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|KIUN ?!!
Urbestro de Komono|3|False|Kiel la urbestro de Komono, mi deklaras la trinkaĵkonkurson malfermita !
Urbestro de Komono|4|False|Nia urbo estas tre feliĉa bonvenigi ne malpli multe ol kvar sorĉistinojn al ĝia unua okazo.
Urbestro de Komono|5|True|Bonvolu aplaŭdi ilin
Urbestro de Komono|6|False|laŭteg e ...
Writing|2|False|Komona Trinkaĵ-konkurso

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|30|False|Klak
Urbestro de Komono|1|True|La venintinon de la granda lando de Unio de Teĥnikistoj estas honoro bonvenigi ; jen la ĉarma kaj eltrovema
Urbestro de Komono|3|True|Ni ne forgesu nian hejman knabinon ; jen la Komona sorĉistino mem,
Urbestro de Komono|5|True|... Nia tria parto-prenantino, alvenis al ni de la lando de la subirantaj lunoj :
Urbestro de Komono|7|True|... Fine, nia lasta partoprenantino elvenis el la fundo de la Sciurarbaro ; jen
Urbestro de Komono|2|False|Koriandro !
Urbestro de Komono|4|False|Safrano !
Urbestro de Komono|6|False|Ŝiĉimio !
Urbestro de Komono|8|False|Pipro !
Urbestro de Komono|9|True|La Konkurso komenciĝu !
Urbestro de Komono|10|False|La voĉdonado fariĝos per la aplaŭdado !
Urbestro de Komono|11|False|Unue, atentu la konkursaĵon de Koriandro.
Koriandro|13|False|Sinjorinoj kaj Sinjoroj...
Koriandro|14|True|... ne plu nepre timu la morton danke al...
Koriandro|15|True|... mia trinkaĵo
Koriandro|16|False|SORĈITKADAVRIGA !
Audience|17|True|Klak
Audience|18|True|Klak
Audience|19|True|Klak
Audience|20|True|Klak
Audience|21|True|Klak
Audience|22|True|Klak
Audience|23|True|Klak
Audience|24|True|Klak
Audience|25|True|Klak
Audience|26|True|Klak
Audience|27|True|Klak
Audience|28|True|Klak
Audience|29|True|Klak

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urbestro de Komono|1|False|MIRINDEGA !
Urbestro de Komono|2|False|La morton mem defias Koriandro per tiu trinkaĵo MIRAKLEGA !
Audience|4|True|Klak
Audience|5|True|Klak
Audience|6|True|Klak
Audience|7|True|Klak
Audience|8|True|Klak
Audience|9|True|Klak
Audience|10|True|Klak
Audience|11|True|Klak
Audience|12|True|Klak
Audience|13|True|Klak
Audience|14|True|Klak
Audience|15|True|Klak
Audience|16|True|Klak
Audience|17|False|Klak
Safrano|19|True|Ĉar jen
Safrano|18|False|Nu, konservu viajn aplaŭdojn, Komona popolo !
Safrano|22|True|La vera trinkaĵo, kiun vi ĉiuj atendas : tia, kia povas imponi viajn ĉiujn najbarojn kaj...
Safrano|23|False|... ilin enviigi !
Safrano|20|True|MIA
Safrano|26|False|LUKSIGA !
Safrano|25|True|... trinkaĵo
Safrano|24|True|... ĉio ĉi de nun ebliĝas per nurnura suronta guto de mia...
Audience|28|True|Klak
Audience|29|True|Klak
Audience|30|True|Klak
Audience|31|True|Klak
Audience|32|True|Klak
Audience|33|True|Klak
Audience|34|True|Klak
Audience|35|True|Klak
Audience|36|True|Klak
Audience|37|True|Klak
Audience|38|True|Klak
Audience|39|True|Klak
Audience|40|True|Klak
Audience|42|False|Klak
Audience|41|True|Klak
Urbestro de Komono|44|False|Tia trinkaĵo povus la Komonajn urbanojn riĉigi !
Urbestro de Komono|43|True|Mirindege ! Nekredeble !
Audience|46|True|Klak
Audience|47|True|Klak
Audience|48|True|Klak
Audience|49|True|Klak
Audience|50|True|Klak
Audience|51|True|Klak
Audience|52|True|Klak
Audience|53|True|Klak
Audience|54|True|Klak
Audience|55|True|Klak
Audience|56|True|Klak
Audience|57|True|Klak
Audience|58|True|Klak
Audience|59|True|Klak
Audience|60|False|Klak
Audience|3|True|Klak
Audience|27|True|Klak
Urbestro de Komono|45|False|Via aplaŭdado ne trompiĝis : Koriandro estas jam eliminita.
Safrano|21|False|trinkaĵo !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urbestro de Komono|1|False|Nun la konkurado estos malfacila por Ŝiĉimio.
Ŝiŝimio|4|True|NE !
Ŝiŝimio|5|True|Mi ne povas, ĝi tro danĝeras.
Ŝiŝimio|6|False|PARDONU !
Urbestro de Komono|3|False|Rapidu, Ŝiĉimio, vin ĉiuj atendas !
Urbestro de Komono|7|False|Ŝajnas, Sinjorinoj kaj Sinjoroj, kvazaŭ Ŝiĉimio kapitulacos...
Safrano|8|False|Donu tion !
Safrano|9|False|... kaj ĉesu tian timan afektaĵon, kaj ne fuŝu la spektaklon.
Safrano|10|False|Ĉiuj jam scias, ke mi gajnis la konkurson, kaj sekve, kiel ajn efikos via trinkaĵo...
Ŝiŝimio|11|False|!!!
Sono|12|False|G R A A AA A N D|nowhitespace
Ŝiŝimio|15|False|GIGANTMONSTRIGA !
Ŝiŝimio|2|False|Mi… Mi ne sciis, ke ni devos demonstri niajn konkursaĵojn.
Ŝiŝimio|13|True|ATENTU !!!
Ŝiŝimio|14|True|Tio estas trinkaĵo

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Birdo|1|False|KRi KRi KRiiii ii ii ii ii ii ii iii i ii|nowhitespace
Sono|2|False|BUM !
Pipro|3|True|... be, amuze !
Pipro|5|False|... mia trinkaĵo almenaŭ meritus vian ridadon, ĉar...
Pipro|4|False|Ĉu do estas mia vico nun ?
Urbestro de Komono|6|True|Forkuru !
Urbestro de Komono|7|False|La konkurso estas finita ! ... saviĝu, stultulino !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|2|False|...ĉiam estas : ĉiuj foriras ĝuste kiam venas nia vico.
Pipro|1|True|Aĥ, tiele...
Pipro|4|True|Almenaŭ mi havas ideeton pri tio, kion ni povos fari per via « trinkaĵo », Karoĉjo...
Pipro|5|False|... ni bonordigu la urbon kaj rehejmiĝu !
Pipro|7|True|Vi, la
Pipro|8|False|Gigant-sorĉit-kadavro-luks-birdino !
Pipro|10|False|Ĉu plaĉus al vi elprovado de la lasta trinkaĵo ? ...
Pipro|11|False|... Ne vere, ĉu ne ?
Pipro|6|False|HE !
Sono|9|False|K R AK K !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|True|Jes, atente legu la etikedon...
Pipro|2|False|... Mi senhezite verŝos tiom de la fluaĵo sur vin, se vi ne tuj forflugos de Komono.
Urbestro de Komono|3|True|Ĉar ŝi savis nian urbon de la detruado,
Urbestro de Komono|4|False|ni donis la premion al Pipro pro ŝia Trinkaĵo de.. . ? ! !
Pipro|7|False|... be... fakte, trinkaĵo ne vera estas tio ; tio estas iom da urino de mia kato okaze de lia lastempa kontrolo ĉe kuracisto !
Pipro|6|True|... Ha ! Ha ! jes...
Pipro|8|False|Neniun demonstron, ĉu ne ?...
Rakontanto|9|False|Ĉapitro 6a : La sorĉotrinkaĵa Konkurso
Rakontanto|10|False|~ FINO ~
Writing|5|False|50 000 Koj
Atribuintaro|11|False|Marto 2015 – Desegno kaj Scenaro : David Revoy. Traduko : libre fan. Korekto : kaoseto, Tirifto

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Atribuintaro|1|False|Pepper&Carrot estas tute libera, senpaga kaj subtenita de mecenataj legantoj. Por ĉi tiu rakontaĵo dankon al la 245 mecenatoj :
Atribuintaro|4|False|https://www.patreon.com/davidrevoy
Atribuintaro|3|False|Vi ankaŭ fariĝu mecenato, subtenante la sekvontan rakontaĵon de Pepper&Carrot:
Atribuintaro|7|False|Iloj : Tiu rakontaĵo estis verkita cent elcente per liberaj programaroj Krita sur Linux Mint
Atribuintaro|6|False|La permesilo disponebligas al vi ĉiujn verkajn fontojn: la tiparoj kaj la dosieroj enhavantaj la tavolojn estas elŝuteblaj de la oficiala TTT-ejo.
Atribuintaro|5|False|Libera Permesilo : Krea Komunaĵo Atribute (CC-By) al David Revoy Tiun verkon vi rajtas ŝanĝi, redisdoni, vendi, ktp.
Atribuintaro|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ

# Transcript of Pepper&Carrot Episode 06 [id]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Judul|1|False|Episode 6: Perlombaan ramuan

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Kemarin aku tertidur dengan jendela terbuka, lagi...
Pepper|2|True|... dingin sekali...
Pepper|3|False|... dan aku bisa melihat Komona dari jendela?
Pepper|4|False|KOMONA!
Pepper|5|True|Perlombaan ramuan!
Pepper|6|True|Aduh, aku ketiduran karena mempersiapkan ramuan!
Pepper|9|True|... tapi?
Pepper|10|False|Di mana aku?!?
Burung|12|False|kwek?|nowhitespace
Pepper|7|False|*
Catatan|8|False|* Lihat Episode 4 : Ramuan kecerdasan

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Terima kasih karena sudah membawaku ke tempat perlombaan!
Pepper|3|False|Luar biasa!
Pepper|4|True|Kamu juga membawakan pakaian dan ramuanku...
Pepper|5|False|... hmm, ini ramuan apa ya...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|APA INI?!!
Walikota Komora|3|False|Sebagai walikota Komona, saya akan memulai perlombaan ramuan ini!
Walikota Komora|4|False|Saya juga mengucapkan terima kasih kepada empat penyihir yang ikut serta dalam perlombaan ini!
Walikota Komora|5|True|Berikan kepada mereka tepuk tangan yang...
Walikota Komora|6|False|meriah
Penulis|2|False|Perlombaan ramuan Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Penonton|29|False|Prok
Walikota Komora|1|True|Peserta pertama yang berasal dari Jakarta, dengan hormat kita sambut...
Walikota Komora|3|True|... peserta kedua berasal dari Komona, kita sambut...
Walikota Komora|5|True|... peserta ketiga berasal dari ibukota Parahyangan, Bandung...
Walikota Komora|7|True|... dan peserta terakhir, dari hutan yang sangat dalam di Jonggol, kita sambut...
Walikota Komora|2|False|Coriander !
Walikota Komora|4|False|Saffron !
Walikota Komora|6|False|Shichimi !
Walikota Komora|8|False|Pepper !
Walikota Komora|9|True|Perlombaan dimulai!
Walikota Komora|10|False|Pemenang akan ditentukan dari banyaknya tepuk tangan
Walikota Komora|11|False|Dimulai dari Coriander, dipersilakan...
Coriander|13|False|... dengan ramuan ini, burung ini bisa...
Coriander|14|True|... menjadi
Coriander|15|False|ZOMBIE!
Penonton|16|True|Prok
Penonton|17|True|Prok
Penonton|18|True|Prok
Penonton|19|True|Prok
Penonton|20|True|Prok
Penonton|21|True|Prok
Penonton|22|True|Prok
Penonton|23|True|Prok
Penonton|24|True|Prok
Penonton|25|True|Prok
Penonton|26|True|Prok
Penonton|27|True|Prok
Penonton|28|True|Prok
Coriander|12|False|Halo! semuanya...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Walikota Komora|1|False|LUAR BIASA !
Penonton|3|True|Prok
Penonton|4|True|Prok
Penonton|5|True|Prok
Penonton|6|True|Prok
Penonton|7|True|Prok
Penonton|8|True|Prok
Penonton|9|True|Prok
Penonton|10|True|Prok
Penonton|11|True|Prok
Penonton|12|True|Prok
Penonton|13|True|Prok
Penonton|14|True|Prok
Penonton|15|True|Prok
Penonton|16|False|Prok
Saffron|18|False|di sini sudah ada ramuan terbaik
Saffron|17|True|... sebaiknya simpan tepuk tangan Anda, rakyat Komona !
Saffron|20|False|... tidak ada apa-apanya!
Saffron|23|False|BERGAYA!
Saffron|22|True|... lebih
Saffron|21|True|... hanya dengan setetes, dapat menjadikan burung ini...
Penonton|26|True|Prok
Penonton|25|True|Prok
Penonton|24|True|Prok
Penonton|28|True|Prok
Penonton|29|True|Prok
Penonton|30|True|Prok
Penonton|31|True|Prok
Penonton|32|True|Prok
Penonton|33|True|Prok
Penonton|34|True|Prok
Penonton|35|True|Prok
Penonton|36|True|Prok
Penonton|37|True|Prok
Penonton|38|True|Prok
Penonton|39|False|Prok
Walikota Komora|41|False|Ini dapat membuat Komona jadi kaya!
Walikota Komora|40|True|Luar biasa! Menakjubkan!
Penonton|43|True|Prok
Penonton|44|True|Prok
Penonton|45|True|Prok
Penonton|46|True|Prok
Penonton|47|True|Prok
Penonton|48|True|Prok
Penonton|49|True|Prok
Penonton|50|True|Prok
Penonton|51|True|Prok
Penonton|52|True|Prok
Penonton|53|True|Prok
Penonton|54|True|Prok
Penonton|55|True|Prok
Penonton|56|True|Prok
Penonton|57|True|Prok
Penonton|58|True|Prok
Penonton|59|False|Prok
Walikota Komora|2|False|Coriander dapat membuat burung itu hidup kembali dengan ramuannya!
Saffron|19|True|Ini adalah ramuan yang sesungguhnya: satu-satunya yang dapat membuat peserta lain...
Walikota Komora|42|False|Dengan ini, maka Coriander langsung tereleminasi!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Walikota Komora|1|False|Apakah Shichimi bisa mengalahkan peserta sebelumnya, silakan dimulai...
Shichimi|4|True|TIDAK!
Shichimi|5|True|Tidak bisa, ini terlalu berbahaya
Shichimi|6|False|MAAF!
Walikota Komora|3|False|... ayolah Shichimi, semua orang sedang menunggumu
Walikota Komora|7|False|Kelihatannya Shichimi sedang tidak percaya diri...
Saffron|8|False|Berikan padaku!
Saffron|9|False|... kamu tak usah malu, karena kamu akan menghancurkan acara ini
Saffron|10|False|Semua orang tahu bahwa aku akan memenangkan perlombaan ini...
Shichimi|11|False|!!!
Suara|12|False|WUUUUNG!!|nowhitespace
Shichimi|15|False|MONSTER RAKSASA!
Shichimi|2|False|Aku... Aku tidak yakin akan melakukan hal ini
Shichimi|13|True|HATI-HATI!!!
Shichimi|14|True|Itu adalah ramuan

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Burung|1|False|WAK-WAK-Waaaaw|nowhitespace
Suara|2|False|BUM!
Pepper|3|True|... hm, jadi...
Pepper|5|False|... ramuanku akan membuat orang tertawa, karena....
Pepper|4|False|apa ini saatnya giliranku?
Walikota Komora|6|True|Lari, cepat!
Walikota Komora|7|False|Selamatkan dirimu sekarang juga!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|... seperti biasa, semuanya pergi saat giliran kita
Pepper|1|True|huh...
Pepper|4|True|Tapi aku punya ide apa yang bisa dilakukan dengan "ramuan" ini, Carrot
Pepper|5|False|... ayo bereskan semuanya dan pulang ke rumah!
Pepper|7|True|Kamu,
Pepper|8|False|Burung yang sangat besar!
Pepper|10|False|Ingin coba satu ramuan terakhir? ...
Pepper|11|False|... aku tak yakin
Pepper|6|False|HEI!
Suara|9|False|KRAK!|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ya, baca ini baik-baik...
Pepper|2|False|... aku akan menuangkan ramuan ini jika kamu tidak pergi dari Komona sekarang juga!
Walikota Komora|3|True|Karena Pepper sudah menyelamatkan Komona,
Walikota Komora|4|False|kami memberikan hadiah padanya, dan ramuannya adalah....??!!
Pepper|7|False|sebenarnya, ini bukan ramuan; melainkan air seni dari kucing peliharaanku saat kunjungan ke dokter hewan!
Pepper|6|True|... Haha! Benar...
Pepper|8|False|... tidak perlu dicoba, kan?
Narator|9|False|Episode 6 : Perlombaan ramuan
Narator|10|False|SELESAI
Penulis|5|False|50.000 Ko
Kredit|11|False|Maret 2015 - Ilustrasi dan gambar oleh David Revoy - Diterjemahkan oleh: Bonaventura Aditya Perdana

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kredit|1|False|Pepper&Carrot benar-benar gratis, terima kasih kepada pembaca yang telah memberikan donasinya:
Kredit|4|False|https://www.patreon.com/davidrevoy
Kredit|3|False|Anda bisa menjadi donator Pepper&Carrot untuk episode selanjutnya:
Kredit|7|False|Peralatan: dibuat dengan perangkat lunak gratis dan bebas Krita di Linux Mint
Kredit|6|False|Sumber terbuka: sumber dari gambar ini tersedia di https://peppercarrot.com
Kredit|5|False|Lisensi: Creative Commons Attribution Anda dapat mengubah, menjual, menerjemahkannya, dsb.
Kredit|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ

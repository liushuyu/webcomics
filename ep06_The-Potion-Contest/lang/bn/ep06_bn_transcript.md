# Transcript of Pepper&Carrot Episode 06 [bn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|গল্প ৬: যাদু প্রতিযোগিতা

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|যাঃ, আবার জানলা খুলে ঘুমিয়ে পড়েছিলাম...
Pepper|2|True|... কিন্তু
Pepper|3|False|... তাহলে জানলা দিয়ে কোমোনা দেখা যাচ্ছে কেন?
Pepper|4|False|কোমোনা!
Pepper|5|False|যাদু প্রতিযোগিতা!
Pepper|6|True|আমি নিশ্চই... নিশ্চই ভূল করে ঘুমিয়ে পড়েছিলাম!
Pepper|9|True|... কিন্তু?
Pepper|10|False|আমি কোথায় ?!?
Bird|12|False|?|nowhitespace
Bird|11|True|প্যাঁক|nowhitespace
Pepper|7|False|*
Note|8|False|* See Episode 4 : Stroke of Genius

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|গাজর! তুমি এত মিষ্টী যে তুমি আমাকে প্রতিযোগিতায় আনার কথাও ভেবেছ!
Pepper|3|False|অ-সাধা-রণ !
Pepper|4|True|তুমি আবার আমার যাদুমিশ্রণ, আমার পোশাক, আমার টুপি আনার কথাও ভেবেছ...
Pepper|5|False|... দেখি কোন যাদুমিশ্রণটা তুমি এনেছ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|একী ?!!
Mayor of Komona|3|False|আমি কোমোনার নগরপাল বলছি যাদু প্রতিযোগিতা... শুরু হোক!
Mayor of Komona|4|False|আমাদের শহর সানন্দে চারজন যাদুকরীকে অভ্যর্থণা করছে এই প্রথম পর্বের জন্য
Mayor of Komona|5|True|দয়া করে
Mayor of Komona|6|True|খুব জোরে
Writing|2|False|কোমোনা যাদু প্রতিযোগিতা
Mayor of Komona|7|False|হাততালি দেবেন

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Clap
Mayor of Komona|1|True|অনেক দূরে যন্ত্রবিদদের গোষ্ঠী থেকে সম্মানের সাথে অভিনন্দণ
Mayor of Komona|3|True|... অবশ্যই ভোলার নয় আমাদের নিজেদের মেয়ে, কোমোনার নিজের যাদুকরী
Mayor of Komona|5|True|... আমাদের তৃতীয় প্রতিযোগী এসেছে চন্দ্রাস্তের দেশ থেকে,
Mayor of Komona|7|True|... আর আমাদের শেষ প্রতিযোগী, "কাঠবিড়ালীর লেজ" জঙ্গল থেকে,
Mayor of Komona|2|False|মিস ডালনা !
Mayor of Komona|4|False|মিস হলুদ !
Mayor of Komona|6|False|মিস লিচু !
Mayor of Komona|8|False|মিস চিনি !
Mayor of Komona|9|True|খেলা শুরু হোক!
Mayor of Komona|10|False|হাততালি মেপে নম্বর দেওয়া হবে
Mayor of Komona|11|False|প্রথমে, ডালনার পালা
Coriander|13|False|... মৃত্তুকে ভয় পাবেননা কারণ হল আমার ...
Coriander|14|True|... যাদুমিশ্রণ
Coriander|15|False|নতুনজীবনলাভ !
Audience|16|True|Clap
Audience|17|True|Clap
Audience|18|True|Clap
Audience|19|True|Clap
Audience|20|True|Clap
Audience|21|True|Clap
Audience|22|True|Clap
Audience|23|True|Clap
Audience|24|True|Clap
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Coriander|12|False|মহিলারা ও ভদ্রলোকেরা...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|অসাধারণ !
Audience|3|True|Clap
Audience|4|True|Clap
Audience|5|True|Clap
Audience|6|True|Clap
Audience|7|True|Clap
Audience|8|True|Clap
Audience|9|True|Clap
Audience|10|True|Clap
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|True|Clap
Audience|16|False|Clap
Saffron|18|True|কারণ এই হল
Saffron|17|True|... হাঃ, কোমোনাবাসী হাততালি থামান !
Saffron|22|False|... ওদের হিংসে হবে!
Saffron|19|True|আমার
Saffron|25|False|ধনসম্পদ !
Saffron|24|True|... লাগিয়ে
Saffron|23|False|... এসব হবে মাত্র একবার একফোটা যাদুমিশ্রণ ...
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Audience|29|True|Clap
Audience|30|True|Clap
Audience|31|True|Clap
Audience|32|True|Clap
Audience|33|True|Clap
Audience|34|True|Clap
Audience|35|True|Clap
Audience|36|True|Clap
Audience|37|True|Clap
Audience|38|True|Clap
Audience|39|True|Clap
Audience|40|False|Clap
Mayor of Komona|42|False|এই যাদুমিশ্রণ কোমোনার সবাইকে বড়লোক বানিয়ে দিতে পারে!
Mayor of Komona|41|True|অসাধারণ! অবিশ্বাশ্য !
Audience|44|True|Clap
Audience|45|True|Clap
Audience|46|True|Clap
Audience|47|True|Clap
Audience|48|True|Clap
Audience|49|True|Clap
Audience|50|True|Clap
Audience|51|True|Clap
Audience|52|True|Clap
Audience|53|True|Clap
Audience|54|True|Clap
Audience|55|True|Clap
Audience|56|True|Clap
Audience|57|True|Clap
Audience|58|True|Clap
Audience|59|True|Clap
Audience|60|False|Clap
Mayor of Komona|2|False|ডালনা মৃত্তুকে জয় করে ফেলেছে এই যাদুমিশ্রণ দিয়ে!
Saffron|21|True|আসল যাদুমিশ্রণ যার জন্য আপনারা অপেক্ষা করছেন যেটা আপনাদের প্রতিবেশীদের তাক লাগিয়ে দেবে
Mayor of Komona|43|False|আপনাদের হাততালি ভূল হতে পারে না৷ ডালনা এখনই বাদ চলে গেছে
Saffron|20|False|যাদুমিশ্রণ

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|এই শেষ পালাটা লিচুর পক্ষে টপকানো কঠিন হবে মনে হচ্ছে !
Shichimi|4|True|না!
Shichimi|5|True|আমি পারবনা, খুব বিপজ্জনক
Shichimi|6|False|মাফ করবেন !
Mayor of Komona|3|False|... ও লিচু, সবাই তোমার জন্য অপেক্ষা করছে
Mayor of Komona|7|False|মনে হচ্ছে, মহিলারা ও ভদ্রলোকেরা লিচু প্রতিযোগিতা ত্যাগ করেছে...
Saffron|8|False|ওটা আমাকে দাও !
Saffron|9|False|... আর লজ্জা কোরো না, তুমি মজাটা মাটি করছ
Saffron|10|False|সবাই জানে আমি জিতে গেছি তা তোমার যাদুমিশ্রণ যাই করুকনা কেন ...
Shichimi|11|False|!!!
Sound|12|False|বুবুবুবু|nowhitespace
Shichimi|15|False|বিশালদানব !
Shichimi|2|False|আমি... আমি জানতাম না আমাদের যাদুমিশ্রণ প্রয়োগ করে দেখাতে হবে
Shichimi|13|True|সাবধান!!!
Shichimi|14|True|এটা যাদুমিশ্রণ

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|কা-কা-কা আ আ আ আ|nowhitespace
Sound|2|False|ধুম!
Pepper|3|True|... হা, বাঃ !
Pepper|5|False|... আমার যাদুমিশ্রণ একটু হাসি ফোটাবে কারণ ....
Pepper|4|False|তো এখন আমার পালা ?
Mayor of Komona|6|True|পালাও, বোকা!
Mayor of Komona|7|False|প্রতিযোগিতা শেষ ! ... নিজেকে বাঁচাও!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|... যেরকম হয়, আমাদের পালার সময় সবাই পালালো
Pepper|1|True|যাঃছলে ...
Pepper|4|True|তবে তোমার "যাদুমিশ্রণ" দিয়ে কি করা যায় তা নিয়ে আমার একটা বুদ্ধি এসছে, গাজর
Pepper|5|False|...এখানে সবকিছু ঠিকঠাক করে বাড়ি রওনা হব!
Pepper|7|True|যে
Pepper|8|False|বিশাল-মোটা-ধনী-জ্যান্তমড়া-হলুদপাখী!
Pepper|10|False|শেষ যাদুমিশ্রণটা দেখবে? ...
Pepper|11|False|... দেখতে চাও না ?
Pepper|6|False|এই !
Sound|9|False|মড়মড় !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|হ্যাঁ, কাগজটা ভাল করে পড় ...
Pepper|2|False|... যদি এক্ষণি কোমোনা থেকে না যাও তবে এটা তোমার ওপর ঢেলে দিতে দুবার ভাববনা !
Mayor of Komona|3|True|আমাদের শহর যখন বিপদে তখন সে এটিকে রক্ষা করেছে
Mayor of Komona|4|False|এজন্য আমরা চিনিকে প্রথম পুরষ্কার দিচ্ছি ওর যাদুমিশ্রণ ... ??!!
Pepper|7|False|... হুম... তবে এটা যাদুমিশ্রণ নয়; এটা আমার বিড়ালের হিসু; ওর ডাক্তারের কথাতে বোতলে ভরতে হয়েছিল!
Pepper|6|True|... হাহা! ঠিক ...
Pepper|8|False|... কোনো প্রয়োগ নয় তাহলে ?...
Narrator|9|False|গল্প ৬: যাদু প্রতিযোগিতা
Narrator|10|False|শেষ
Writing|5|False|৫০০০০কো
Credits|11|False|March 2015 - Artwork and story by David Revoy - Translation by Hulo Biral the Tomcat

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free, open-source and sponsored thanks to the kind patronage of readers. For this episode, thank you to the 245 Patrons :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|You too can become a patron of Pepper&Carrot for the next episode :
Credits|7|False|Tools : This episode was 100% drawn with Free/Libre software Krita on Linux Mint
Credits|6|False|Open-source : all source files with layers and fonts, are available on the official site
Credits|5|False|License : Creative Commons Attribution You can modify, reshare, sell etc. ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ

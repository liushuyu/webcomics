# Transcript of Pepper&Carrot Episode 03 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa tu wan: wan mute pi sona nasa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|1|False|ma Komona; tenpo esun
jan Pepa|2|False|jan o, tenpo pona. mi wile e kili jelo luka tu wan.
jan esun|3|False|mi pana. o pana e mani Komo* mute mute mute.
jan Pepa|5|False|...ike
sitelen|4|False|* mani Komo = mani pi ma Komona

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|a, mi pakala. mi wile e kili tu tu taso...
jan esun|2|False|pakala...
jan Sapon|3|False|jan pona suli o! o pana e ijo ali, e ijo mute tu tu. sina sona la, o pana e ijo pona taso.
jan esun|4|False|jan Sapon o. tenpo ale la esun sina li pona
jan Sapon|5|False|jan Pepa anu seme o
jan Sapon|6|False|a, ni li lon ala lon: esun li suli lon ma pi tomo ala?
jan Pepa|7|False|...
jan Sapon|8|False|ken la, sina kama jo e wan tawa utala pi pali telo lon tenpo suno kama
jan Pepa|9|True|seme? utala pi pali telo a...
jan Pepa|10|False|tenpo suno kama a!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|4|False|ni li pona a: ale pi tenpo suno la, mi ken pali tawa utala!
jan Pepa|5|False|mi utala tawa nanpa wan!
sitelen|1|False|utala pi pali telo pi ma Komona:
sitelen|2|False|mani Komo 50 000 TAWA TELO NANPA WAN
sitelen|3|False|lon tenpo Pakalasuno, lon tenpo Pinimun nanpa 3, lon ma suli pi ma Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|2|False|a ... ! mi sona ...
jan Pepa|3|False|... ni li pona. mi wile e ni!
jan Pepa|4|True|soweli Kawa o!
jan Pepa|5|False|o tawa. mi tu li alasa e wan
jan Pepa|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|pali nanpa wan la, mi wile e sike telo lili tan kon pimeja ni ...
jan Pepa|2|False|... e kili sike loje tan ma kasi pi kon ike

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|... e selo pi sike waso tan ma pi waso seli lon poka pi nena seli...
jan Pepa|2|False|... e ni pini: waso AkesiSoweli pi tenpo lili li pana e telo moku

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|soweli Kawa o, ni li pini. mi pilin e ni: mi jo e ijo ale
jan Pepa|2|True|lukin ni li ...
jan Pepa|3|False|... pona
jan Pepa|4|True|Mmm ...
jan Pepa|5|True|telo ... pi weka lape ... nanpa wan !
jan Pepa|6|False|tan ni la, tenpo ale pi suno weka la, mi ken pali tawa telo nanpa wan, tawa utala pi tenpo suno kama
sitelen toki|7|False|lipu kama la, toki ni li awen ...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon,
mama|2|False|jan 93 li pana e mani la, lipu wan ni li lon
mama|1|False|sitelen ni li nasin jo pi jan ale a ( nasin lawa Creative Commons Attribution 3.0 Unported; sina ken jo e sitelen pi suli mute tan lipu mama )
mama|3|False|https://www.patreon.com/davidrevoy
mama|6|False|pona suli tan jan ni: jan David Tschumperlé (G'MIC) en jan ale pi kulupu Krita ! ante toki pi toki pona tan jan Ret Samys
mama|7|False|pali pi lipu wan ni li kepeken taso ilo pi nasin jo pi jan ale. ilo pi nasin Free(libre) en ilo pi nasin Open-source li ilo Krita, li ilo G'MIC lon ilo Xubuntu (GNU/Linux)
jan Pepa|5|False|sina pona!

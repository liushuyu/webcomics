# Transcript of Pepper&Carrot Episode 32 [pl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tytuł|1|False|Odcinek 32: Pole bitwy

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Król|1|True|Oficerze!
Król|2|False|Czy wcieliłeś do wojsk wiedź-mę, jak nakaza-łem?
Oficer|3|True|Tak, mój panie!
Oficer|4|False|Stoi tuż obok ciebie.
Król|5|False|Hmm?
Pepper|6|True|Cześć!
Pepper|7|False|Mam na imię Pepp…
Król|8|False|?!
Król|9|True|GŁUPCZE!
Król|10|True|Od kiedy rekru-tujemy dzieci?!
Król|11|False|Potrzebujemy praw-dziwej wiedźmy!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Hola, hola!
Pepper|2|True|Jestem najpraw-dziwszą wiedźmą Chaosu.
Pepper|3|False|Posiadam nawet dyplom świadczą…
Napis|4|False|Dyplom
Napis|5|False|Chaosu
Napis|8|False|Cayenne
Napis|7|False|Cumin
Napis|6|False|T h yme|nowhitespace
Napis|9|False|~ dla Pepper ~
Król|10|False|MILCZEĆ!
Król|11|False|Żadne dziecko nie skala mojej armii.
Król|12|True|Wracaj do domu bawić się zabawkami.
Król|13|False|Plask!
Dźwięk|14|False|HAHA HA HA!
Armia|15|True|HAHA HA HA!
Armia|16|True|HAHA HA HA!
Armia|17|False|HA HA HAHA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|To niewiary-godne!
Pepper|2|False|Wkuwałam przez lata, ale nikt nie bierze mnie na poważnie,
Pepper|3|False|bo nie wyglą-dam na do-świadczoną!
Dźwięk|4|False|PUF ! ! !|nowhitespace
Pepper|5|False|CARROT!
Dźwięk|6|False|PAF ! ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Serio, Carrot?
Pepper|2|True|Mam nadzieję, że było warto.
Pepper|3|False|Wyglądasz tragi…
Pepper|4|False|Wyglądasz…
Pepper|5|True|No jasne!
Pepper|6|False|Wygląd!
Dźwięk|7|False|Trach!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ej, wy!
Pepper|2|False|Słyszałam, że szu-kacie DOŚWIAD-CZONEJ WIEDŹMY!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Król|1|True|Wiesz, jednak przyz-wij tamto dziecko.
Król|2|False|Na tę na pewno nas nie stać.
Napis|3|False|CIĄG DALSZY NASTĄPI…

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Ty również możesz zostać patronem Pepper&Carrot i znaleźć się na tej liście.
Pepper|3|True|Pepper&Carrot jest całkowicie darmowy, open-source'owy oraz wspierany przez naszych czytelników?
Pepper|4|False|Za ten odcinek dziękujemy 1121 patronom!
Pepper|7|True|Wejdź na www.peppercarrot.com, by dowiedzieć się więcej!
Pepper|6|True|Jesteśmy na Patreonie, Tipeee, PayPal, Liberapay i wielu innych!
Pepper|8|False|Dziękujemy!
Pepper|2|True|Wiedziałeś, że
Credits|1|False|31 marca, 2020 Rysunki i scenariusz: David Revoy. Poprawki skryptu: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. Wersja polska Tłumaczenie: Sölve Svartskogen. Korekta i kontrola jakości: Besamir. Oparto na uniwersum Herevy Autor: David Revoy. Pomocnik: Craig Maloney. Pisarze: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korekta: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.2.9-beta, Inkscape 0.92.3 na Kubuntu 19.10. Licencja: Creative Commons Uznanie autorstwa 4.0. www.peppercarrot.com
<hidden>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|10|False|You can also translate this page if you want.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

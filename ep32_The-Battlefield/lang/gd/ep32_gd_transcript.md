# Transcript of Pepper&Carrot Episode 32 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 32: Am blàr

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rìgh|1|True|A thànaiste!
Rìgh|2|False|An do thrus thu bana-bhuidseach mar a dh’iarr mi ort?
Tànaiste|3|True|Thrus, a thighearna!
Tànaiste|4|False|Tha i ’na seasamh ri ’r taobh.
Rìgh|5|False|...?
Peabar|6|True|Sin sibh!
Peabar|7|False|Is mise Peab…
Rìgh|8|False|?!!
Rìgh|9|True|AMADAIN!!!
Rìgh|10|True|Carson an do thrus thu an leanabh ud?!
Rìgh|11|False|Feumaidh mi bana-bhuidseach catha cheart!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Gabhaibh mo leisgeul!
Peabar|2|True|’S e bana-bhuidseach cheart na Dubh-choimeasgachd a th’ annam.
Peabar|3|False|Tha fiù ’s dioplòma agam a dh’innseas…
Rìgh|12|False|IST!
Rìgh|13|True|Chan eil clann-nighnean gu feum dhomh san fheachd seo.
Rìgh|14|False|Thalla dhachaigh ’s cluich leis na doileagan agad.
Fuaim|15|False|Pais!
Feachd|16|False|HÀHÀ HÀ HÀ!
Feachd|17|False|HÀHÀ HÀ HÀ!
Feachd|18|False|HÀHÀ HÀ HÀ!
Feachd|19|False|HÀ HÀ HÀHÀ!
Sgrìobhte|4|True|An Dubh-
Sgrìobhte|5|False|choimeasgachd
Sgrìobhte|9|False|Carabhaidh
Sgrìobhte|8|True|Cìob
Sgrìobhte|7|True|T ì om|nowhitespace
Sgrìobhte|11|False|~ Peabar ~
Sgrìobhte|10|False|bana-bhuidseach oifigeil
Sgrìobhte|6|False|~ CEUM ~

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Cha chreid mi e!
Peabar|2|False|Às dèidh bliadhnaichean mòra ag ionnsachadh, cha ghabh duine beò gu da-rìribh mi…
Peabar|3|False|…air sgàth ’s nach eil riochd an t-seana-chinn orm!
Fuaim|4|False|SÈID ! !|nowhitespace
Peabar|5|False|A CHURRAIN !
Fuaim|6|False|Sgleog ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Ann an da-rìribh, a Churrain?
Peabar|2|True|Tha mi ’n dòchas gum b’ fhiach am biadh e.
Peabar|3|False|Tha coltas sgrathail ort…
Peabar|4|False|…Tha coltas…
Peabar|5|True|An riochd a th’ ort!
Peabar|6|False|Saoil…
Fuaim|7|False|Brag !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|SIBHSE!
Peabar|2|False|Chuala mi gu bheil sibh a’ lorg BANA-BHUIDSEACH CHEART?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rìgh|1|True|Ghabh mi ath-smuain mu dhèidhinn, nach iarr sinn tilleadh air an nighneag eile?
Rìgh|2|False|Cha chreid mi nach bi an tè seo ro dhaor.
Sgrìobhte|3|False|RI LEANTAINN…

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|5|True|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd is chì thu d’ àinm an-seo!
Peabar|3|True|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean.
Peabar|4|False|Mòran taing dhan 1121 pàtran a thug taic dhan eapasod seo!
Peabar|7|True|Tadhail air www.peppercarrot.com airson barrachd fiosrachaidh!
Peabar|6|True|Tha sinn air Patreon, Tipeee, PayPal, Liberapay ...’s a bharrachd!
Peabar|8|False|Mòran taing!
Peabar|2|True|An robh fios agad?
Urram|1|False|31ad dhen Mhàrt 2020 Obair-ealain ⁊ sgeulachd: David Revoy. Leughadairean Beta: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. Tionndadh Gàidhlig Eadar-theangachadh: GunChleoc. Stèidhichte air saoghal Hereva Air a chruthachadh le: David Revoy. Prìomh neach-glèidhidh: Craig Maloney. Sgrìobhadairean: Craig Maloney, Nartance, Scribblemaniac, Valvin. Ceartachadh: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Bathar-bog: Krita 4.2.9-beta, Inkscape 0.92.3 air Kubuntu 19.10. Ceadachas: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|10|False|You can also translate this page if you want.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

# Transcript of Pepper&Carrot Episode 32 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute luka luka tu: ma pi utala suli

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa|1|True|jan pali mi o!
jan lawa|2|False|sina kama ala kama e jan pi wawa nasa sama wile mi?
jan pali pi jan lawa|3|True|kama, jan lawa mi o!
jan pali pi jan lawa|4|False|ona li lon poka sina.
jan lawa|5|False|...?
jan Pepa|6|True|toki!
jan Pepa|7|False|mi jan Pep...
jan lawa|8|False|?!!
jan lawa|9|True|JAN NASA O!!!
jan lawa|10|True|sina kama e jan lili ni tan seme?!
jan lawa|11|False|mi wile e jan suli utala pi wawa nasa!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|ni li seme?
jan Pepa|2|True|mi jan sona pi wawa Pakalaa.
jan Pepa|3|False|o lukin: mi jo e lipu la...
sitelen|4|True|lipu
sitelen|5|True|pi kulupu
sitelen|6|False|Pakalaa
sitelen|8|True|jKajen
sitelen|9|False|jKumin
sitelen|7|True|j T u me|nowhitespace
sitelen|10|False|~ jan Pepa li ~
jan lawa|11|False|O PINI!
jan lawa|12|True|mi wile ala e jan lili lon kulupu utala.
jan lawa|13|False|o tawa tomo sina. o musi e ijo musi sina.
kalama|14|False|Weka!
kulupu utala|15|True|A A A A A!
kulupu utala|16|True|A A A A A A!
kulupu utala|17|True|A A A A A!
kulupu utala|18|False|A A A A A!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|ni li pakala!
jan Pepa|2|False|mi kama sona kepeken tenpo sike, taso jan ala li pilin e suli sona mi tan seme?...
jan Pepa|3|False|...mi wawa sona ala tawa lukin ona!
kalama|4|False|KOO! !|nowhitespace
jan Pepa|5|False|SOWELI KAWA O !|nowhitespace
kalama|6|False|ANPA! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|seme, soweli Kawa o?
jan Pepa|2|True|wile la, moku o pona.
jan Pepa|3|False|sina jaki tawa lukin...
jan Pepa|4|False|...tawa lukin...
jan Pepa|5|True|nasin ante tawa lukin!
jan Pepa|6|False|ni a!
kalama|7|False|Pakala!|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|JAN O!
jan Pepa|2|False|kute la, sina wile e JAN SULI PI WAWA NASA anu seme?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan lawa|1|True|pilin ante la, o kama sin e jan lili.
jan lawa|2|False|ken suli la, jan ni li wile e mani mute.
sitelen|3|False|TOKI NI LI AWEN…

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|5|True|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa. nimi sina li ken lon ni!
jan Pepa|3|True|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute.
jan Pepa|4|False|jan 1121 li pana e mani la, lipu ni li lon!
jan Pepa|7|True|sina wile sona la, o lukin e lipu www.peppercarrot.com !
jan Pepa|6|True|ilo Patreon en ilo Tipeee en ilo PayPal en ilo Liberapay en ilo ante la, mi lon!
jan Pepa|8|False|sina pona!
jan Pepa|2|True|sina sona ala sona?
mama|1|False|tenpo March 31, 2020 musi sitelen & toki: jan David Revoy. jan pi lukin pona: jan Craig Maloney en jan Martin Disch en jan Arlo James Barnes en jan Nicolas Artance en jan Parnikkapore en jan Stephen Paul Weber en jan Valvin en jan Vejvej. toki pona ante toki: jan Ret Samys . musi ni li tan pali pi ma Elewa mama: jan David Revoy. jan awen nanpa wan: jan Craig Maloney. jan pi toki sitelen: jan Craig Maloney en jan Nartance en jan Scribblemaniac en jan Valvin. jan pi pona pali: jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson . ilo: ilo Krita 4.2.9-beta en ilo Inkscape 0.92.3 li kepeken ilo Kubuntu 19.10. nasin pi lawa jo: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|10|False|You can also translate this page if you want.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.

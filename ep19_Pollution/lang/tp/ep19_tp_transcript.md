# Transcript of Pepper&Carrot Episode 19 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa luka luka luka tu tu: jaki

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kajen|1|True|mi tawa tomo tan ni:
jan Kajen|2|True|o weka e telo pali ike sina lon anpa ma. pini la, sina kin o tawa tomo, o lape.
jan Kajen|3|False|mi wile e ni: sina pini e ale lon tenpo suno kama. mi pilin e ni: sina pini ala e ale. taso ken li ken!
jan Kajen|4|False|mi kute, mi pali mute!
jan Pepa|5|True|taso, mi wile sona e ni: mi weka ale lon anpa ma lon tenpo ale tan seme?
jan Pepa|6|True|mi pilin e ni: pona la...
jan Pepa|7|False|pona la SEME ?

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|labels in the garden :-)
jan Pepa|1|True|aaa...
jan Pepa|2|True|mi jan pi sona lili taso...
jan Pepa|3|True|taso tenpo sike ni la, kasi kili li nasa mute
jan Pepa|4|False|kasi ante lon poka tomo la, nasa sama li lon
sitelen|5|False|kili loje
sitelen|6|False|kili pi laso loje
jan Pepa|7|False|sama la, pipi li pali e ijo nasa.
jan Pepa|8|True|pilin mi la...
jan Pepa|9|False|ken la, ma li jaki lili tan pali mi. ken la, mi ken ante e pali mi. ken la, mi ken weka e jaki ni...
jan Kajen|10|True|o kute, jan " mi-ike-e-telo-pali-ale-mi " o,
jan Kajen|11|False|ken la len sina pi kulupu Piponaa li nasa e lawa sina.
jan Kajen|12|True|kulupu Pakalaa la, mi weka e pali pakala lon anpa ma!
jan Kajen|13|True|tenpo pini ale la, ni li nasin. ma li wile e NASIN ANTE la, ni li suli ala tawa mi!
jan Kajen|14|False|ni la o kalama ala, o PALI !!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|mi weka e pali ike mi
jan Pepa|2|False|nasin
jan Pepa|3|False|tenpo pini ale la
jan Pepa|4|False|MI SONA A!
soweli Kawa|5|False|Lllllape
jan Pepa|6|False|o wawa, soweli Kawa o !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Diary label on book
sitelen|6|False|lipu pi jan Kajen taso
jan Pepa|1|False|"jan wawa o, linja lawa sina li jelo suno, li pona a!"
jan Pepa|2|False|"...sina nasa e wawa mi, jan o!"
jan Pepa|3|False|"...suno mi pi tenpo pimeja la, sina pakala suwi."
jan Pepa|4|True|toki sitelen sina li pona kute, jan sona Kajen o!
jan Pepa|5|False|a, jan pi wawa nasa li weka e pali ike ona la, jan ante li ken sona e ona!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kajen|1|True|mi ale li pali ike lon tenpo pini.
jan Kajen|2|True|jan Kajen pi tenpo pini li toki e ni, li mi ala.
jan Kajen|3|False|mi weka e lipu ni tan sona pona.
jan Pepa|4|False|...
jan Pepa|5|True|pona!
jan Pepa|6|False|taso toki sina la, ni li seme?
sitelen|7|False|UNPA PAKALAA tan jan Tume
jan Tume|8|False|ni li pali ike taso tan tenpo pi mi lili. kin la, ona li ike tawa jan lili sama sina!
jan Pepa|9|True|aaa...
jan Pepa|10|False|mi kama sona...
jan Pepa|11|False|sina pilin ike ala tan ni ale...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|taso ma a en kasi a en soweli a !
jan Pepa|2|False|mi ale o jaki ala e ale sama ni. ike li kama tan pali ni!!!
jan Kajen|3|True|ike li kama ala lon tenpo ni!
jan Kajen|4|True|mi ale li jan pi kulupu Pakalaa! mi ale li weka e pali ike mi lon
jan Kajen|5|True|anpa mute!
jan Kajen|6|False|nasin mi li awen. mi toki ala e ona.
jan Kumin|7|True|aa, o lukin e ijo ni!
jan Kumin|8|False|ni li ken ala ken!
jan Kumin|9|False|ona li lon anpa ni tan seme?
jan Kumin|10|False|linja kalama li wile e pali pona. taso ona li musi awen.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Label of materials to translate
jan Kumin|1|False|nasin kalama li seme? Paa~Paa Paa, Paaaaakalaa!
jan Kumin|2|True|a a a, nimi kalama li weka tan sona mi!
jan Kumin|3|False|ken la, lipu mi pi nimi kalama li lon anpa ni kin...
jan Kajen|4|True|...ni la, wile pi mi ale li sama. ni li lawa sin pi kulupu Pakalaa:
jan Kajen|5|True|tenpo ni la, mi
jan Kajen|6|True|poki e ijo ale,
jan Kajen|7|True|li pakala e ona
jan Kajen|8|True|li pona sin e ona!
jan Kajen|9|False|IJO ALE A!!
sitelen|10|False|poki
sitelen|11|False|kiwen
sitelen toki|12|False|- PINI -
mama|13|False|tenpo 09/2016 - www.peppercarrot.com - musi sitelen & toki li tan jan David Revoy - ante toki pi toki pona li tan jan Ret Samys
mama|14|False|jan Craig Maloney li pona e kon toki. jan Valvin en jan Seblediacre en jan Alex Gryson li pona e nasin toki, e nimi, e toki jan. jan Tamalu li pona e toki pona. toki ni li sama toki "The book of secrets" tan jan Juan José Segura
mama|15|False|musi ni li tan pali pi ma Elewa. pali ni li tan jan David Revoy. jan Craig Maloney li pana e pona tawa ona. jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson li pona e musi ni.
mama|16|False|nasin lawa : Creative Commons Attribution 4.0, ilo: ilo Krita 3.0.1 en ilo Inkscape 0.91 li kepeken ilo Arch Linux XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 755 li pana e mani la, lipu ni li lon:
mama|2|False|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa lon www.patreon.com/davidrevoy
